//
//  loginViewController.h
//  sweaa
//
//  Created by ShKhan on 10/9/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ticketsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *conversationslb;
@property (weak, nonatomic) IBOutlet UILabel *compNamelb;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *backBu;
@property (weak, nonatomic) IBOutlet UIButton *closeBu;
@property (weak, nonatomic) IBOutlet UITableView *areaSettTable;
@property (weak, nonatomic) IBOutlet UILabel *unReadMesslb;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *unReadViewHCon;

@property (weak, nonatomic) IBOutlet UIButton *createConBu;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadActivity;

- (IBAction)newConClicked:(id)sender;

- (IBAction)backClicked:(id)sender;
- (IBAction)closeClicked:(id)sender;

 



@end


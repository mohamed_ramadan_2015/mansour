//
//  FirstCustomSegueUnwind.m
//  Helprox
//
//  Created by ShKhan on 10/10/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "FirstCustomSegueUnwind.h"

@implementation FirstCustomSegueUnwind

-(void)perform
{
    // Assign the source and destination views to local variables.
    UIView* secondVCView = self.sourceViewController.view;
    UIView* firstVCView = self.destinationViewController.view;
    
    NSLog(@"qqqwqwqwqwqwq");
   
    CGFloat screenHeight = UIScreen.mainScreen.bounds.size.height;
    
  //    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    
   //  secondVCView.frame = CGRectMake(0.0, -screenHeight/2, screenWidth, screenHeight);
    
    // Access the app's key window and insert the destination view above the current (source) one.
    
    [[[UIApplication sharedApplication] keyWindow] insertSubview:secondVCView aboveSubview:firstVCView];
    
    [UIView animateWithDuration:0.1 animations:^{
        
        firstVCView.frame = CGRectOffset(firstVCView.frame, 0.0, screenHeight);
        secondVCView.frame = CGRectOffset(secondVCView.frame, 0.0, screenHeight);
        
    } completion:^(BOOL finished) {
        
        
      // [self.sourceViewController dismissViewControllerAnimated:NO completion:nil];
        
        
        [self.destinationViewController.navigationController popViewControllerAnimated:NO];
    }];
    
    
    
}

 
@end

//
//  settings.h
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ticketItem : NSObject

@property (nonatomic,strong) NSString*id;

@property   (nonatomic,strong) NSString* lastMessage;

@property (nonatomic,strong) NSString*profilePic;

@property (nonatomic,strong) NSString*profileName;

@property (nonatomic,strong) NSString*profileId;

@property (nonatomic,strong) NSString*respond;

@property (nonatomic,strong) NSString*date;

@property (nonatomic,strong) NSString*time;

@property (nonatomic,strong) NSString*  unreadCount;

@property (nonatomic,strong) NSString*title;

@property (nonatomic,strong) NSString*project;

@property   bool status;

-(ticketItem*)initWithId:(NSString*)id
lastMessage:(NSString*)lastMessage
profilePic:(NSString*)profilePic
respond:(NSString*)respond
date:(NSString*)date
time:(NSString*)time
unreadCount:(NSString*)unreadCount
status:(bool)status
profileName:(NSString*)profileName
profileId:(NSString*)profileId
project:(NSString*)project
title:(NSString*)title;
@end

public void SocketOnNewMessage() {
        SocketOffNewMessage();
        mSocket.on("Ticket/" + messangerItem.getId()+"/out", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                for (int i = 0; i < args.length; i++) {
                   
                    ConversationItem conversationItem = new GsonBuilder().create().fromJson(args[i].toString(), ConversationItem.class);
                    List<ConversationItem> list = new ArrayList<>();
                    list.add(conversationItem);
                    addMessages(list);

                    HelproxRequests.readMessages(Conversation.this, messangerItem.getId());

                    Messanger.isThereNewTickets = true;

                    if(!pause) {
                        Conversation.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                vibe.vibrate(50);
                                Music.play1(Conversation.this, R.raw.yahoo, false, 5);
                            }
                        });
                    }
                }
            }
        });
    }
//
//  chatArTableViewCell.h
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface chatArTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet  UILabel*   nameLb;
@property (weak, nonatomic)  IBOutlet  UILabel*   messageLb;
@property (weak, nonatomic)  IBOutlet  UILabel*   datelb;
@property (weak, nonatomic)  IBOutlet  UILabel*   timelb;

@property (weak, nonatomic)  IBOutlet  UIImageView*   profileImageV;

@property (weak, nonatomic)  IBOutlet  UIView*   borderView;

@end

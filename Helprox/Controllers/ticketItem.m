//
//  settings.m
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//


#import "ticketItem.h"
  

@implementation ticketItem


-(ticketItem*)initWithId:(NSString*)id lastMessage:(NSString*)lastMessage profilePic:(NSString*)profilePic respond:(NSString*)respond date:(NSString*)date time:(NSString*)time unreadCount:(NSString*)unreadCount status:(bool)status profileName:(NSString*)profileName profileId:(NSString*)profileId project:(NSString *)project title:(NSString *)title
{
    self.id = id;
    
    self.lastMessage = lastMessage;
 
    self.profilePic = profilePic;
    
    self.respond = respond;
    
    self.date = date;
    
    self.time = time;
    
    self.unreadCount = unreadCount;
    
    self.status = status;
    
    self.profileName = profileName;
    
    self.profileId = profileId;
    
    self.title = title;
    
    self.project = project;
    
    return self;
}

 
- (NSString*)description
{
    return [NSString stringWithFormat:@"MyClass:%@ %@ %@ %@ %@ %@ %@ %ld %@ %@ %@ %@", self.id,self.lastMessage,self.profilePic,self.respond,self.date,self.time,self.unreadCount,(long)self.status,self.profileName,self.profileId, self.title, self.project];
}
 


@end

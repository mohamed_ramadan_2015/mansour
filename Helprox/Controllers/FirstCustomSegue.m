//
//  FirstCustomSegue.m
//  Helprox
//
//  Created by ShKhan on 10/10/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "FirstCustomSegue.h"

@implementation FirstCustomSegue

-(void)perform
{
    // Assign the source and destination views to local variables.
    UIView* firstVCView = self.sourceViewController.view;
    UIView* secondVCView = self.destinationViewController.view;
    
    
    // Get the screen width and height.
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    CGFloat screenHeight = UIScreen.mainScreen.bounds.size.height;
 
    secondVCView.frame = CGRectMake(0.0, screenHeight, screenWidth, screenHeight);
    
    // Access the app's key window and insert the destination view above the current (source) one.
    
    [[[UIApplication sharedApplication] keyWindow] insertSubview:secondVCView aboveSubview:firstVCView];
    
    [UIView animateWithDuration:0.1 animations:^{
        
        firstVCView.frame = CGRectOffset(firstVCView.frame, 0.0, -screenHeight);
        secondVCView.frame = CGRectOffset(secondVCView.frame, 0.0, -screenHeight);
        
    } completion:^(BOOL finished) {
        
        // [self.sourceViewController presentViewController:self.destinationViewController animated:NO  completion:nil];
        
      
          [self.sourceViewController.navigationController pushViewController:self.destinationViewController animated:NO];
    }];
    
  
    
}

@end

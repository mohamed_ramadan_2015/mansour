//
//  csUser.h
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface csUser : NSObject<NSCoding>

@property (nonatomic,strong) NSString*id;

@property (nonatomic,strong) NSString*name;

@property (nonatomic,strong) NSURL*imageUrl;


-(csUser*)initWithId:(NSString*)id  name:(NSString*)name imageStr:(NSString*)imageStr;

@end

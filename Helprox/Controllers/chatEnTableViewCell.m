//
//  chatEnTableViewCell.m
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "chatEnTableViewCell.h"

@implementation chatEnTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
     self.borderView.layer.cornerRadius = 8;
    
    self.sendlb.text = NSLocalizedString(@"messSending", "");
    
    self.tryAgainlb.text = NSLocalizedString(@"messTryAgain", "");
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

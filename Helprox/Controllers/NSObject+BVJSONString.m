//
//  NSObject+BVJSONString.m
//  Helprox
//
//  Created by ShKhan on 10/12/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "NSObject+BVJSONString.h"

@implementation NSObject (BVJSONString)

-(NSString*) jsonString  {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions)
                                                       NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

@end

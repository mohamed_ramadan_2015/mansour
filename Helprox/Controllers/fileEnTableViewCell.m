//
//  photoTableViewCell.m
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "fileEnTableViewCell.h"

@implementation fileEnTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.fileUrl .userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = \
    [[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(didTapLabelWithGesture:)];
    [self.fileUrl addGestureRecognizer:tapGesture];
  
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture
{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.fileUrl.text]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  DataClasss.h
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IdentifiedUser.h"
#import "settings.h"
 
static NSString *kUser = @"currentUser";

static NSString *ksettings = @"currentSettings";

static NSString *kUserType = @"UserType";

static NSString *kticketId = @"ticketId";

@import SocketIO;


@interface DataClassH : NSObject

@property (nonatomic,strong) IdentifiedUser*user;

@property (nonatomic,strong) settings*settings;

@property (nonatomic,strong) NSString*apiKey;

@property (nonatomic,strong) NSArray*weekNames;

@property (nonatomic,strong) SocketIOClient* socket;

@property (nonatomic,assign) NSInteger unReadCount;

+(bool)isNetworkAvailable;

+ (instancetype) shared;

+(void)clearUser;

+(void)clearSettings;

+(void)clearAll;

@end

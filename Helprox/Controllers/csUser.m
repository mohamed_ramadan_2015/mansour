//
//  csUser.m
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "csUser.h"

@interface csUser ()
{
    
}



@end

@implementation csUser

-(csUser*)initWithId:(NSString*)id  name:(NSString*)name imageStr:(NSString*)imageStr
{
    self.id = id;
    
    self.name = name ;
    
    self.imageUrl = [[NSURL alloc] initWithString:imageStr];
    
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.id forKey:@"id"];
    [encoder encodeObject:self.name forKey:@"name"];
     [encoder encodeObject:self.imageUrl forKey:@"imageUrl"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.id = [decoder decodeObjectForKey:@"id"];
        self.name = [decoder decodeObjectForKey:@"name"];
         self.imageUrl = [decoder decodeObjectForKey:@"imageUrl"];
        
    }
    return self;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"MyClass:%@ %@  ", self.name,self.imageUrl ];
}



@end

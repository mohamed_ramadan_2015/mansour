//
//  downloadItem.m
//  IBSWeather
//
//  Created by ShKhan on 12/4/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "downloadItem.h"

@implementation downloadItem


-(downloadItem*)initWithId:(NSString*)id
url:(NSURL*)url sender:(NSString*)sender
{
    
    self.id = id;
    
    self.url = url;
    
    self.sender = sender;
    
    return self;
    
    
}
-(void)startDownload
{
    
    NSLog(@"sdcvsdcfadsc : %@ -- %@",self.id,self.url);
    
    NSError*error=nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/Application Support"];
    
   NSString*filePathC = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Helprox/CSInfo"]];
    
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:filePathC])
    {
        if(
           
           [[NSFileManager defaultManager] createDirectoryAtPath:filePathC
                                     withIntermediateDirectories:YES
                                                      attributes:nil
                                                           error:&error])
        {
            NSLog(@"created");
        }
        else
        {
            NSLog(@"not created");
        }
    }
    else
    {
        NSLog(@"exists");
    }
    
    
    NSString *getImagePath = [filePathC stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",self.id]];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:getImagePath])
    {
        dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        dispatch_async(concurrentQueue, ^{
            __block NSData * imageData  = nil;
            dispatch_sync(concurrentQueue, ^{
                
                imageData = [[NSData alloc] initWithContentsOfURL:self.url];
                
                //Add the file name
                [imageData writeToFile:getImagePath atomically:YES]; //Write the file
                
            });
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                if(imageData)
                {
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"refreshImagesYaTickets"
                     object:self.sender
                     
                     ];
                }
            });
        });
    }
            
    
    
}



@end

//
//  settings.m
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//


#import "settings.h"
#import "csUser.h"
@interface settings ()
{
    
}



@end

@implementation settings


-(settings*)initWithTitle:(NSString*)title color:(NSString*)color active:(NSNumber*)active csUsers:(NSArray*)csUsers
{
    self.title = title ;
    
    self.color = [settings colorFromHexString:color];
 
    self.active = active;
    
    self.csUsers = csUsers;
    
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.color forKey:@"color"];
    [encoder encodeObject:self.active forKey:@"active"];
    [encoder encodeObject:self.csUsers forKey:@"csUsers"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
    self.title = [decoder decodeObjectForKey:@"title"];
    self.color = [decoder decodeObjectForKey:@"color"];
     self.active = [decoder decodeObjectForKey:@"active"];
    self.csUsers = [decoder decodeObjectForKey:@"csUsers"];
        
    }
    return self;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"MyClass:%@ %@ %@ %@", self.title,self.active,self.color,self.csUsers];
}

+(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}



@end

//
//  messengerViewController.m
//  sweaa
//
//  Created by ShKhan on 10/9/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "messengerViewController.h"
#import "loginViewController.h"
#import "chatArTableViewCell.h"
#import "chatEnTableViewCell.h"
#import "photoArTableViewCell.h"
#import "photoEnTableViewCell.h"
#import "fileArTableViewCell.h"
#import "fileEnTableViewCell.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "FirstCustomSegueUnwind.h"
#import "ticketsViewController.h"
#import "chatItem.h"
#import "NSObject+BVJSONString.h"
#import "downloadItem.h"
#import <AudioToolbox/AudioToolbox.h> 
static NSString *CellIdentifier1 = @"ContentCell1";
static NSString *CellIdentifier2 = @"ContentCell2";
static NSString *CellIdentifier3 = @"ContentCell3";
static NSString *CellIdentifier4 = @"ContentCell4";
static NSString *CellIdentifier5 = @"ContentCell5";
static NSString *CellIdentifier6 = @"ContentCell6";
@interface messengerViewController ()
{
    BOOL supportShown;
    
    BOOL onceDo;
    
    BOOL animationRunning;
    
    NSUserDefaults*defaults;
    
    NSString*filePathC;
    
    BOOL questionsSent;
    
    BOOL socketLisAdded;
    
    NSArray*tpImages;
    
    NSArray*btlbs;
    
    NSArray*btImages;
    
}

@property(nonatomic,strong)NSMutableArray*dataDef;

@end

@implementation messengerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    //  [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    
    tpImages = @[ self.tpImage1View,self.tpImage2View,self.tpImag3View];
    
    btImages = @[ self.btImage1View,self.btImage2View,self.btImage3View];
    
    btlbs = @[ self.bt1lb,self.bt2lb,self.bt3lb];
    
    _proNameLbl.text = _currentTicket.project;
    _titleLbl.text = _currentTicket.title;
    
    questionsSent = NO;
    
    socketLisAdded = NO;
    
    _headerView.backgroundColor = [DataClassH shared].settings.color;
    
    _supportView.backgroundColor = [DataClassH shared].settings.color;
    
    _backLaterView.backgroundColor = [DataClassH shared].settings.color;
    
    self.view.layer.borderWidth = 2 ;
    
    self.view.layer.borderColor = [DataClassH shared].settings.color.CGColor;
    
    self.dataDef = [NSMutableArray new];
    
    self.status = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"refreshImagesYaTickets"
                                               object:nil];
    
    
    /* chatItem*c1 = [[chatItem alloc]initWithType:@"text" name:@"" message:@"dcgbjsldkcbjkdsbcikascdssadkbgcasjkdcbaksjbcajksdbcasd from user" sender:@"user" messageId:@"" date:@"TUE , 2:00 PM" time:@""];
     
     chatItem*c2 = [[chatItem alloc]initWithType:@"text" name:@"" message:@"dcgbjsldkcbjkdsbcikascdssadkbgcasjkdcbaksjbcajksdbcasd from system" sender:@"system" messageId:@"" date:@"TUE , 2:00 PM" time:@""];
     
     chatItem*c3 = [[chatItem alloc]initWithType:@"image" name:@"avatarFemale.jpg" message:@"http://46.101.143.146:7894/uploads/h9VVu724wyKwdUBT9PommR8hW1nTVVou1KNHUPEy.jpeg" sender:@"user" messageId:@"K5iEXrJ0zl" date:@"TUE , 2:00 PM" time:@""];
     
     chatItem*c4 = [[chatItem alloc]initWithType:@"file" name:@"word.doc" message:@"http://46.101.143.146:7894/uploads/eLuIxn2eINgTXmjVEayES3XJTqL1gtskzjqfdfHx.doc" sender:@"user" messageId:@"" date:@"TUE , 2:00 PM" time:@""];
     
     
     [dataDef addObject:c1];
     
     [dataDef addObject:c2];
     
     [dataDef addObject:c3];
     
     [dataDef addObject:c4];*/
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    
    
    
    onceDo = YES;
    
    supportShown = YES;
    
    animationRunning = NO;
    
    [_titleBu setTitle: @"Approcks" forState:UIControlStateNormal];
    
    [_userlb setText:@"Shehata"];
    
    
    
    [_btRespondSoolb setText:NSLocalizedString(@"messRespond", "")];
    
    [_tpRespondSoolb setText:NSLocalizedString(@"messRespond", "")];
    
    
    [_supportBu setTitle:NSLocalizedString(@"messSupport", "") forState:UIControlStateNormal];
    
    [_backLaterlb setText:@"Back later today"];
    
    [_laterIconlb setText:[NSString stringWithFormat:@"%C",0xf0e2]];
    
    [_backBu setTitle: [NSString stringWithFormat:@"%C",0xf053] forState:UIControlStateNormal];
    
    [_closeBu setTitle: [NSString stringWithFormat:@"%C",0xf00d] forState:UIControlStateNormal];
    
    NSBundle* frameworkBundle = [NSBundle bundleForClass:[self class]];
    
    UINib *nib1 = [UINib nibWithNibName:@"chatArTableViewCell" bundle:frameworkBundle];
    
    [self.areaSettTable registerNib:nib1 forCellReuseIdentifier:CellIdentifier1];
    
    UINib *nib2 = [UINib nibWithNibName:@"chatEnTableViewCell" bundle:frameworkBundle];
    
    [self.areaSettTable registerNib:nib2 forCellReuseIdentifier:CellIdentifier2];
    
    UINib *nib3 = [UINib nibWithNibName:@"photoArTableViewCell" bundle:frameworkBundle];
    
    [self.areaSettTable registerNib:nib3 forCellReuseIdentifier:CellIdentifier3];
    
    UINib *nib4 = [UINib nibWithNibName:@"photoEnTableViewCell" bundle:frameworkBundle];
    
    [self.areaSettTable registerNib:nib4 forCellReuseIdentifier:CellIdentifier4];
    
    UINib *nib5 = [UINib nibWithNibName:@"fileArTableViewCell" bundle:frameworkBundle];
    
    [self.areaSettTable registerNib:nib5 forCellReuseIdentifier:CellIdentifier5];
    
    UINib *nib6 = [UINib nibWithNibName:@"fileEnTableViewCell" bundle:frameworkBundle];
    
    [self.areaSettTable registerNib:nib6 forCellReuseIdentifier:CellIdentifier6];
    
    
    [self.areaSettTable setBounces:NO];
    
    self.areaSettTable.delegate=self;
    
    self.areaSettTable.dataSource=self;
    
    //self.areaSettTable.backgroundColor = [UIColor lightGrayColor];
    
    self.areaSettTable.opaque = NO;
    
    self.areaSettTable.separatorColor = [UIColor clearColor];
    
    self.areaSettTable.estimatedRowHeight = 200;
    
    self.areaSettTable.rowHeight = UITableViewAutomaticDimension;
    
    self.areaSettTable.showsVerticalScrollIndicator = NO;
    
    _sendTexF.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"messSendAmes", "")
                                    attributes:@{
                                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                                 
                                                 }
     ];
    
    
    _sendTexF.delegate = self ;
    
    
    UITapGestureRecognizer *tapGesture = \
    [[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(didTapLabelWithGesture:)];
    
    
    tapGesture.numberOfTapsRequired=1;
    
    
    [self.supportView addGestureRecognizer:tapGesture];
    
    self.supportView.clipsToBounds = YES ;
    
    
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:
                                         @selector(swipeUp:)];
    
    
    swipeUp.direction=UISwipeGestureRecognizerDirectionUp;
    
    
    [self.supportView addGestureRecognizer:swipeUp];
    
    
    UITapGestureRecognizer *tapGesture2 = \
    [[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(didTapLabelWithGesture:)];
    
    
    tapGesture.numberOfTapsRequired=1;
    
    
    [self.topView addGestureRecognizer:tapGesture2];
    
    
    
    
    NSError*error=nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/Application Support"];
    
    filePathC = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"chatImages"]];
    
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:filePathC])
    {
        if(
           
           [[NSFileManager defaultManager] createDirectoryAtPath:filePathC
                                     withIntermediateDirectories:YES
                                                      attributes:nil
                                                           error:&error])
        {
            NSLog(@"created");
        }
        else
        {
            NSLog(@"not created");
        }
    }
    else
    {
        NSLog(@"exists");
    }
    
    
    
    if([[DataClass getInstance] ar])
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
    }
    
    
    [self.loadActivity startAnimating];
    
    
    if([DataClass isNetworkAvailable])
    {
        
        if([defaults objectForKey:kticketId] != nil)
        {
            [self getMessages];
            
        }
        
    }
    else
    {
        
        UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                           message:NSLocalizedString(@"internetMessage", nil)
                                                          delegate:self
                                                 cancelButtonTitle:NSLocalizedString(@"langAlertTry", nil)
                                                 otherButtonTitles:nil];
        [alert show];
        
    }
    
    
    if([DataClassH shared].socket == nil && [DataClassH shared].user != nil )
    {
        
        NSLog(@"setttttttttttting in messengerViewController");
        
        NSURL* url = [[NSURL alloc] initWithString:@"http://46.101.143.146:7071"];
        
        [DataClassH shared].socket = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
        
        
        [ [DataClassH shared].socket connect];
        
        NSDictionary*wer = @{@"id":[DataClassH shared].user.id,@"type":[DataClassH shared].user.type};
        
        
        [ [DataClassH shared].socket emit:@"auth" with:@[[wer jsonString]]];
    }
    else
    {
        NSLog(@"connected255 socket in messengerViewController");
        
        NSDictionary*wer = @{@"id":[DataClassH shared].user.id,@"type":[DataClassH shared].user.type};
        
        [ [DataClassH shared].socket emit:@"auth" with:@[[wer jsonString]]];
        
    }
    
    
    if([defaults objectForKey:kticketId] == nil)
    {
        [self.loadActivity stopAnimating];
        
        self.loadActivity.hidden = YES;
    }
    
    
    
    [self displayBoard];
    
    NSLog(@"asicxhn;aioksdhjcoa;sdncao : %@ ==== %@ ",self.currentTicket.profilePic ,[DataClassH shared].settings.csUsers );
    
}
-(void)receiveNotification:(NSNotification *)paramNotification
{
    
    
    if([paramNotification.name isEqualToString:@"refreshImagesYaTickets"] && [paramNotification.object  isEqualToString:@"messenger"])
    {
        
    }
    
}



-(void)displayBoard
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/Application Support"];
    
    NSString* fileCS = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Helprox/CSInfo"]];
    
    
    if([self.currentTicket.profilePic isEqualToString:@""] || [defaults objectForKey:kticketId] == nil)
    {
        
        
        self.supportBu.hidden = NO;
        
        self.topView.hidden = YES;
        
        for (int i = 0 ;i< [[DataClassH shared].settings.csUsers count];i++)
        {
            
            csUser*us = [[DataClassH shared].settings.csUsers objectAtIndex:i];
            
            NSString *getImagePath = [fileCS stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",us.id]];
            
            if([[NSFileManager defaultManager] fileExistsAtPath:getImagePath])
            {
                UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
                
                UIImageView*im1 =  [tpImages objectAtIndex:i];
                
                UILabel*bt1 = [btlbs objectAtIndex:i];
                
                UIImageView*im2 =  [btImages objectAtIndex:i];
                
                im1.image = img;
                
                im2.image = img;
                
                bt1.text = us.name;
                
            }
            else
            {
                
                downloadItem*dw = [[downloadItem alloc]initWithId:us.id url:us.imageUrl sender:@"messenger"];
                
                [dw startDownload];
                
                
            }
            
        }
        
    }
    else
    {
        
        self.supportBu.hidden = NO;
        
        self.topView.hidden = YES;
        
        for (int i = 0 ;i< [[DataClassH shared].settings.csUsers count];i++)
        {
            
            csUser*us = [[DataClassH shared].settings.csUsers objectAtIndex:i];
            
            
            if(![us.id  isEqualToString: self.currentTicket.profileId])
            {
                
                // UIImageView*im1 =  [tpImages objectAtIndex:i];
                
                UILabel*bt1 = [btlbs objectAtIndex:i];
                
                UIImageView*im2 =  [btImages objectAtIndex:i];
                
                // im1.hidden = YES;
                
                im2.hidden = YES;
                
                bt1.hidden = YES;
                
                NSLog(@"hiiiiiit555555");
                
                continue;
                
            }
            
            NSLog(@"hiiiiiit6666666");
            
            NSString *getImagePath = [fileCS stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",us.id]];
            
            if([[NSFileManager defaultManager] fileExistsAtPath:getImagePath])
            {
                UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
                
                UIImageView*im1 =  [tpImages objectAtIndex:1];
                
                UILabel*bt1 = [btlbs objectAtIndex:1];
                
                UIImageView*im2 =  [btImages objectAtIndex:1];
                
                im1.hidden = NO;
                
                im2.hidden = NO;
                
                bt1.hidden = NO;
                
                im1.image = img;
                
                im2.image = img;
                
                bt1.text = us.name;
                
            }
            else
            {
                
                downloadItem*dw = [[downloadItem alloc]initWithId:us.id url:us.imageUrl sender:@"messenger"];
                
                [dw startDownload];
                
                
            }
            
        }
        
        
        
    }
    
    
}

-(void) swipeUp:(id) sender {
    
    [self manageSupportView:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    
    
    [center addObserver:self selector:@selector(handleKeyboardWillShow:)
                   name:UIKeyboardWillShowNotification object:nil];
    
    [center addObserver:self selector:@selector(handleKeyboardWillHide:)
                   name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    // circle the CS images
    for (int i=0; i < [tpImages count]; ++i) {
        [self setupCornersToView:tpImages[i]];
    }
    
    for (int j=0; j < [btImages count]; ++j) {
        [self setupCornersToView:btImages[j]];
    }
}

-(void)setupCornersToView:(UIView*)view {
    view.layer.cornerRadius = view.frame.size.width / 2;
    view.clipsToBounds = YES;
}

-(void)viewDidLayoutSubviews
{
    
    if(onceDo)
    {
        
        if(!self.status)
        {
            
            self.chatViewTopCon.constant -= self.chatView.bounds.size.height;
            
            [self.view layoutSubviews];
            
        }
        
        NSLog(@"support2555 %hhd",supportShown);
        
        //   [self manageSupportView:NO];
        
        //  [self manageSupportView:NO];
        
        self.backBu.titleLabel.textColor = [UIColor whiteColor];
        
        self.closeBu.titleLabel.textColor = [UIColor whiteColor];
        
        [self.titleBu setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.titleBu.titleLabel.textColor = [UIColor whiteColor];
        
        self.headerView.backgroundColor = self.supportView.backgroundColor ;
        
        onceDo = NO;
        
    }
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture
{
    
    if([tapGesture.view  isEqual: self.supportView])
    {
        [self manageSupportView:YES];
    }
    else   if([tapGesture.view  isEqual: self.topView])
    {
        [self manageSupportView:YES];
    }
    
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void) handleKeyboardWillShow:(NSNotification *)paramNotification
{
    
    if(self.sendTexF.isFirstResponder)
    {
        
        
        NSLog(@"called");
        
        if(supportShown)
        {
            [self manageSupportView:YES];
        }
        
        NSDictionary *userInfo = paramNotification.userInfo;
        /* Get the duration of the animation of the keyboard for when it
         gets displayed on the screen. We will animate our contents using
         the same animation duration */
        NSValue *animationDurationObject =
        userInfo[UIKeyboardAnimationDurationUserInfoKey];
        NSValue *keyboardEndRectObject = userInfo[UIKeyboardFrameEndUserInfoKey];
        
        
        double animationDuration = 0.5;
        CGRect keyboardEndRect = CGRectMake(0.0f, 0.0f, 0.0f, 0.0f);
        [animationDurationObject getValue:&animationDuration];
        [keyboardEndRectObject getValue:&keyboardEndRect];
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        /* Convert the frame from window's coordinate system to
         our view's coordinate system */
        keyboardEndRect = [self.view convertRect:keyboardEndRect
                                        fromView:window];
        /* Find out how much of our view is being covered by the keyboard */
        CGRect intersectionOfKeyboardRectAndWindowRect =
        CGRectIntersection(self.view.frame, keyboardEndRect);
        /* Scroll the scroll view up to show the full contents of our view */
        
        self.sendViewBottomCon.constant =  intersectionOfKeyboardRectAndWindowRect.size.height;;
        
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            if([self.dataDef count] >= 1)
                [self.areaSettTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.dataDef count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
        }];
        
        
        
    }
    
    
}
- (void) handleKeyboardWillHide:(NSNotification *)paramSender
{
    
    if(self.sendTexF.isFirstResponder)
    {
        
        NSDictionary *userInfo = [paramSender userInfo];
        NSValue *animationDurationObject =
        [userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey];
        double animationDuration = 0.5;
        [animationDurationObject getValue:&animationDuration];
        
        self.sendViewBottomCon.constant = 0;
        
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.view layoutIfNeeded];
        }];
        
    }
}



- (void)viewWillDisappear:(BOOL)paramAnimated{
    [super viewWillDisappear:paramAnimated];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL)cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0){ NSLog(@"Media type is empty."); return NO;
    }
    NSArray *availableMediaTypes =
    [UIImagePickerController
     availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock:
     ^(id obj, NSUInteger idx, BOOL *stop) {
         NSString *mediaType = (NSString *)obj;
         if ([mediaType isEqualToString:paramMediaType]){
             result = YES;
             *stop= YES; }
     }];
    return result;
}
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"Picker returned successfully.");
    NSLog(@"%@", info);
    NSString     *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(__bridge NSString *)kUTTypeMovie]){ NSURL *urlOfVideo = info[UIImagePickerControllerMediaURL]; NSLog(@"Video URL = %@", urlOfVideo);
    }
    else if ([mediaType isEqualToString:(__bridge NSString *)kUTTypeImage]){
        /* Let's get the metadata. This is only for
         images. Not videos */
        NSDictionary *metadata = info[UIImagePickerControllerMediaMetadata];
        UIImage *theImage = info[UIImagePickerControllerOriginalImage];
        NSLog(@"Image Metadata = %@", metadata);
        NSLog(@"Image = %@", theImage);
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{ NSLog(@"Picker was cancelled");
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.areaSettTable])
    {
        
        return  [self.dataDef count];
        
    }
    
    return 0;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([tableView isEqual:self.areaSettTable])
    {
        
        
        chatItem* cr = [self.dataDef objectAtIndex:indexPath.row];
        
        if([cr.type isEqualToString:@"text"])
        {
            if([cr.userId isEqualToString:@""] || cr.userId == nil)
            {
                chatArTableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier1];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.nameLb.text = [NSString stringWithFormat:@"%@ %@",[DataClassH shared].user.firstName,[DataClassH shared].user.lastName];
                
                cell.messageLb.text = cr.message;
                
                cell.datelb.text = cr.date;
                
                cell.timelb.text = cr.time;
                
                cell.backgroundColor=[UIColor whiteColor];
                
                [cell layoutSubviews];
                
                [cell layoutIfNeeded];
                
                return cell;
            }
            else
            {
                chatEnTableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier2];
                
                cell.borderView.backgroundColor = [DataClassH shared].settings.color;
                
                cell.messageLb.backgroundColor = [DataClassH shared].settings.color;
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.nameLb.text = @"System";
                
                cell.messageLb.text = cr.message;
                
                cell.datelb.text = cr.date;
                
                cell.timelb.text = cr.time;
                
                if(cr.sendState == KSbinding)
                {
                    cell.tryAgainlb.hidden = YES;
                    
                    cell.datelb.hidden = YES;
                    
                    cell.seenImageV.hidden = YES;
                    
                    cell.sendActivity.hidden = NO;
                    
                    [cell.sendActivity startAnimating];
                    
                    cell.sendlb.hidden = NO;
                }
                else if(cr.sendState == KSsended)
                {
                    cell.tryAgainlb.hidden = YES;
                    
                    cell.datelb.hidden = NO;
                    
                    cell.seenImageV.hidden = NO;
                    
                    cell.sendActivity.hidden = YES;
                    
                    cell.sendlb.hidden = YES;
                }
                else if(cr.sendState == KSerror)
                {
                    cell.tryAgainlb.hidden = NO;
                    
                    cell.datelb.hidden = YES;
                    
                    cell.seenImageV.hidden = YES;
                    
                    cell.sendActivity.hidden = YES;
                    
                    cell.sendlb.hidden = YES;
                    
                }
                
                [cell layoutSubviews];
                
                [cell layoutIfNeeded];
                
                return cell;
            }
        }
        else if([cr.type isEqualToString:@"image"])
        {
            
            if([cr.userId isEqualToString:@""] || cr.userId == nil)
            {
                photoArTableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier3];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                NSString *getImagePath = [filePathC stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpeg",cr.messageId]];
                
                if([[NSFileManager defaultManager] fileExistsAtPath:getImagePath])
                {
                    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
                    cell.sendedImageV.image =img;
                    [cell.activity stopAnimating];
                    cell.activity.hidden=YES;
                }
                else
                {
                    
                    [cell.activity startAnimating];
                    cell.activity.hidden=NO;
                    
                    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    
                    dispatch_async(concurrentQueue, ^{
                        __block NSData * imageData  = nil;
                        dispatch_sync(concurrentQueue, ^{
                            
                            
                            
                            imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:cr.message]];
                            
                            //Add the file name
                            [imageData writeToFile:getImagePath atomically:YES]; //Write the file
                            
                            
                            
                            
                        });
                        
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            
                            
                            if(imageData)
                            {
                                
                                [self.areaSettTable reloadData];
                                
                            }
                            
                            
                            
                        });
                    });
                    
                    
                }
                
                
                [cell layoutSubviews];
                
                [cell layoutIfNeeded];
                
                return cell;
            }
            else
            {
                photoEnTableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier4];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                NSString *getImagePath = [filePathC stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpeg",cr.messageId]];
                
                if([[NSFileManager defaultManager] fileExistsAtPath:getImagePath])
                {
                    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
                    cell.sendedImageV.image =img;
                    [cell.activity stopAnimating];
                    cell.activity.hidden=YES;
                }
                else
                {
                    
                    [cell.activity startAnimating];
                    cell.activity.hidden=NO;
                    
                    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    
                    dispatch_async(concurrentQueue, ^{
                        __block NSData * imageData  = nil;
                        dispatch_sync(concurrentQueue, ^{
                            
                            
                            
                            imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:cr.message]];
                            
                            //Add the file name
                            [imageData writeToFile:getImagePath atomically:YES]; //Write the file
                            
                            
                            
                            
                        });
                        
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            
                            
                            if(imageData)
                            {
                                
                                [self.areaSettTable reloadData];
                                
                            }
                            
                            
                            
                        });
                    });
                    
                    
                }
                
                [cell layoutSubviews];
                
                [cell layoutIfNeeded];
                
                return cell;
            }
            
            
            
        }
        else if([cr.type isEqualToString:@"file"])
        {
            
            if([cr.userId isEqualToString:@""] || cr.userId == nil)
            {
                fileEnTableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier6];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.fileUrl.text = cr.message;
                
                cell.fileName.text = cr.fileName;
                
                [cell layoutSubviews];
                
                [cell layoutIfNeeded];
                
                return cell;
            }
            else
            {
                
                fileArTableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier5];
                
                [cell.link1Bu setTitleColor:[DataClassH shared].settings.color forState:UIControlStateNormal];
                
                [cell.link2Bu setTitleColor:[DataClassH shared].settings.color forState:UIControlStateNormal];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.fileUrl.text = cr.message;
                
                cell.fileName.text = cr.fileName;
                
                [cell layoutSubviews];
                
                [cell layoutIfNeeded];
                
                return cell;
            }
            
            
        }
        
    }
    
    return nil;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([tableView isEqual:self.areaSettTable])
        
    {
        chatItem*cr = [self.dataDef objectAtIndex:indexPath.row];
        
        if(cr.sendState == KSerror)
        {
            cr.sendState = KSbinding ;
            
            [self.areaSettTable reloadData];
            
            [self newMessage2:cr.message];
        }
        
    }
    
    
}

/*
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return 72;
 }
 
 */



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)supportClicked:(id)sender {
    
    [self manageSupportView:YES];
}

- (IBAction)backClicked:(id)sender {
    
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"refreshDataYaTickets"
     object:nil
     ];
    
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[ticketsViewController class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            
            
            break;
        }
    }
    
    // [self performSegueWithIdentifier:@"goBack" sender:self];
    
    
    
}



- (IBAction)closeClicked:(id)sender {
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[ticketsViewController class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            
            
            break;
        }
    }
    
    
}

- (IBAction)sendClicked:(id)sender {
    
    
    NSString *trimmedString = [self.sendTexF.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    if([trimmedString isEqualToString:@""])
    {
        UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                           message:NSLocalizedString(@"messEnterText", "")
                                                          delegate:nil
                                                 cancelButtonTitle:NSLocalizedString(@"langAlertOk", "")
                                                 otherButtonTitles:nil];
        [alert show];
        
        return ;
    }
    
    
    /* if(![DataClass isNetworkAvailable])
     {
     
     UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
     message:NSLocalizedString(@"internetMessage", nil)
     delegate:self
     cancelButtonTitle:NSLocalizedString(@"langAlertTry", nil)
     otherButtonTitles:nil];
     [alert show];
     
     return;
     }*/
    
    
    chatItem*cr = [[chatItem alloc] initWithMessage:trimmedString messageId:@"" sender:@"system" ticketId:[defaults objectForKey:kticketId] type:@"text" userId:[DataClassH shared].user.id date:@"11/11/2017" time:@"" fileName:@"" sendState:KSbinding];
    
    [self.dataDef addObject:cr];
    
    [self.areaSettTable reloadData];
    
    if([self.dataDef count] >= 1)
        [self.areaSettTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.dataDef count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    
    [self newMessage];
    
}

- (IBAction)photoClicked:(id)sender {
    
    [self showPhotos];
}

- (IBAction)textClicked:(id)sender {
    
    [_sendTexF becomeFirstResponder];
    
}

- (IBAction)titleClicked:(id)sender {
    
    // [self manageSupportView:YES];
    
}


-(void)manageSupportView:(BOOL)animate
{
    
    if(animationRunning)
    {
        return ;
    }
    
    animationRunning = YES;
    
    if(supportShown)
    {
        NSLayoutConstraint*full=[NSLayoutConstraint constraintWithItem:self.supportView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
        
        
        
        [self.supportView addConstraint:full];
        
        
        for (NSLayoutConstraint*con in self.supportView.constraints)
        {
            if (con.firstAttribute == NSLayoutAttributeBottom && con.firstItem == self.btRespondSoolb)
            {
                
                [self.supportView removeConstraint:con];
                
                break;
            }
            
            
        }
        
        
        /*  self.backBu.titleLabel.textColor = [UIColor colorWithRed:6/255.0 green:103/255.0 blue:167/255.0 alpha:1];
         
         self.closeBu.titleLabel.textColor = [UIColor colorWithRed:6/255.0 green:103/255.0 blue:167/255.0 alpha:1];
         
         [self.titleBu setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
         
         self.headerView.backgroundColor = [UIColor colorWithRed:243/255.0 green:243/255.0 blue:243/255.0 alpha:1];
         */
        
        
        self.suppHCon.constant = 0;
        
        if(animate)
        {
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
                
            }completion:^(BOOL finished){
                
                supportShown = NO;
                animationRunning = NO ;
                
                self.topView.hidden = NO;
                
                self.supportBu.hidden = YES;
                
                
            } ];
            
        }
        else
        {
            [self.view layoutIfNeeded];
            
            supportShown = NO;
            animationRunning = NO ;
            
            self.topView.hidden = NO;
            
            self.supportBu.hidden = YES;
            
            
        }
        
        
        
    }
    else
    {
        for (NSLayoutConstraint*con in self.supportView.constraints)
        {
            if (con.firstAttribute == NSLayoutAttributeHeight)
            {
                
                [self.supportView removeConstraint:con];
                
                break;
            }
            
            
        }
        
        
        NSLayoutConstraint*full1=[NSLayoutConstraint constraintWithItem:self.btRespondSoolb attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.supportView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-15.0];
        
        full1.priority = UILayoutPriorityDefaultHigh;
        
        [self.supportView addConstraint:full1];
        
        
        /*   self.backBu.titleLabel.textColor = [UIColor whiteColor];
         
         self.closeBu.titleLabel.textColor = [UIColor whiteColor];
         
         // self.titleBu.titleLabel.textColor = [UIColor whiteColor];
         
         [self.titleBu setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         
         self.headerView.backgroundColor = self.supportView.backgroundColor ;
         
         //self.supportView.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:102.0/255.0 blue:170.0/255.0 alpha:1];
         */
        
        //self.suppHCon.constant =165;
        
        
        if(animate)
        {
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
                
            }completion:^(BOOL finished){
                
                supportShown = YES;
                animationRunning = NO;
                
                self.topView.hidden = YES;
                
                self.supportBu.hidden = NO;
                
            } ];
            
        }
        else
        {
            [self.view layoutIfNeeded];
            
            supportShown = YES;
            animationRunning = NO ;
            
            self.topView.hidden = YES;
            
            self.supportBu.hidden = NO;
        }
        
        
        
    }
}

-(void)showPhotos
{
    if ([self isPhotoLibraryAvailable])
    {
        UIImagePickerController *controller =
        [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary; NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
        if ([self canUserPickPhotosFromPhotoLibrary])
        {
            
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
        }
        
        /*  if ([self canUserPickVideosFromPhotoLibrary])
         {
         [mediaTypes addObject:(__bridge NSString *)kUTTypeMovie];
         }*/
        controller.mediaTypes = mediaTypes;
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    
    
}

-(void)newMessage
{
    
    self.sendBu.enabled = NO;
    
    NSLog(@"sending.......");
    
    NSString*message = [self.sendTexF.text stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    
    NSString*ticket = [defaults objectForKey:kticketId] ;
    
    NSString*ticketId = (ticket != nil)? ticket : @"" ;
    
    NSString *urlAsString = @"http://46.101.143.146:7894/apis/v1/newMessage";
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body = @"";
    
    if(!questionsSent)
    {
        NSString*projectName = [defaults objectForKey:kProjectName];
        
        
        NSString*chatTitle = [defaults objectForKey:kChatTitle];
        
        
        
        NSDictionary*dic1 = @{@"question":NSLocalizedString(@"queProjectName",@""),
                              
                              @"answer":projectName
                              
                              };
        
        NSDictionary*dic2 = @{@"question":NSLocalizedString(@"queChatTitle",@""),
                              
                              @"answer":chatTitle
                              
                              };
        
        
        NSArray*arr1 = [[NSArray alloc]initWithObjects:dic1,dic2, nil];
        
        
        body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_id=%@&message=%@&ticket_id=%@&user_type=%@&questions=%@",[DataClassH shared].apiKey,@"ios",[DataClassH shared].user.id,message,ticketId,[DataClassH shared].user.type,[arr1 jsonString]];
        
        
        NSLog(@"api key : %@",[DataClassH shared].apiKey);
        
        NSLog(@"user_id : %@",[DataClassH shared].user.id);
        
        NSLog(@"message : %@",message);
        
        NSLog(@"ticket_id : %@",ticketId);
        
        NSLog(@"user_type : %@",[DataClassH shared].user.type);
        
        NSLog(@"questions : %@",[arr1 jsonString]);
        
        
    }
    else
    {
        body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_id=%@&message=%@&ticket_id=%@&user_type=%@",[DataClassH shared].apiKey,@"ios",[DataClassH shared].user.id,message,ticketId,[DataClassH shared].user.type];
    }
    
    
    
    
    
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             NSLog(@"newMessage respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     NSLog(@"newMessage success");
                     
                     
                     
                     NSDictionary*dic = responseDict[@"message"];
                     
                     NSString*ticketId = dic[@"ticket_id"] ;
                     
                     if(ticketId != nil)
                     {
                         
                         [defaults setObject:ticketId forKey:kticketId];
                         
                         
                     }
                     
                     NSString*type = dic[@"type"];
                     
                     NSString*message = dic[@"message"];
                     
                     NSString*filename = dic[@"file_name"];
                     
                     NSString*receiver = dic[@"receiver"];
                     
                     NSString*message_id = dic[@"message_id"];
                     
                     NSNumber*timestamp = dic[@"timestamp"];
                     
                     NSTimeInterval timeInterval = [timestamp doubleValue]/1000.0;
                     
                     
                     NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                     
                     // Create a Gregorian Calendar
                     NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                     
                     
                     long dayNumber   = [gregorianCalendar component:NSCalendarUnitWeekday fromDate:date];
                     
                     NSString*dayName = [[DataClassH shared].weekNames objectAtIndex:dayNumber-1];
                     
                     NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
                     
                     [dateformatter setDateFormat:@"dd/MM/YYYY"];
                     
                     NSString *dateString=[dateformatter stringFromDate:date];
                     
                     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                     
                     formatter.AMSymbol = @"AM";
                     
                     formatter.PMSymbol = @"PM";
                     
                     [formatter setDateFormat:@"HH:mm a"];
                     
                     NSString *startTimeString = [formatter stringFromDate:date];
                     
                     NSString*lastStr = [NSString stringWithFormat:@"%@,%@",dayName,startTimeString];
                     
                     chatItem*cr = [[chatItem alloc] initWithMessage:message messageId:message_id sender:receiver ticketId:ticketId type:type userId:userId date:lastStr time:startTimeString fileName:filename sendState:KSsended];
                     
                     bool add = YES;
                     
                     for (int i=0;i<self.dataDef.count;i++)
                     {
                         
                         chatItem*df  = self.dataDef[i];
                         
                         if(df.sendState == KSbinding)
                         {
                             
                             [self.dataDef replaceObjectAtIndex:i withObject:cr];
                             
                             add = NO;
                             
                             break;
                         }
                         
                     }
                     
                     if(add)
                     {
                         [self.dataDef addObject:cr];
                     }
                     
                     if(!socketLisAdded)
                     {
                         [self addSocketListeners];
                         
                         socketLisAdded = YES;
                     }
                     
                     
                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {
                         
                         questionsSent = YES;
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.areaSettTable reloadData];
                             
                             if([self.dataDef count] >= 1)
                                 [self.areaSettTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.dataDef count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                             
                             
                             
                             
                         });
                         
                         
                         self.sendTexF.text = @"";
                         
                         
                         
                         
                         self.sendBu.enabled = YES;
                         
                     });
                 }
                 
                 else
                 {
                     
                     
                     
                     NSLog(@"newMessage request fail reason %@",responseDict[@"reason"]);
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"newMessage nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"newMessage error : %@",error);
             
             dispatch_queue_t mainQueue = dispatch_get_main_queue();
             
             dispatch_async(mainQueue, ^(void) {
                 
                 
                 for (int i=0;i<self.dataDef.count;i++)
                 {
                     
                     chatItem*df  = self.dataDef[i];
                     
                     if(df.sendState == KSbinding)
                     {
                         df.sendState = KSerror;
                         
                         break;
                     }
                     
                 }
                 [self.areaSettTable reloadData];
                 
             });
             
         }
         
     }];
    
    
}

-(void)newMessage2:(NSString*)mess
{
    
    self.sendBu.enabled = NO;
    
    NSLog(@"sending.......");
    
    NSString*ticket = [defaults objectForKey:kticketId] ;
    
    NSString*ticketId = (ticket != nil)? ticket : @"" ;
    
    NSString *urlAsString = @"http://46.101.143.146:7894/apis/v1/newMessage";
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body = @"";
    
    if(!questionsSent)
    {
        NSString*projectName = [defaults objectForKey:kProjectName];
        
        
        NSString*chatTitle = [defaults objectForKey:kChatTitle];
        
        _proNameLbl.text = projectName;
        _titleLbl.text = chatTitle;
        
        NSDictionary*dic1 = @{@"question":NSLocalizedString(@"queProjectName",@""),
                              
                              @"answer":projectName
                              
                              };
        
        NSDictionary*dic2 = @{@"question":NSLocalizedString(@"queChatTitle",@""),
                              
                              @"answer":chatTitle
                              
                              };
        
        
        NSArray*arr1 = [[NSArray alloc]initWithObjects:dic1,dic2, nil];
        
        
        body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_id=%@&message=%@&ticket_id=%@&user_type=%@&questions=%@",[DataClassH shared].apiKey,@"ios",[DataClassH shared].user.id,mess,ticketId,[DataClassH shared].user.type,[arr1 jsonString]];
        
        
        NSLog(@"api key : %@",[DataClassH shared].apiKey);
        
        NSLog(@"user_id : %@",[DataClassH shared].user.id);
        
        NSLog(@"message : %@",mess);
        
        NSLog(@"ticket_id : %@",ticketId);
        
        NSLog(@"user_type : %@",[DataClassH shared].user.type);
        
        NSLog(@"questions : %@",[arr1 jsonString]);
        
        
    } else {
        body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_id=%@&message=%@&ticket_id=%@&user_type=%@",[DataClassH shared].apiKey,@"ios",[DataClassH shared].user.id,mess,ticketId,[DataClassH shared].user.type];
    }
    

    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             NSLog(@"newMessage2 respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     NSLog(@"newMessage2 success");
                     
                     
                     
                     NSDictionary*dic = responseDict[@"message"];
                     
                     NSString*ticketId = dic[@"ticket_id"] ;
                     
                     if(ticketId != nil)
                     {
                         
                         [defaults setObject:ticketId forKey:kticketId];
                         
                         
                     }
                     
                     NSString*type = dic[@"type"];
                     
                     NSString*message = dic[@"message"];
                     
                     NSString*filename = dic[@"file_name"];
                     
                     NSString*receiver = dic[@"receiver"];
                     
                     NSString*message_id = dic[@"message_id"];
                     
                     NSNumber*timestamp = dic[@"timestamp"];
                     
                     NSTimeInterval timeInterval = [timestamp doubleValue]/1000.0;
                     
                     
                     NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                     
                     // Create a Gregorian Calendar
                     NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                     
                     
                     long dayNumber   = [gregorianCalendar component:NSCalendarUnitWeekday fromDate:date];
                     
                     NSString*dayName = [[DataClassH shared].weekNames objectAtIndex:dayNumber-1];
                     
                     NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
                     
                     [dateformatter setDateFormat:@"dd/MM/YYYY"];
                     
                     NSString *dateString=[dateformatter stringFromDate:date];
                     
                     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                     
                     formatter.AMSymbol = @"AM";
                     
                     formatter.PMSymbol = @"PM";
                     
                     [formatter setDateFormat:@"HH:mm a"];
                     
                     NSString *startTimeString = [formatter stringFromDate:date];
                     
                     NSString*lastStr = [NSString stringWithFormat:@"%@,%@",dayName,startTimeString];
                     
                     chatItem*cr = [[chatItem alloc] initWithMessage:message messageId:message_id sender:receiver ticketId:ticketId type:type userId:userId date:lastStr time:startTimeString fileName:filename sendState:KSsended];
                     
                     bool add = YES;
                     
                     for (int i=0;i<self.dataDef.count;i++)
                     {
                         
                         chatItem*df  = self.dataDef[i];
                         
                         if(df.sendState == KSbinding)
                         {
                             
                             [self.dataDef replaceObjectAtIndex:i withObject:cr];
                             
                             add = NO;
                             
                             break;
                         }
                         
                     }
                     
                     if(add)
                     {
                         [self.dataDef addObject:cr];
                     }
                     
                     if(!socketLisAdded)
                     {
                         [self addSocketListeners];
                         
                         socketLisAdded = YES;
                     }
                     
                     
                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {
                         
                         questionsSent = YES;
                         
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.areaSettTable reloadData];
                             
                             if([self.dataDef count] >= 1)
                                 [self.areaSettTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.dataDef count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                         });
                         
                         
                         self.sendTexF.text = @"";
                         
                         
                         
                         
                         self.sendBu.enabled = YES;
                         
                     });
                 }
                 
                 else
                 {
                     
                     
                     
                     NSLog(@"newMessage2 request fail reason %@",responseDict[@"reason"]);
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"newMessage2 nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"newMessage2 error : %@",error);
             
             dispatch_queue_t mainQueue = dispatch_get_main_queue();
             
             dispatch_async(mainQueue, ^(void) {
                 
                 
                 for (int i=0;i<self.dataDef.count;i++)
                 {
                     
                     chatItem*df  = self.dataDef[i];
                     
                     if(df.sendState == KSbinding)
                     {
                         df.sendState = KSerror;
                         
                         break;
                     }
                     
                 }
                 [self.areaSettTable reloadData];
                 
             });
             
             
             
             
             
         }
         
     }];
    
    
}

-(void)getMessages
{
    
    NSString*message = self.sendTexF.text;
    
    NSString*ticket = [defaults objectForKey:kticketId] ;
    
    NSString*ticketId = (ticket != nil)? ticket : @"" ;
    
    
    NSLog(@"api key : %@",[DataClassH shared].apiKey);
    
    NSLog(@"user_id : %@",[DataClassH shared].user.id);
    
    NSLog(@"message : %@",message);
    
    NSLog(@"ticket_id : %@",ticketId);
    
    NSLog(@"user_type : %@",[DataClassH shared].user.type);
    
    
    NSString *urlAsString = @"http://46.101.143.146:7894/apis/v1/getMessages";
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_id=%@&message=%@&ticket_id=%@&user_type=%@",[DataClassH shared].apiKey,@"ios",[DataClassH shared].user.id,message,ticketId,[DataClassH shared].user.type];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             NSLog(@"getMessages respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     NSLog(@"getTickets success");
                     
                     NSArray*messages = responseDict[@"messages"];
                     
                     // Create a Gregorian Calendar
                     NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                     
                     for (int i = 0; i<messages.count; i++) {
                         
                         NSDictionary*dic = [messages objectAtIndex:i];
                         
                         NSString*message = dic[@"message"];
                         
                         NSString*messageId = dic[@"message_id"];
                         
                         NSString*sender = dic[@"receiver"];
                         
                         NSString*ticketId = dic[@"ticket_id"];
                         
                         NSString*timestamp = dic[@"timestamp"];
                         
                         NSString*type = dic[@"type"];
                         
                         NSString*userId = dic[@"user_id"];
                         
                         NSString*fileName = dic[@"file_name"];
                         
                         
                         NSTimeInterval timeInterval = [timestamp doubleValue]/1000.0;
                         
                         NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                         
                         
                         long dayNumber   = [gregorianCalendar component:NSCalendarUnitWeekday fromDate:date];
                         
                         NSString*dayName = [[DataClassH shared].weekNames objectAtIndex:dayNumber-1];
                         
                         NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
                         
                         [dateformatter setDateFormat:@"dd/MM/YYYY"];
                         
                         NSString *dateString=[dateformatter stringFromDate:date];
                         
                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                         
                         formatter.AMSymbol = @"AM";
                         
                         formatter.PMSymbol = @"PM";
                         
                         [formatter setDateFormat:@"HH:mm a"];
                         
                         NSString *startTimeString = [formatter stringFromDate:date];
                         
                         NSString*lastStr = [NSString stringWithFormat:@"%@,%@",dayName,startTimeString];
                         
                         chatItem*tc = [[chatItem alloc]initWithMessage:message messageId:messageId sender:sender ticketId:ticketId type:type userId:userId date:lastStr time:startTimeString fileName:fileName sendState:KSsended];
                         
                         [self.dataDef addObject:tc];
                         
                         
                     }
                     
                     
                     [self addSocketListeners];
                     
                     [self readMessages];
                     
                     socketLisAdded = YES;
                     
                     NSLog(@"fasdfsadfsdfasdfas %@",self.dataDef);
                     
                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {
                         
                         self.loadActivity.hidden = YES;
                         
                         [self.loadActivity stopAnimating];
                         
                         [self.areaSettTable reloadData];
                         
                         if([self.dataDef count] >= 1)
                             
                             [self.areaSettTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.dataDef count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                         
                         
                     });
                 }
                 else
                 {
                     
                     
                     
                     NSLog(@"getMessages request fail reason %@",responseDict[@"reason"]);
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"getMessages nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"getMessages error : %@",error);
             
             
         }
         
     }];
    
    
}


-(void)readMessages
{
    
    NSString*ticket = [defaults objectForKey:kticketId] ;
    
    NSString*ticketId = (ticket != nil)? ticket : @"" ;
    
    NSString *urlAsString = @"http://46.101.143.146:7894/apis/v1/readMessages";
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_id=%@&ticket_id=%@&user_type=%@",[DataClassH shared].apiKey,@"ios",[DataClassH shared].user.id,ticketId,[DataClassH shared].user.type];
    
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             NSLog(@"readMessages respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     NSLog(@"readMessages success");
                     
                     
                     
                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {
                         
                         NSLog(@"readddddddddddd2112515155151");
                         
                         
                     });
                 }
                 
                 else
                 {
                     
                     
                     
                     NSLog(@"readMessages request fail reason %@",responseDict[@"reason"]);
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"readMessages nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"readMessages error : %@",error);
             
             
         }
         
     }];
    
    
}

-(void)addSocketListeners
{
    
    [[DataClassH shared].socket on:[NSString stringWithFormat:@"Ticket/%@",[defaults objectForKey:kticketId]] callback:^(NSArray* data, SocketAckEmitter* ack)
     {
         NSLog(@"socket2555744 connected : %@",data);
         
         NSLog(@"current thread : %@",[NSThread currentThread]);
         
         NSLog(@"main thread : %@",[NSThread mainThread]);
         
         AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
         
         NSDictionary*dic = [data objectAtIndex:0];
         
         NSString*message = dic[@"message"];
         
         NSString*messageId = dic[@"message_id"];
         
         NSString*sender = dic[@"receiver"];
         
         NSString*ticketId = dic[@"ticket_id"];
         
         NSString*timestamp = dic[@"timestamp"];
         
         NSString*type = dic[@"type"];
         
         NSString*userId = dic[@"user_id"];
         
         NSString*fileName = dic[@"file_name"];
         
         
         NSTimeInterval timeInterval = [timestamp doubleValue]/1000.0;
         
         
         NSDate*date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
         
         
         // Create a Gregorian Calendar
         NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
         
         
         long dayNumber   = [gregorianCalendar component:NSCalendarUnitWeekday fromDate:date];
         
         NSString*dayName = [[DataClassH shared].weekNames objectAtIndex:dayNumber-1];
         
         NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
         
         [dateformatter setDateFormat:@"dd/MM/YYYY"];
         
         NSString *dateString=[dateformatter stringFromDate:date];
         
         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
         
         formatter.AMSymbol = @"AM";
         
         formatter.PMSymbol = @"PM";
         
         [formatter setDateFormat:@"HH:mm a"];
         
         NSString *startTimeString = [formatter stringFromDate:date];
         
         NSString*lastStr = [NSString stringWithFormat:@"%@,%@",dayName,startTimeString];
         
         chatItem*tc = [[chatItem alloc]initWithMessage:message messageId:messageId sender:sender ticketId:ticketId type:type userId:userId date:lastStr time:startTimeString fileName:fileName sendState:KSsended];
         
         [self.dataDef addObject:tc];
         
         [self.areaSettTable reloadData];
         
         if([self.dataDef count] >= 1)
             [self.areaSettTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.dataDef count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
         
         [self readMessages];
         
     }];
    
}

-(void)removeSocketListeners
{
    [ [DataClassH shared].socket  off:[NSString stringWithFormat:@"Ticket/%@",[defaults objectForKey:kticketId]]];
    
}


-(void)dealloc
{
    
    [self removeSocketListeners];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    
}

@end

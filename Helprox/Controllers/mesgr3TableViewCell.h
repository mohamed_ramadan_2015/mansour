//
//  mesgrTableViewCell.h
//  PrayerNow
//
//  Created by ApprocksEg on 10/30/15.
//  Copyright © 2015 ApprocksEg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mesgr3TableViewCell : UITableViewCell
 
@property (weak, nonatomic) IBOutlet UILabel* titleLb;

@property (weak, nonatomic) IBOutlet UILabel* messNumlb;

@property (weak, nonatomic) IBOutlet UILabel* messlb;

@property (weak, nonatomic) IBOutlet UILabel* datelb;

@property (weak, nonatomic) IBOutlet UIView* topBarView;
 
@property (weak, nonatomic) IBOutlet UILabel *closedlb;


@property (weak, nonatomic) IBOutlet UIImageView *image1ImageV;

@property (weak, nonatomic) IBOutlet UIImageView *image2ImageV;

@property (weak, nonatomic) IBOutlet UIImageView *image3ImageV;

@property (strong, nonatomic) NSArray*allImages;

@end

//
//  loginViewController.h
//  sweaa
//
//  Created by ShKhan on 10/9/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loginViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *fnameTextF;
@property (weak, nonatomic) IBOutlet UITextField *lnameTextF;
@property (weak, nonatomic) IBOutlet UITextField *emailTextF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextF;
@property (weak, nonatomic) IBOutlet UIButton *submitBu;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UILabel *createAccountlb;

@property (weak, nonatomic) IBOutlet UIButton *backBu;


@property (weak, nonatomic) IBOutlet UIScrollView *logScrollView;


- (IBAction)submitClicked:(id)sender;
- (IBAction)returnFromSegueActions25:(UIStoryboardSegue*)sender;
- (IBAction)backClicked:(id)sender;

@end


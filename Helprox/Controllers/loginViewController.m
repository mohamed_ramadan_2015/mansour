//
//  loginViewController.m
//  sweaa
//
//  Created by ShKhan on 10/9/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "loginViewController.h"
//#import "FirstCustomSegueUnwind.h"
#import "splashViewController.h"
#import "settings.h"
#import "csUser.h"
#import "NSObject+BVJSONString.h"
@interface loginViewController ()
{
    NSUserDefaults*defaults;
}
@end

@implementation loginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    self.view.layer.borderWidth = 2 ;
    
    self.view.layer.borderColor = [DataClassH shared].settings.color.CGColor;
    
    _headerView.backgroundColor = [DataClassH shared].settings.color;
    
    _submitBu.backgroundColor = [DataClassH shared].settings.color;
    
    [_createAccountlb setText:@"Enter your data"];
    
     [_backBu setTitle: [NSString stringWithFormat:@"%C",0xf053] forState:UIControlStateNormal];
    
    NSLog(@"sadcfasdasd %@",[DataClassH shared].user);
    
      NSLog(@"sadcfasdasd %@",[DataClassH shared].settings);
    
    UIColor* color = [UIColor lightGrayColor];
    
 
     _fnameTextF.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"First name"
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
    
                                                 }
     ];
    
    _lnameTextF.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Last name"
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
    
                                                 }
     ];
    
    _emailTextF.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Email"
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
    
                                                 }
     ];
    
    _phoneTextF.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Phone"
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color,
    
                                                 }
     ];
    
    
    _fnameTextF.delegate = self ;
    
    _lnameTextF.delegate = self ;
    
    _emailTextF.delegate = self ;
    
    _phoneTextF.delegate = self ;
    
    
    self.submitBu.layer.cornerRadius = 7;
    
    self.logScrollView.bounces = NO;
    
    if([[DataClass getInstance] ar])
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
    }
    
    
}


/*-(void)viewWillAppear:(BOOL)animated
{
      [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
}*/

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)returnFromSegueActions25:(UIStoryboardSegue*)sender
{
    
}

- (IBAction)backClicked:(id)sender
{
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[splashViewController class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            
            
            break;
        }
    }
}

/*-(UIStoryboardSegue*)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
{
    NSLog(@"5353556556aAAAAAA");
    
    if([identifier isEqualToString:@"goBack"])
    {
        
        NSLog(@"rrrrrrr25555");
        
        return [[FirstCustomSegueUnwind alloc] initWithIdentifier:identifier source:toViewController destination:fromViewController];
    }
    
    return nil;
    
}*/

- (IBAction)submitClicked:(id)sender {
    
    
    if([_fnameTextF.text  isEqual: @""] || [_lnameTextF.text  isEqual: @""] || [_phoneTextF.text  isEqual: @""] ||[_emailTextF.text  isEqual: @""])
    {
        
        UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                           message:@"Enter all data please"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [alert show];
        
        return ;
        
    }
    
    NSLog(@"Continue sending.....");
    
    
    NSString*fname = _fnameTextF.text;
    
    NSString*lname = _lnameTextF.text;
    
    NSString*phone = _phoneTextF.text;
    
    NSString*email = _emailTextF.text;
    
    
   // IdentifiedUser*user = [[IdentifiedUser alloc] initWithId:nil firstName:fname lastName:lname phone:phone ];
    
  //  [defaults setObject:@"undefined" forKey:kUserType];
    
  //  [self createUser:user];
    
   
    
}

-(void)createUser:(IdentifiedUser*)user
{
    
    NSMutableDictionary*dic = [NSMutableDictionary new];
    
    [dic setObject:@"" forKey:@"user_id"];
    
    [dic setObject:[DataClassH shared].user.type forKey:@"type"];
    
    [dic setObject:user.firstName forKey:@"first_name"];
    
    [dic setObject:user.lastName forKey:@"last_name"];
    
    [dic setObject:user.phone forKey:@"phone"];
    
   
    NSString *urlAsString = [NSString stringWithFormat:@"%@apis/v1/createUser",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_data=%@",[DataClassH shared].apiKey,@"ios", [dic jsonString]];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             NSLog(@"createUser respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     NSLog(@"createUser success");
                 
                   
                     
                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {
                         
                         user.id = responseDict[@"user_id"];
                         
                         NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
                         
                         [defaults setObject:data forKey:kCashedUser];
                         
                         [defaults synchronize];
                         
                         
                         [DataClassH shared].user = user ;
                         
                         [self saveToken];
                         
                          UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Helprox" bundle:nil];
                         
                         UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ticketsView"];
                         
                         [self.navigationController pushViewController:vc animated:YES];
                         
                         
                     });
                 }
                 
                 else
                 {
                     
                     
                     
                     NSLog(@"createUser request fail reason %@",responseDict[@"reason"]);
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"createUser nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"createUser error : %@",error);
             
             
         }
         
     }];
    
    
}

-(void)saveToken
{
    
    NSLog(@"saveToken2001455555");
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@apis/v1/saveToken",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_type=%@&user_id=%@&fcm_token=%@",[DataClassH shared].apiKey,@"ios",[defaults valueForKey:kUserType],[DataClassH shared].user.id,[defaults valueForKey:kFCMTokenHelprox]];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             
             NSLog(@"saveToken respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     
                     
                     /*dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {
                         
                         UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                            message:@"token sent"
                                                                           delegate:nil
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil];
                         [alert show];
                         
                     });*/
                 }
                 
                 else
                 {
                     
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"saveToken nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"saveToken error : %@",error);
             
             
         }
         
     }];
    
    
}



@end

//
//  settings.h
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface settings : NSObject

@property (nonatomic,strong) NSString*title;

@property (nonatomic,strong) UIColor*color;

@property   (nonatomic,strong) NSNumber* active;

@property (nonatomic,strong) NSArray*csUsers;

-(settings*)initWithTitle:(NSString*)title color:(NSString*)color active:(NSNumber*)active csUsers:(NSArray*)csUsers;
@end

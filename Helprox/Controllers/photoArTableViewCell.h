//
//  photoTableViewCell.h
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface photoArTableViewCell : UITableViewCell

@property (weak, nonatomic)  IBOutlet  UILabel*   nameLb;
@property (weak, nonatomic)  IBOutlet  UIImageView*   profileImageV;

@property (weak, nonatomic) IBOutlet UIImageView *sendedImageV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@end

//
//  UserItem.h
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IdentifiedUser : NSObject<NSCoding>

@property (nonatomic,strong) NSString* id;
@property (nonatomic,strong) NSString* firstName;
@property (nonatomic,strong) NSString* lastName;
@property (nonatomic,strong) NSString* phone;
@property (nonatomic,strong) NSString* type;


-(IdentifiedUser*)initWithId:(NSString*)id
                 firstName:(NSString*)firstName
                  lastName:(NSString*)lastName
                     phone:(NSString*)phone
                      type:(NSString*)type;

@end


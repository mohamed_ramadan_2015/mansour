//
//  questionsViewController.h
//  Mansour
//
//  Created by ShKhan on 11/23/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface questionsViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UITextField *fnameTexf;


@property (weak, nonatomic) IBOutlet UITextField *lnameTexf;


@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UILabel *createConlb;

@property (weak, nonatomic) IBOutlet UIButton *sendBu;


@property (weak, nonatomic) IBOutlet UITableView *areaSettTable;

@property (weak, nonatomic) IBOutlet UILabel *projectNamelb;

@property (weak, nonatomic) IBOutlet UILabel *arrowlb;

@property (weak, nonatomic) IBOutlet UIButton *projectNameBu;

@property (weak, nonatomic) IBOutlet UIView *chatNameView;



@property (weak, nonatomic) IBOutlet UITextField *chatTexF;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *areaTableHCon;




- (IBAction)projectNameClicked:(id)sender;

- (IBAction)sendClicked:(id)sender;

- (IBAction)backClicked:(id)sender;

@end

//
//  settings.m
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//


#import "chatItem.h"
  

@implementation chatItem


-(chatItem*)initWithMessage:(NSString*)message messageId:(NSString*)messageId sender:(NSString*)sender ticketId:(NSString*)ticketId type:(NSString*)type userId:(NSString*)userId date:(NSString*)date time:(NSString*)time fileName:(NSString*)fileName sendState:(sendState)sendState
{
    self.message = message;
    
    self.messageId = messageId;
 
    self.sender = sender;
    
    self.ticketId = ticketId;
    
    self.type = type;
    
    self.userId = userId;
    
    self.date = date;
    
    self.time = time;
    
    self.fileName = fileName;
    
    self.sendState = sendState;
    
    return self;
}

 
- (NSString*)description
{
    return [NSString stringWithFormat:@"MyClass:%@ %@ %@ %@ %@ %@ %@ %@ %@ %ld", self.message,self.messageId,self.sender,self.ticketId,self.type,self.userId,self.date,self.time,self.fileName,(long)self.sendState];
}
 


@end

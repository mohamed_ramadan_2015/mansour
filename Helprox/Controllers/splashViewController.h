//
//  splashViewController.h
//  Helprox
//
//  Created by ShKhan on 10/12/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface splashViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *messageImageV;

- (IBAction)userDataClicked:(id)sender;
- (IBAction)clearDataClicked:(id)sender;

@end

//
//  questionsViewController.m
//  Mansour
//
//  Created by ShKhan on 11/23/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "questionsViewController.h"
#import "DataClassH.h"
#import "proChatItem.h"
#import "dropTableViewCell.h"

static NSString *CellIdentifier1 = @"ContentCell1";
@interface questionsViewController ()
{
    NSUserDefaults*defaults;
    
    BOOL animationRunn;
    
    BOOL once;
}
@end

@implementation questionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    animationRunn = NO;
    
    once = YES;
    
    self.projectNamelb.text = NSLocalizedString(@"queProjectName", "");
    
    self.createConlb.text = NSLocalizedString(@"queCreateConvers", "");
    
    
    self.projectNameBu.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.projectNameBu.layer.borderWidth = 1;
    
    self.projectNameBu.layer.cornerRadius = 7;
    
    
    self.chatNameView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.chatNameView.layer.borderWidth = 1;
    
    self.chatNameView.layer.cornerRadius = 7;
    
    
    
    self.areaSettTable.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.areaSettTable.layer.borderWidth = 1;
    
    
    
    NSLog(@"current Sett %@",[DataClassH shared].settings);
    
    NSLog(@"current user %@",[DataClassH shared].user);
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSBundle* frameworkBundle = [NSBundle bundleForClass:[self class]];
    
    UINib *nib6 = [UINib nibWithNibName:@"dropTableViewCell" bundle:frameworkBundle];
    
    [self.areaSettTable registerNib:nib6 forCellReuseIdentifier:CellIdentifier1];
    
    
    [self.areaSettTable setBounces:NO];
    
    self.areaSettTable.delegate=self;
    
    self.areaSettTable.dataSource=self;
    
    self.areaSettTable.backgroundColor = [UIColor blueColor];
    
    self.areaSettTable.opaque = NO;
    
    self.areaSettTable.separatorColor = [UIColor clearColor];
    
    self.areaSettTable.showsVerticalScrollIndicator = NO;
    
    _chatTexF.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"queChatTitle", "")
                                    attributes:@{
                                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                                 
                                                 }
     ];
    
    _chatTexF.delegate = self ;
    
    self.arrowlb.text = [NSString stringWithFormat:@"%C",0xf078];
   
    self.view.layer.borderWidth = 2 ;
    
    self.view.layer.borderColor = [DataClassH shared].settings.color.CGColor;
    
    _headerView.backgroundColor = [DataClassH shared].settings.color;
    
    _sendBu.backgroundColor = [DataClassH shared].settings.color;
    
    [_sendBu setTitle:NSLocalizedString(@"queSend", "") forState:UIControlStateNormal];
    
     [_backBu setTitle: [NSString stringWithFormat:@"%C",0xf053] forState:UIControlStateNormal];
    
     self.sendBu.layer.cornerRadius = 7;
    
    if([[DataClass getInstance] ar])
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
    }
    
}

-(void)viewDidLayoutSubviews
{
   if(once)
   {
    
       self.areaSettTable.tag = 1;
       
       [self manageDrop:NO];
       
       once = NO;
   }
    
    
}

-(void)manageDrop:(BOOL)animate
{
    if(animationRunn)
    {
        return ;
    }
    
    animationRunn = YES;
    
    if(self.areaSettTable.tag == 0)
    {
        if([[DataClass getInstance].projectsChatArr count] <= 5)
        {
              self.areaTableHCon.constant = [[DataClass getInstance].projectsChatArr count] * 50.0;
        }
        else
        {
              self.areaTableHCon.constant = 300 ;
            
        }
       
        
         self.arrowlb.text = [NSString stringWithFormat:@"%C",0xf077];
        
        self.areaSettTable.tag = 1;
    }
    else
    {
         self.areaTableHCon.constant = 0 ;
        
         self.arrowlb.text = [NSString stringWithFormat:@"%C",0xf078];
        
        self.areaSettTable.tag = 0;
    }
    
    
    
    if(animate)
    {
       
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
            animationRunn = NO;
            
        }];
    }
    else
    {
         [self.view layoutIfNeeded];
        animationRunn = NO;
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.areaSettTable])
    {
        
        return [[DataClass getInstance].projectsChatArr count];
        
    }
    
    return 0;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath      *)indexPath
{
    
    
    if ([tableView isEqual:self.areaSettTable])
    {
        
        
        proChatItem* cr = [[DataClass getInstance].projectsChatArr objectAtIndex:indexPath.row];
        
        dropTableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
        
        if([DataClass getInstance].ar)
        {
            cell.titleLb.text = [NSString stringWithFormat:@"%@",cr.textAr];
        }
        else
        {
            cell.titleLb.text = [NSString stringWithFormat:@"%@",cr.textEn];
        }
        
        return cell;
        
    }
    
    return nil;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([tableView isEqual:self.areaSettTable])
        
    {
        proChatItem* cr = [[DataClass getInstance].projectsChatArr objectAtIndex:indexPath.row];
        
        
        if([DataClass getInstance].ar)
        {
            self.projectNamelb.text = [NSString stringWithFormat:@"%@",cr.textAr];
        }
        else
        {
            self.projectNamelb.text = [NSString stringWithFormat:@"%@",cr.textEn];
        }
        
        [self manageDrop:YES];
        
    }
    
    
}

 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return 50;
 }
 




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)projectNameClicked:(id)sender {
    
    [self manageDrop:YES];
}

- (IBAction)sendClicked:(id)sender
{
    
    if([self.projectNamelb.text isEqualToString:NSLocalizedString(@"queProjectName",@"")])
    {
        UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                           message:NSLocalizedString(@"queSelectProj", "")
                                                          delegate:nil
                                                 cancelButtonTitle:NSLocalizedString(@"langAlertOk", "")
                                                 otherButtonTitles:nil];
        [alert show];
        
        return;
        
    }
    
 
    NSString *str = self.chatTexF.text ;
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[str stringByTrimmingCharactersInSet: set] length] == 0)
    {
        UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                           message:NSLocalizedString(@"queEnterChatTi", "")
                                                          delegate:nil
                                                 cancelButtonTitle:NSLocalizedString(@"langAlertOk", "")
                                                 otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    

    [defaults setObject:self.projectNamelb.text forKey:kProjectName];
    
    [defaults setObject:[self.chatTexF.text stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]  forKey:kChatTitle];
    
    [defaults setObject:nil forKey:kticketId];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Helprox" bundle:nil];
    
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"messengerView"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
   
}

- (IBAction)backClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end

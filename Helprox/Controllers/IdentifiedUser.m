//
//  UserItem.m
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "IdentifiedUser.h"

@implementation IdentifiedUser

-(IdentifiedUser*)initWithId:(NSString*)id
                 firstName:(NSString*)firstName
                  lastName:(NSString*)lastName
                     phone:(NSString *)phone
                      type:(NSString*)type
               {
                    
    self.id = id;
    self.firstName = firstName;
    self.lastName = lastName;
    self.type = type;
    self.phone = phone;
   
    
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.id forKey:@"id"];
    [encoder encodeObject:self.firstName forKey:@"firstName"];
    [encoder encodeObject:self.lastName forKey:@"lastName"];
    [encoder encodeObject:self.type forKey:@"type"];
    [encoder encodeObject:self.phone forKey:@"phone"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.id = [decoder decodeObjectForKey:@"id"];
        self.firstName = [decoder decodeObjectForKey:@"firstName"];
        self.lastName = [decoder decodeObjectForKey:@"lastName"];
        self.type = [decoder decodeObjectForKey:@"type"];
        self.phone = [decoder decodeObjectForKey:@"phone"];
    }
    return self;
}

-(NSString*) description
{
  return [NSString stringWithFormat:@"qwqwqwqw  %@== %@== %@== %@== %@==",self.id,self.firstName,self.lastName,self.type,self.phone];
    
}
@end







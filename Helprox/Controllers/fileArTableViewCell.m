//
//  photoTableViewCell.m
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "fileArTableViewCell.h"

@implementation fileArTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
     self.borderView.layer.cornerRadius = 8;
    
    self.fileUrl .userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = \
    [[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(didTapLabelWithGesture:)];
    [self.fileUrl addGestureRecognizer:tapGesture];
  
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture
{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.fileUrl.text]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)link1Clicked:(id)sender {
}

- (IBAction)link2Clicked:(id)sender {
}
@end

//
//  NSObject+BVJSONString.h
//  Helprox
//
//  Created by ShKhan on 10/12/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (BVJSONString)

-(NSString*) jsonString;

@end

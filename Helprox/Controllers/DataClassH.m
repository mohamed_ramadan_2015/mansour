//
//  DataClass.m
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "DataClassH.h"
#import <SystemConfiguration/SystemConfiguration.h>


@implementation DataClassH

+ (instancetype) shared {
    static DataClassH *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[[self class] alloc] init];
        
        NSUserDefaults*defaults = [NSUserDefaults standardUserDefaults];
        
        shared.apiKey = @"b17e6ae1063cc9283b136eb95eb7cecf";
        
        NSData *data1 = [defaults objectForKey:ksettings];
        
        settings *set = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
        
        shared.settings = set;
        
        NSData *data2 = [defaults objectForKey:kCashedUser];
        
        IdentifiedUser *user = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
        
        shared.user = user;
        
        shared.weekNames=[[NSArray alloc]initWithObjects:NSLocalizedString(@"SamoonDay1", nil),NSLocalizedString(@"SamoonDay2", nil),NSLocalizedString(@"SamoonDay3", nil),NSLocalizedString(@"SamoonDay4", nil),NSLocalizedString(@"SamoonDay5", nil),NSLocalizedString(@"SamoonDay6", nil),NSLocalizedString(@"SamoonDay7", nil), nil];
        
        
        shared.unReadCount = 0;
    });
    return shared;
}

+(void)clearUser
{
    

    NSUserDefaults*defaults =  [[NSUserDefaults alloc] initWithSuiteName:@"com.Approcks.Helprox"];

    [defaults setObject:nil forKey:kCashedUser];
    
    [defaults synchronize];
    
    [DataClassH shared].user = nil;
    
}
+(void)clearSettings
{

    NSUserDefaults*defaults =  [[NSUserDefaults alloc] initWithSuiteName:@"com.Approcks.Helprox"];
    
    [defaults setObject:nil forKey:ksettings];
    
    [defaults synchronize];
    
    [DataClassH shared].settings = nil;
}

+(void)clearAll
{

    NSUserDefaults*defaults =  [[NSUserDefaults alloc] initWithSuiteName:@"com.Approcks.Helprox"];
    
    [defaults setObject:nil forKey:kCashedUser];
    
    [defaults  setObject:nil forKey:ksettings];
    
    [defaults synchronize];
    
    [DataClassH shared].user = nil;
    
    [DataClassH shared].settings = nil;
}



+(bool)isNetworkAvailable {
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com");
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    return canReach;
}

@end

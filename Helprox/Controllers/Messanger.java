public void SocketOnNewMessage(){
        Log.d(TAG,"SocketOnNewMessage");
        SocketOffNewMessage();
        for(MessangerItem messangerItem : list) {
            mSocket.on("Ticket/" + messangerItem.getId(), new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    for (int i = 0; i < args.length; i++) {
                       
                        ConversationItem conversationItem = new GsonBuilder().create().fromJson(args[i].toString(), ConversationItem.class);
                        updateTicket(conversationItem);
                    }
                }
            });
        }
    }
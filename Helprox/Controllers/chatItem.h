//
//  settings.h
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum : NSInteger {
    KSbinding,
    KSerror,
    KSsended
} sendState;


@interface chatItem : NSObject

@property (nonatomic,strong) NSString*message;

@property (nonatomic,strong) NSString* messageId;

@property (nonatomic,strong) NSString*sender;

@property (nonatomic,strong) NSString*ticketId;

@property (nonatomic,strong) NSString*type;

@property (nonatomic,strong) NSString*userId;

@property (nonatomic,strong) NSString*date;

@property (nonatomic,strong) NSString*time;

@property (nonatomic,strong) NSString*fileName;

@property  (nonatomic,assign) sendState sendState;

-(chatItem*)initWithMessage:(NSString*)message messageId:(NSString*)messageId sender:(NSString*)sender ticketId:(NSString*)ticketId type:(NSString*)type userId:(NSString*)userId date:(NSString*)date time:(NSString*)time fileName:(NSString*)fileName sendState:(sendState)sendState;
@end

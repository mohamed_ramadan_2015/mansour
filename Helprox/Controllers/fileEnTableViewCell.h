//
//  photoTableViewCell.h
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface fileEnTableViewCell : UITableViewCell


@property (weak, nonatomic)  IBOutlet  UILabel*   nameLb;
@property (weak, nonatomic)  IBOutlet  UIImageView*   profileImageV;



@property (weak, nonatomic) IBOutlet UILabel *fileUrl;
@property (weak, nonatomic) IBOutlet UILabel *fileName;

@property (weak, nonatomic) NSString *messageId;


@end

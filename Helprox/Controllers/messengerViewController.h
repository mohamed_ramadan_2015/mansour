//
//  loginViewController.h
//  sweaa
//
//  Created by ShKhan on 10/9/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ticketItem.h"
@interface messengerViewController : UIViewController <UITableViewDataSource,UITableViewDelegate , UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backBu;
@property (weak, nonatomic) IBOutlet UIButton *titleBu;
@property (weak, nonatomic) IBOutlet UIButton *closeBu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendViewBottomCon;
@property (weak, nonatomic) IBOutlet UIButton *sendBu;
@property (weak, nonatomic) IBOutlet UIButton *photoBu;
@property (weak, nonatomic) IBOutlet UIButton *textBu;
@property (weak, nonatomic) IBOutlet UITextField *sendTexF;
@property (weak, nonatomic) IBOutlet UITableView *areaSettTable;
@property (weak, nonatomic) IBOutlet UIImageView *userImageV;
@property (weak, nonatomic) IBOutlet UILabel *userlb;

@property (weak, nonatomic) IBOutlet UILabel *asklb;

@property (weak, nonatomic) IBOutlet UILabel *backLaterlb;
@property (weak, nonatomic) IBOutlet UILabel *laterIconlb;
@property (weak, nonatomic) IBOutlet UIView *supportView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewTopCon;

@property (weak, nonatomic) IBOutlet UIView *backLaterView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *suppHCon;

@property (weak, nonatomic) IBOutlet UIView *chatView;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatViewTopCon;

@property BOOL status;

@property (strong, nonatomic) IBOutlet ticketItem *currentTicket;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadActivity;



@property (weak, nonatomic) IBOutlet UIImageView *btImage1View;

@property (weak, nonatomic) IBOutlet UIImageView *btImage2View;

@property (weak, nonatomic) IBOutlet UIImageView *btImage3View;

@property (weak, nonatomic) IBOutlet UILabel *bt1lb;

@property (weak, nonatomic) IBOutlet UILabel *bt2lb;


@property (weak, nonatomic) IBOutlet UILabel *bt3lb;




@property (weak, nonatomic) IBOutlet UIImageView *tpImage1View;

@property (weak, nonatomic) IBOutlet UIImageView *tpImage2View;

@property (weak, nonatomic) IBOutlet UIImageView *tpImag3View;


@property (weak, nonatomic) IBOutlet UILabel *supportlb;

@property (weak, nonatomic) IBOutlet UILabel *btRespondSoolb;

@property (weak, nonatomic) IBOutlet UILabel *tpRespondSoolb;

@property (weak, nonatomic) IBOutlet UIButton *supportBu;


@property (weak, nonatomic) IBOutlet UIView *topView;


@property (weak, nonatomic) IBOutlet UILabel *proNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;


- (IBAction)supportClicked:(id)sender;

- (IBAction)backClicked:(id)sender;
- (IBAction)closeClicked:(id)sender;
- (IBAction)sendClicked:(id)sender;
- (IBAction)photoClicked:(id)sender;
- (IBAction)textClicked:(id)sender;
- (IBAction)titleClicked:(id)sender;

@end


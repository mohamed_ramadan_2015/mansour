//
//  photoTableViewCell.h
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface fileArTableViewCell : UITableViewCell


@property (weak, nonatomic)  IBOutlet  UILabel*   nameLb;
@property (weak, nonatomic)  IBOutlet  UIImageView*   profileImageV;



@property (weak, nonatomic) IBOutlet UILabel *fileUrl;
@property (weak, nonatomic) IBOutlet UILabel *fileName;

@property (weak, nonatomic) NSString *messageId;

@property (weak, nonatomic) IBOutlet UIView *borderView;

@property (weak, nonatomic) IBOutlet UIImageView *sendedImageV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;


@property (weak, nonatomic) IBOutlet UIButton *link1Bu;

@property (weak, nonatomic) IBOutlet UIButton *link2Bu;

- (IBAction)link1Clicked:(id)sender;


- (IBAction)link2Clicked:(id)sender;


@end

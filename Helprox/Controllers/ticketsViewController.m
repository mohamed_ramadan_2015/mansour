//
//  ticketsViewController.m
//  sweaa
//
//  Created by ShKhan on 10/9/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "ticketsViewController.h"
#import "loginViewController.h"
#import "mesgrTableViewCell.h"
#import "splashViewController.h"
#import "NSObject+BVJSONString.h"
#import "ticketItem.h"
#import "mesgr3TableViewCell.h"
#import "downloadItem.h"
#import "messengerViewController.h"
#import <AudioToolbox/AudioToolbox.h> 
static NSString *CellIdentifier1 = @"ContentCell";

static NSString *CellIdentifier2 = @"ContentCell1";
@interface ticketsViewController ()
{
    NSUserDefaults*defaults;
    
    
    
    NSInteger totalUnread;
    
    NSString*filePathC;
    
    NSMutableArray*allDownloads;
    
    NSInteger tUnread;
}

@property(nonatomic,strong)NSMutableArray*dataDef;

@end

@implementation ticketsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    tUnread = 0 ;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"refreshImagesYaTickets"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"refreshDataYaTickets"
                                               object:nil];
    
    
    self.dataDef = [NSMutableArray new];
    allDownloads  = [NSMutableArray new];
    totalUnread = 0;
    
    NSLog(@"current Sett %@",[DataClassH shared].settings);
    
    NSLog(@"current user %@",[DataClassH shared].user);
    
    self.view.layer.borderWidth = 2 ;
    
    self.view.layer.borderColor = [DataClassH shared].settings.color.CGColor;
    
    _headerView.backgroundColor = [DataClassH shared].settings.color;
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    _conversationslb.text  =  NSLocalizedString(@"tickConversations", "") ;
    
    // [_compNamelb setText:@"with Approcks"];
    
    [_createConBu setTitle: [NSString stringWithFormat:@"%C",0xf044] forState:UIControlStateNormal];
    
    [_createConBu setBackgroundColor:[DataClassH shared].settings.color];
    
    [_backBu setTitle: [NSString stringWithFormat:@"%C",0xf053] forState:UIControlStateNormal];
    
    [_closeBu setTitle: [NSString stringWithFormat:@"%C",0xf00d] forState:UIControlStateNormal];
    
    NSBundle* frameworkBundle = [NSBundle bundleForClass:[self class]];
    
    UINib *nib1 = [UINib nibWithNibName:@"mesgrTableViewCell" bundle:frameworkBundle];
    
    [self.areaSettTable registerNib:nib1 forCellReuseIdentifier:CellIdentifier1];
    
    UINib *nib2 = [UINib nibWithNibName:@"mesgr3TableViewCell" bundle:frameworkBundle];
    
    [self.areaSettTable registerNib:nib2 forCellReuseIdentifier:CellIdentifier2];
    
    
    [self.areaSettTable setBounces:NO];
    
    self.areaSettTable.delegate=self;
    
    self.areaSettTable.dataSource=self;
    
    // self.areaSettTable.backgroundColor = [UIColor lightGrayColor];
    
    self.areaSettTable.opaque = NO;
    
    self.areaSettTable.separatorColor = [UIColor clearColor];
    
    self.areaSettTable.estimatedRowHeight = 200;
    
    self.areaSettTable.rowHeight = UITableViewAutomaticDimension;
    
    self.areaSettTable.showsVerticalScrollIndicator = NO;
    
    [self.loadActivity startAnimating];
    
    if([DataClass isNetworkAvailable])
    {
        [self getTickets];
        
        //  [self sendMessageImage];
    }
    else
    {
        
        UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                           message:NSLocalizedString(@"internetMessage", nil)
                                                          delegate:self
                                                 cancelButtonTitle:NSLocalizedString(@"langAlertTry", nil)
                                                 otherButtonTitles:nil];
        [alert show];
    }
    
    if([[DataClass getInstance] ar])
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
    }
    
    NSError*error=nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/Application Support"];
    
    filePathC = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Helprox/CSInfo"]];
    
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:filePathC])
    {
        if(
           
           [[NSFileManager defaultManager] createDirectoryAtPath:filePathC
                                     withIntermediateDirectories:YES
                                                      attributes:nil
                                                           error:&error])
        {
            NSLog(@"created");
        }
        else
        {
            NSLog(@"not created");
        }
    }
    else
    {
        NSLog(@"exists");
    }
    
    
    if([DataClassH shared].socket == nil && [DataClassH shared].user != nil )
    {
        
        NSLog(@"setttttttttttting in ticketsViewController");
        
        NSURL* url = [[NSURL alloc] initWithString:@"http://46.101.143.146:7071/"];
        
        [DataClassH shared].socket = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
        
        
        [ [DataClassH shared].socket connect];
       /*
        NSDictionary*wer = @{@"id":[DataClassH shared].user.id,@"type":[DataClassH shared].user.type};
        [ [DataClassH shared].socket emit:@"auth" with:@[[wer jsonString]]];
       */
    }
    else
    {
        NSLog(@"connected255 socket in ticketsViewController");
        
        NSDictionary*wer = @{@"id":[DataClassH shared].user.id,@"type":[DataClassH shared].user.type};
        
        [ [DataClassH shared].socket emit:@"auth" with:@[[wer jsonString]]];
        
    }
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    
    [self removeSocketListeners];
    
}

-(void)receiveNotification:(NSNotification *)paramNotification
{
    
    
    if([paramNotification.name isEqualToString:@"refreshImagesYaTickets"] && [paramNotification.object  isEqualToString:@"tickets"])
    {
        NSLog(@"654656655sdaasdasd");
        
        [self.areaSettTable reloadData];
    }
    else if([paramNotification.name isEqualToString:@"refreshDataYaTickets"])
    {
        
        self.loadActivity.hidden = NO ;
        
        [self.loadActivity startAnimating];
        
        [self.dataDef removeAllObjects];
        
        [self.areaSettTable reloadData];
        
        self.unReadMesslb.text = @"";
        
        if([DataClass isNetworkAvailable])
        {
            [self getTickets];
            
        }
        else
        {
            
            UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                               message:NSLocalizedString(@"internetMessage", nil)
                                                              delegate:self
                                                     cancelButtonTitle:NSLocalizedString(@"langAlertTry", nil)
                                                     otherButtonTitles:nil];
            [alert show];
        }
        
    }
    
}
-(void)viewDidLayoutSubviews
{
    
    self.createConBu.layer.cornerRadius = self.createConBu.frame.size.width/2 ;
    
    /* self.unReadViewHCon.constant = 0;
     
     [self.view layoutIfNeeded];*/
    
}

-(void)dealloc
{
    
    [self removeSocketListeners];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.areaSettTable])
    {
        
        return [self.dataDef count];
        
    }
    
    return 0;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath      *)indexPath
{
    if ([tableView isEqual:self.areaSettTable]) {
        
        ticketItem*tc = self.dataDef[indexPath.row];
        
        if([tc.profilePic isEqualToString:@""]) {
            mesgr3TableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier2];
            
            
            for (int i = 0 ;i< [[DataClassH shared].settings.csUsers count];i++)
            {
                
                csUser*us = [[DataClassH shared].settings.csUsers objectAtIndex:i];
                
                NSString *getImagePath = [filePathC stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",us.id]];
                
                if([[NSFileManager defaultManager] fileExistsAtPath:getImagePath])
                {
                    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
                    
                    UIImageView*im =  [[cell allImages] objectAtIndex:i];
                    
                    im.image = img;
                    
                }
                else
                {
                    BOOL download  = YES;
                    
                    for (downloadItem*dwi in allDownloads) {
                        
                        if(dwi.id == us.id)
                        {
                            download = NO ;
                            
                        }
                        
                    }
                    
                    if(download)
                    {
                        downloadItem*dw = [[downloadItem alloc]initWithId:us.id url:us.imageUrl sender:@"tickets"];
                        
                        [allDownloads addObject:dw];
                        
                        [dw startDownload];
                    }
                    
                }
                
            }
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.messNumlb.layer.cornerRadius = cell.messNumlb.frame.size.width/2;
            
            cell.messNumlb.clipsToBounds = YES;
            
            cell.messlb.textColor = [DataClassH shared].settings.color;
            
            cell.messNumlb.backgroundColor = [DataClassH shared].settings.color;
            
            // change to conversation title
            cell.titleLb.text =tc.project;
            
            cell.messlb.text = tc.lastMessage;
            
            cell.messNumlb.text = tc.unreadCount;
            
            cell.datelb.text = tc.date;
            
            if(tc.status == true) {
                cell.closedlb.hidden = YES;
            } else {
                cell.closedlb.hidden = NO;
            }
            
            if(indexPath.row == 0) {
                cell.topBarView.hidden = YES;
            } else {
                cell.topBarView.hidden = NO;
            }
            
            cell.backgroundColor=[UIColor whiteColor];
            
            [cell layoutSubviews];
            
            [cell layoutIfNeeded];
            
            return cell;
            
        } else {
            
            mesgrTableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier1];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.messNumlb.layer.cornerRadius = cell.messNumlb.frame.size.width/2;
            
            cell.messNumlb.clipsToBounds = YES;
            
            cell.messlb.textColor = [DataClassH shared].settings.color;
            
            cell.messNumlb.backgroundColor = [DataClassH shared].settings.color;
            
            // change to conversation title
            cell.titleLb.text =tc.project;
            
            cell.messlb.text = tc.lastMessage;
            
            cell.messNumlb.text = tc.unreadCount;
            
            cell.datelb.text = tc.date;
            
            
            if(tc.status == true) {
                cell.closedlb.hidden = YES;
            } else  {
                cell.closedlb.hidden = NO;
            }
            
            if(indexPath.row == 0) {
                cell.topBarView.hidden = YES;
            } else {
                cell.topBarView.hidden = NO;
            }
            
            cell.backgroundColor=[UIColor whiteColor];
            
            [cell layoutSubviews];
            
            [cell layoutIfNeeded];
            
            return cell;
        }
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.areaSettTable])  {
        
        ticketItem*tc = [self.dataDef objectAtIndex:indexPath.row];
        
        [defaults setObject:tc.id forKey:kticketId];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Helprox" bundle:nil];
        
        messengerViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"messengerView"];
        
        vc.status = tc.status;
        
        //  tc.profileId = @"5F09DalEKL";
        
        // tc.profilePic = @"sdfasfsf";
        
        vc.currentTicket = tc;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

/*
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return 72;
 }
 
 */



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)newConClicked:(id)sender
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Helprox" bundle:nil];
    
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"questionsView"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

- (IBAction)backClicked:(id)sender {
    
    [ [DataClassH shared].socket emit:@"auth" with:@[@"test socket"]];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeClicked:(id)sender {
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[loginViewController class]] || [controller isKindOfClass:[splashViewController class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            break;
        }
    }
}

- (IBAction)sendClicked:(id)sender {
}

- (IBAction)photoClicked:(id)sender {
 
}

- (IBAction)textClicked:(id)sender {
   
}


-(void)getTickets
{
    
    NSData *data2 = [defaults objectForKey:kCashedUser];
    
    IdentifiedUser *user = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
    
    NSMutableDictionary*dic = [NSMutableDictionary new];
    
    [dic setObject:user.id forKey:@"user_id"];
    
    [dic setObject:[DataClassH shared].user.type forKey:@"type"];
    
    [dic setObject:user.firstName forKey:@"first_name"];
    
    [dic setObject:user.lastName forKey:@"last_name"];
    
    [dic setObject:user.phone forKey:@"phone"];
    
    
    NSString *urlAsString = @"http://46.101.143.146:7894/apis/v1/getTickets";
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_data=%@",[DataClassH shared].apiKey,@"ios", [dic jsonString]];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             NSLog(@"getTickets respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     NSLog(@"getTickets success");
                     
                     tUnread = [responseDict[@"total_unread_count"] integerValue];
                     
                     NSArray*tickets = responseDict[@"tickets"];
                     
                     for (int i = 0; i<tickets.count; i++) {
                         
                         NSDictionary*dic = [tickets objectAtIndex:i];
                         
                         NSString*id = dic[@"id"];
                         
                         NSString*lastMessage = dic[@"last_message"];
                         
                         NSString*profilePic = dic[@"profile_pic"];
                         
                         NSString*profileName = dic[@"profile_name"];
                         
                         NSString*profileId= dic[@"profile_id"];
                         
                         NSString*respond = [dic[@"respond"] stringValue];
                         
                         NSString*timestamp = dic[@"timestamp"];
                         
                         NSString*unreadCount = [dic[@"unread_count"] stringValue];
                         
                         NSString*title;
                         NSString*project;
                         
                         if ([dic[@"title"] isKindOfClass:[NSNull class]]) {
                             title = @"";
                             project = @"";
                         } else {
                             title = dic[@"title"];
                             project = dic[@"project"];
                         }
                         
                         bool status = [dic[@"status"] boolValue];
                         
                         NSTimeInterval timeInterval = [timestamp doubleValue]/1000.0;
                         
                         NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                         
                         NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
                         
                         [dateformatter setDateFormat:@"dd/MM/YYYY"];
                         
                         NSString *dateString=[dateformatter stringFromDate:date];
                         
                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                         [formatter setDateFormat:@"HH:mm"];
                         
                         NSString *startTimeString = [formatter stringFromDate:date];
                         
                         ticketItem*tc = [[ticketItem alloc]initWithId:id lastMessage:lastMessage profilePic:profilePic respond:respond date:dateString time:startTimeString unreadCount:unreadCount status:status profileName:profileName profileId:profileId project:project title:title];
                         
                         [self.dataDef addObject:tc];
                     }
                     
                     [self addSocketListeners];
                     
                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {
                         
                         self.loadActivity.hidden = YES;
                         
                         [self.loadActivity stopAnimating];
                         
                         if(tUnread == 0)
                         {
                             self.unReadMesslb.text =  NSLocalizedString(@"tickNoUnreadMess", "");
                         }
                         else
                         {
                             self.unReadMesslb.text = [NSString stringWithFormat:@"%ld %@",(long)tUnread,NSLocalizedString(@"tickUnreadMess", "")] ;
                             
                         }
                         
                         
                         
                         [self.areaSettTable reloadData];
                         
                         
                         
                     });
                 }
                 
                 else
                 {
                     
                     
                     
                     NSLog(@"getTickets request fail reason %@",responseDict[@"reason"]);
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"getTickets nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"getTickets error : %@",error);
             
             
         }
         
     }];
    
    
}


-(void)sendMessageImage
{
    
    //apiKey", "platform", "user_type", "user_id
    
    // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    
    [_params setObject:[DataClassH shared].apiKey forKey:@"apiKey"];
    [_params setObject:@"ios" forKey:@"platform"];
    [_params setObject:[DataClassH shared].user.type forKey:@"user_type"];
    [_params setObject:[DataClassH shared].user.id forKey:@"user_id"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    
    
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:@"http://46.101.143.146:7894/apis/v1/newMessage"];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in [_params allKeys]) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    NSBundle* frameworkBundle = [NSBundle bundleForClass:[self class]];
    
    NSLog(@"fffffff : %@ ",frameworkBundle);
    
    NSString*filename=@"word.doc";
    
    NSString*path= [frameworkBundle pathForResource:@"word" ofType:@"doc"];
    
    NSLog(@"file name is : %@",path);
    
    NSString* FileParamConstant = [NSString stringWithFormat:@"file"];
    
    NSData *imageData = [NSData dataWithContentsOfFile:path];
    
    if (imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", FileParamConstant,filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    
    
    //****
    
    NSLog(@"Sending....");
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             
             NSLog(@"sendMessageImage respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     
                     
                     
                     NSLog(@"sendMessageImage  success");
                     
                     if(responseDict[@"ticket_id"] != nil)
                     {
                         [defaults setObject:responseDict[@"ticket_id"] forKey:kticketId];
                     }
                     
                     
                     
                 }
                 
                 else
                 {
                     
                     
                     
                 }
                 
                 
                 
             }
             else
             {
                 NSLog(@"sendMessageImage Dic ");
             }
             
         }
         else
         {
             NSLog(@"sendMessageImage error  tt: %@",error);
             
             
         }
         
     }];
    
    
    
    
}

-(void)addSocketListeners
{
    
    for (ticketItem*tc in self.dataDef)
    {
        
        NSLog(@"observer1111 added : %@",tc.id);
        
        [ [DataClassH shared].socket on:[NSString stringWithFormat:@"Ticket/%@/out",tc.id] callback:^(NSArray* data, SocketAckEmitter* ack)
         {
             
             AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
             
             NSLog(@"socket2555744 connected : %@",data);
             
             NSLog(@"current thread : %@",[NSThread currentThread]);
             
             NSLog(@"main thread : %@",[NSThread mainThread]);
             
             NSDictionary*dic = [data objectAtIndex:0];
             
             NSString*message = dic[@"message"];
             
             NSLog(@"message2231212121212 : %@",message);
             
             NSString*ticketId = dic[@"ticket_id"];
             
             NSString*timestamp = dic[@"timestamp"];
             
             NSTimeInterval timeInterval = [timestamp doubleValue]/1000.0;
             
             NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
             
             NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
             
             [dateformatter setDateFormat:@"dd/MM/YYYY"];
             
             NSString *dateString=[dateformatter stringFromDate:date];
             
             
             NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
             [formatter setDateFormat:@"HH:mm"];
             
             NSString *startTimeString = [formatter stringFromDate:date];
             
             
             NSString*profilePic = dic[@"profile_pic"];
             
             NSString*profileName = dic[@"profile_name"];
             
             NSString*profileId= dic[@"profile_id"];
             
             NSString*respond = [dic[@"respond"] stringValue];
             
             NSString*unreadCount = [dic[@"unread_count"] stringValue];
             
             NSString*title;
             NSString*project;
             if ([dic[@"title"] isKindOfClass:[NSNull class]]) {
                 title = @"";
                 project = @"";
             } else {
                 title = dic[@"title"];
                 project = dic[@"project"];
             }
             
             bool status = [dic[@"status"] boolValue];
             
             
             ticketItem*tc = [[ticketItem alloc]initWithId:ticketId lastMessage:message profilePic:profilePic respond:respond date:dateString time:startTimeString unreadCount:unreadCount status:status profileName:profileName profileId:profileId project:project title:title];
             
             [self updateTicket:tc];
         }];
        
        
    }
}

-(void)updateTicket:(ticketItem*)tc
{
    for (int i = 0; i<self.dataDef.count;i++)
    {
        
        ticketItem*we = [self.dataDef objectAtIndex:i];
        
        if([we.id isEqualToString:tc.id ])
        {
            
            tc.unreadCount = [NSString stringWithFormat:@"%ld",[we.unreadCount integerValue]+1];
            
            NSLog(@"matchikl;sdcjlscjnlsdcnscnksc 12555");
            
            [self.dataDef replaceObjectAtIndex:i withObject:tc];
            
            break;
        }
        else
        {
            NSLog(@"matchikl;sdcjlscjnlsdcnscnksc noooooooo");
            
        }
    }
    
    
    self.unReadMesslb.text = [NSString stringWithFormat:@"%ld unread messages",tUnread+1];
    
    [self.areaSettTable reloadData];
    
}

-(void)removeSocketListeners
{
    
    for (ticketItem*tc in self.dataDef)
    {
        [ [DataClassH shared].socket  off:[NSString stringWithFormat:@"Ticket/%@/out",tc.id]];
        
    }
    
}

@end

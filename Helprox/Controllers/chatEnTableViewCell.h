//
//  chatEnTableViewCell.h
//  Helprox
//
//  Created by ShKhan on 10/11/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface chatEnTableViewCell : UITableViewCell

@property (weak, nonatomic)  IBOutlet  UILabel*   nameLb;
@property (weak, nonatomic)  IBOutlet  UILabel*   messageLb;
@property (weak, nonatomic)  IBOutlet  UILabel*   datelb;
@property (weak, nonatomic)  IBOutlet  UILabel*   timelb;

@property (weak, nonatomic)  IBOutlet  UIImageView*   profileImageV;

@property (weak, nonatomic)  IBOutlet  UIView*   borderView;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *sendActivity;

@property (weak, nonatomic) IBOutlet UILabel *sendlb;

@property (weak, nonatomic) IBOutlet UIImageView *seenImageV;

@property (weak, nonatomic) IBOutlet UILabel *tryAgainlb;


@end

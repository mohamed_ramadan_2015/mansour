//
//  Question.h
//  Helprox
//
//  Created by ShKhan on 10/4/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject



/**
 The arTitle for this question.
 */
@property (nonatomic, copy, nullable) NSString *arTitle;

/**
 The enTitle for this question.
 */
@property (nonatomic, copy, nullable) NSString *enTitle;

/**
 The type of this question.
 */
@property (nonatomic, copy, nullable) NSString *type;

/**
 The arAnswers of this question.
 */
@property (nonatomic, copy, nullable) NSArray *arAnswers;


/**
 The enAnswers of this question.
 */
@property (nonatomic, copy, nullable) NSArray *enAnswers;


/**
 whether the qustion is mandatory.
 */

@property bool obligatory;











@end

//
//  HelproxMessenger.h
//  HelproxMessenger
//
//  Created by ShKhan on 10/3/17.
//  Copyright © 2017 Approcks. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface HelproxMessenger : NSObject


/*!
 
 show circle of messenger.
 
 
 */
+ (void)showBall;


/*!
 
 hide circle of messenger
 
 */
+ (void)hideBall;

/*!

 open messenger chat
 
 */

+ (void)openMessenger;


/*!
   close messenger chat.
 */
+ (void)closeMessenger;








@end

//
//  downloadItem.h
//  IBSWeather
//
//  Created by ShKhan on 12/4/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface downloadItem : NSObject


@property (nonatomic,strong)  NSString*id;

@property (nonatomic,strong)  NSURL*url;

@property (nonatomic,strong)  NSString*sender;


-(downloadItem*)initWithId:(NSString*)id
url:(NSURL*)url sender:(NSString*)sender;

-(void)startDownload;
 

@end





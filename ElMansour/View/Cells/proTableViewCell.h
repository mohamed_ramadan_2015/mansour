//
//  projTableViewCell.h
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface proTableViewCell : UITableViewCell
@property(weak,nonatomic) IBOutlet UIImageView*leftImageV;

@property(weak,nonatomic) IBOutlet UILabel*titlelb;

@property(weak,nonatomic) IBOutlet UILabel*descriptionlb;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@end

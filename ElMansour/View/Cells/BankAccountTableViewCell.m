//
//  BankAccountTableViewCell.m
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "BankAccountTableViewCell.h"

@implementation BankAccountTableViewCell{
    NSDictionary *underlineAttribute;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    _mansArLbl.attributedText = [[NSAttributedString alloc] initWithString: @"المنصور"
                                                                  attributes:underlineAttribute];
    _mansEnLbl.attributedText = [[NSAttributedString alloc] initWithString: @"El Mansour"
                                                                attributes:underlineAttribute];
}


-(void)initWithBankItem:(BankItem*)bankItem{
    self.acountNumEgyLbl.attributedText = [[NSAttributedString alloc] initWithString: bankItem.egyAccountNum attributes:underlineAttribute];
    
    self.accountNumDollLbl.attributedText = [[NSAttributedString alloc] initWithString: bankItem.dolAccountNum attributes:underlineAttribute];
    
    self.softCodeLbl.attributedText = [[NSAttributedString alloc] initWithString: bankItem.softCode attributes:underlineAttribute];
    
    self.bankLogoImageV.image = [UIImage imageNamed:bankItem.imageName];
    
    if ([bankItem.softCode isEqualToString:@""]) {
        [_softLbl setHidden:YES];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

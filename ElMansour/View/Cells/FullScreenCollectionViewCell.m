//
//  FullScreenCollectionViewCell.m
//  ElMansour
//
//  Created by M R on 1/8/18.
//  Copyright © 2018 Approcks. All rights reserved.
//

#import "FullScreenCollectionViewCell.h"
#define MAXIMUM_SCALE 3.0
#define MINIMUM_SCALE 1.0

@implementation FullScreenCollectionViewCell 

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setup {
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoomImage:)];
    self.imageView.gestureRecognizers = @[pinch];
    self.imageView.userInteractionEnabled = YES;
    self.scrollView.delegate = self;
    
}

//-----------------------------------------------------------------------

#pragma  mark  - Scrollview Delegate Method

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

//-----------------------------------------------------------------------

#pragma mark - Custom Methods

- (void)zoomImage:(UIPinchGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded
        || gesture.state == UIGestureRecognizerStateChanged) {
        NSLog(@"gesture.scale = %f", gesture.scale);
        
        CGFloat currentScale = self.frame.size.width / self.bounds.size.width;
        CGFloat newScale = currentScale * gesture.scale;
        
        if (newScale < MINIMUM_SCALE) {
            newScale = MINIMUM_SCALE;
        }
        if (newScale > MAXIMUM_SCALE) {
            newScale = MAXIMUM_SCALE;
        }
        
        CGAffineTransform transform = CGAffineTransformMakeScale(newScale, newScale);
        self.imageView.transform = transform;
        self.scrollView.contentSize = self.imageView.frame.size;
    }
}

@end

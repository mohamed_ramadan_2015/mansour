//
//  BankAccountTableViewCell.h
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankItem.h"

@interface BankAccountTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bankLogoImageV;

@property (weak, nonatomic) IBOutlet UILabel *acountNumEgyLbl;

@property (weak, nonatomic) IBOutlet UILabel *accountNumDollLbl;

@property (weak, nonatomic) IBOutlet UILabel *softCodeLbl;

@property (weak, nonatomic) IBOutlet UILabel *mansArLbl;
@property (weak, nonatomic) IBOutlet UILabel *mansEnLbl;
@property (weak, nonatomic) IBOutlet UILabel *softLbl;

-(void)initWithBankItem:(BankItem*)bankItem;
@end

//
//  filterTableViewCell.m
//  Mansour
//
//  Created by M R on 10/31/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "filterTableViewCell.h"

@implementation filterTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _checkLbl.text = [NSString stringWithFormat:@"%C",0xf10c];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

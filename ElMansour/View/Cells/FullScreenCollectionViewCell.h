//
//  FullScreenCollectionViewCell.h
//  ElMansour
//
//  Created by M R on 1/8/18.
//  Copyright © 2018 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullScreenCollectionViewCell : UICollectionViewCell<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (void)setup ;
@end

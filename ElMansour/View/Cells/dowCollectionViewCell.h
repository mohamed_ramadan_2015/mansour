//
//  dowCollectionViewCell.h
//  Mansour
//
//  Created by ShKhan on 10/25/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface dowCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *previewImageV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@end

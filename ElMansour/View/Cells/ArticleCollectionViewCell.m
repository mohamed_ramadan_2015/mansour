//
//  ArticleCollectionViewCell.m
//  Mansour
//
//  Created by M R on 11/6/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "ArticleCollectionViewCell.h"

@implementation ArticleCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.contentView.clipsToBounds = NO;
}

@end

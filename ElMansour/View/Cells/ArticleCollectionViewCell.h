//
//  ArticleCollectionViewCell.h
//  Mansour
//
//  Created by M R on 11/6/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCollectionViewCell : UICollectionViewCell

@property(weak,nonatomic) IBOutlet UILabel*titleLb;
@property(weak,nonatomic) IBOutlet UILabel*summaryLb;
@property(weak,nonatomic) IBOutlet UIImageView*leftImageV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property(weak,nonatomic) IBOutlet UIImageView*youtubeImg;
@end

//
//  instalmentsTableViewCell.h
//  Mansour
//
//  Created by M R on 11/20/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface instalmentsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *amountLbl;

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@property (weak, nonatomic) IBOutlet UILabel *noteLbl
;

@end

//
//  NewsTableViewCell.m
//  Mansour
//
//  Created by M R on 11/6/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "NewsTableViewCell.h"

@implementation NewsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    
    self.opaque = NO;
    
    self.whiteView.layer.cornerRadius = 5;
    
    self.leftImageV.layer.cornerRadius = 5;
    
    self.leftImageV.clipsToBounds = YES;
    
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

@end

//
//  instalmentsTableViewCell.m
//  Mansour
//
//  Created by M R on 11/20/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "instalmentsTableViewCell.h"

@implementation instalmentsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    self.contentView.backgroundColor = [UIColor whiteColor];
    // Configure the view for the selected state
}

@end

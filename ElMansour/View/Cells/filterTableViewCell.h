//
//  filterTableViewCell.h
//  Mansour
//
//  Created by M R on 10/31/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface filterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *checkLbl;
@property (weak, nonatomic) IBOutlet UILabel *tagLbl;

@end

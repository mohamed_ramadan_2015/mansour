//
//  mainTableViewCell.h
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface maiTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *whiteView;
@property(weak,nonatomic) IBOutlet UILabel*titleLb;
@property(weak,nonatomic) IBOutlet UILabel*summaryLb;
@property(weak,nonatomic) IBOutlet UIImageView*leftImageV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property(weak,nonatomic) IBOutlet UIImageView*youtubeImg;
@end

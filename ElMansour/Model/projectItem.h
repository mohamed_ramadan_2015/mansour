//
//  projectItem.h
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface projectItem : NSObject<NSCoding>



@property (nonatomic,strong) NSString* textEn;
@property (nonatomic,strong) NSString* textAr;
@property (nonatomic,strong) NSString* subtitleEn;
@property (nonatomic,strong) NSString* subtitleAr;
@property (nonatomic,strong) NSString* locationEn;
@property (nonatomic,strong) NSString* locationAr;
@property (nonatomic,strong) NSString* descriptionEn;
@property (nonatomic,strong) NSString* descriptionAr;
@property (nonatomic,strong) NSString* availableAreas;
@property (nonatomic,strong) NSString* avgPrice;
@property (nonatomic,strong) NSString* amountSold;
@property (nonatomic,strong) NSString* amountRemaining;
@property (nonatomic,strong) NSString*isActive;
@property (nonatomic,strong) NSString* inSlider;
@property (nonatomic,strong) NSString* inFeatured;
@property (nonatomic,strong) NSString* youtubeStr;
@property (nonatomic,strong) NSString* mainImgStr;
@property (nonatomic,strong) NSString* featuredPosition;
@property (nonatomic,strong) NSString* sliderPosition;
@property (nonatomic,strong) NSString* noViews;
@property (nonatomic,strong) NSString* lat;
@property (nonatomic,strong) NSString* lon;
@property (nonatomic,strong) NSArray* metadata;
@property (nonatomic,strong) NSDictionary* locationTag;
@property (nonatomic,strong) NSString* id;
@property (nonatomic,strong) NSString* createdAt;
@property (nonatomic,strong) NSString* updatedAt;
@property (nonatomic,strong) NSArray* externalImages;
@property (nonatomic,strong) NSArray* intersectionImages;
@property (nonatomic,strong) NSString* shareAr;
@property (nonatomic,strong) NSString* shareEn;
@property (nonatomic,strong) NSString* mSquareImgUrl;

-(projectItem*)initWithTextEn:(NSString*)textEn
                       textAr:(NSString*)textAr
                   subtitleEn:(NSString*)subtitleEn
                   subtitleAr:(NSString*)subtitleAr
                   locationEn:(NSString*)locationEn
                   locationAr:(NSString*)locationAr
                descriptionEn:(NSString*)descriptionEn
                descriptionAr:(NSString*)descriptionAr
               availableAreas:(NSString*)availableAreas
                     avgPrice:(NSString*)avgPrice
                   amountSold:(NSString*)amountSold
              amountRemaining:(NSString*)amountRemaining
                     isActive:(NSString*)isActive
                     inSlider:(NSString*)inSlider
                   inFeatured:(NSString*)inFeatured
                   youtubeStr:(NSString*)youtubeStr
                   mainImgStr:(NSString*)mainImgStr
             featuredPosition:(NSString*)featuredPosition
               sliderPosition:(NSString*)sliderPosition
                      noViews:(NSString*)noViews
                          lat:(NSString*)lat
                          lon:(NSString*)lon
                     metadata:(NSArray*)metadata
                  locationTag:(NSDictionary*)locationTag
                           id:(NSString*)id
                    createdAt:(NSString*)createdAt
                    updatedAt:(NSString*)updatedAt
                externalImages:(NSArray*)externalImages
            intersectionImages:(NSArray*)intersectionImages
                        shareAr:(NSString*)shareAr
                        shareEn:(NSString*)shareEn
                  mSquareImgUrl:(NSString*)m_square_img_url;

@end

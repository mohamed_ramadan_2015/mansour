//
//  ArticleItem.m
//  Mansour
//
//  Created by M R on 11/7/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "ArticleItem.h"

@implementation ArticleItem

-(ArticleItem*)initWithTitleEn:(NSString*)titleEn
                       titleAr:(NSString*)titleAr
                 descriptionEn:(NSString*)descriptionEn
                 descriptionAr:(NSString*)descriptionAr
                            Id:(NSString*)Id
                    youtubeStr:(NSString*)youtubeStr
                    mainImgStr:(NSString*)mainImgStr
                        htmlAr:(NSString*)htmlAr
                        htmlEn:(NSString*)htmlEn
                       shareAr:(NSString*)shareAr
                       shareEn:(NSString*)shareEn
                 mSquareImgUrl:(NSString*)m_square_img_url {
    
    self.mSquareImgUrl = m_square_img_url;
    
    self.titleAr = titleAr;
    
    self.titleEn = titleEn;
    
    self.descriptionAr = descriptionAr;
    
    self.descriptionEn = descriptionEn;
    
    self.youtubeStr = youtubeStr;
    
    self.mainImgStr = mainImgStr;
    
    self.Id = Id;
    
    self.htmlAr = htmlAr;
    
    self.htmlEn = htmlEn;
    
    self.shareAr = shareAr;
    
    self.shareEn = shareEn;
    
    return  self;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.titleAr forKey:@"titleAr"];
    [encoder encodeObject:self.titleEn forKey:@"titleEn"];
    [encoder encodeObject:self.descriptionAr forKey:@"descriptionAr"];
    [encoder encodeObject:self.descriptionEn forKey:@"descriptionEn"];
    [encoder encodeObject:self.youtubeStr forKey:@"youtubeStr"];
    [encoder encodeObject:self.mainImgStr forKey:@"mainImgStr"];
    [encoder encodeObject:self.Id forKey:@"id"];
    [encoder encodeObject:self.htmlEn forKey:@"htmlEn"];
    [encoder encodeObject:self.htmlAr forKey:@"htmlAr"];
    [encoder encodeObject:self.shareAr forKey:@"shareAr"];
    [encoder encodeObject:self.shareEn forKey:@"shareEn"];
    [encoder encodeObject:self.mSquareImgUrl forKey:@"mSquareImgUrl"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.titleAr = [decoder decodeObjectForKey:@"titleAr"];
        self.titleEn = [decoder decodeObjectForKey:@"titleEn"];
        self.descriptionAr = [decoder decodeObjectForKey:@"descriptionAr"];
        self.descriptionEn = [decoder decodeObjectForKey:@"descriptionEn"];
        self.youtubeStr = [decoder decodeObjectForKey:@"youtubeStr"];
        self.mainImgStr = [decoder decodeObjectForKey:@"mainImgStr"];
        self.Id = [decoder decodeObjectForKey:@"id"];
        self.htmlEn = [decoder decodeObjectForKey:@"htmlEn"];
        self.htmlAr = [decoder decodeObjectForKey:@"htmlAr"];
        self.shareAr = [decoder decodeObjectForKey:@"shareAr"];
        self.shareEn = [decoder decodeObjectForKey:@"shareEn"];
        self.mSquareImgUrl = [decoder decodeObjectForKey:@"mSquareImgUrl"];
    }
    return self;
}


@end

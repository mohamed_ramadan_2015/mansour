//
//  UserItem.m
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "UserItem.h"

@implementation UserItem

-(UserItem*)initWithUserId:(NSString*)userId
                 firstName:(NSString*)firstName
                  lastName:(NSString*)lastName
                     phone:(NSString *)phone
                   isGuest:(BOOL)isGuest{
    self.userId = userId;
    self.firstName = firstName;
    self.lastName = lastName;
    self.isGuest = isGuest;
    self.phone = phone;
    
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.firstName forKey:@"firstName"];
    [encoder encodeObject:self.lastName forKey:@"lastName"];
    [encoder encodeBool:self.isGuest forKey:@"isGuest"];
    [encoder encodeObject:self.phone forKey:@"phone"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.firstName = [decoder decodeObjectForKey:@"firstName"];
        self.lastName = [decoder decodeObjectForKey:@"lastName"];
        self.isGuest = [decoder decodeBoolForKey:@"isGuest"];
        self.phone = [decoder decodeObjectForKey:@"phone"];
    }
    return self;
}


@end

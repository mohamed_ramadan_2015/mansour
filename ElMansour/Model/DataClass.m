//
//  DataClass.m
//  PrayerNow
//
//  Created by ApprocksEg on 11/2/15.
//  Copyright © 2015 ApprocksEg. All rights reserved.
//

#import "DataClass.h"
#import "newsItem.h"
#import "projectItem.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "BankItem.h"

@implementation DataClass

static DataClass *instance = nil;

+(DataClass *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [DataClass new];
            
            NSUserDefaults*defaults = [NSUserDefaults  standardUserDefaults];
            
            NSString* dd = NSLocalizedString(@"appLang", comment: "");
            
            if([dd isEqualToString:@"ar"])
            {
                instance.ar = YES ;
                
                [defaults setObject:@[dd] forKey:@"AppleLanguages"];
            }
            else
            {
                instance.ar = NO ;
                
                [defaults setObject:@[dd] forKey:@"AppleLanguages"];
            }
            
            NSData *data1 = [defaults objectForKey:kNews];
            
            NSMutableArray *n1 = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
            
            instance.newsArr = n1;
            
            NSData *data2 = [defaults objectForKey:kProjectsSlider];
            
            NSMutableArray *n2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
            
            instance.projectsSliderArr = n2;
            
            NSData *data3 = [defaults objectForKey:kProjectsChat];
            
            NSMutableArray *n3 = [NSKeyedUnarchiver unarchiveObjectWithData:data3];
            
            instance.projectsChatArr = n3;
            
            
            
            instance.leftArrow = [NSString stringWithFormat:@"%C",0xf104];
            instance.rightArrow = [NSString stringWithFormat:@"%C",0xf105];
            
            
            NSMutableArray* banksData = [[NSMutableArray alloc]init];
            BankItem *item1 = [[BankItem alloc] init];
            BankItem *item2 = [[BankItem alloc] init];
            BankItem *item3 = [[BankItem alloc] init];
            BankItem *item4 = [[BankItem alloc] init];
            
            // Ahly bank
            [banksData addObject:[item1 initWithEgyAccountNum:@"1063070654596800014"
                                                dolAccountNum:@"1063060654596800014"
                                                     softCode:@"NBEGEGCX106"
                                                    imageName:@"ahly.png"]];
            
            // Auda bank
            [banksData addObject:[item2 initWithEgyAccountNum:@"29810046400100800"
                                                dolAccountNum:@""
                                                     softCode:@""
                                                    imageName:@"odaa.png"]];
            
            // agricole agrecaul bank
            [banksData addObject:[item3 initWithEgyAccountNum:@"11018180003890"
                                                dolAccountNum:@"11018400001198"
                                                     softCode:@""
                                                    imageName:@"agricole.png"]];
            
            // NKB Keweet bank
            [banksData addObject:[item4 initWithEgyAccountNum:@"2854172"
                                                dolAccountNum:@"1902285417210010"
                                                     softCode:@""
                                                    imageName:@"keweet"]];
            
            instance.banksArr = [banksData mutableCopy];
        }
    }
    
    return instance;
}


+(bool)isNetworkAvailable {
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com");
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    return canReach;
}

@end

//
//  InstallmentItem.h
//  Mansour
//
//  Created by M R on 11/22/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstallmentItem : NSObject

@property (nonatomic,strong) NSString* amount;
@property (nonatomic,strong) NSString* date;
@property (nonatomic,strong) NSString* note;


-(InstallmentItem*)initWithAmount:(NSString*)amount
                         date:(NSString*)date
                         note:(NSString*)note;

@end

//
//  InstallmentItem.m
//  Mansour
//
//  Created by M R on 11/22/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "InstallmentItem.h"

@implementation InstallmentItem

-(InstallmentItem *)initWithAmount:(NSString *)amount
                             date:(NSString *)date
                             note:(NSString *)note {
    
    self.amount = amount;
    self.date = date;
    self.note = note;
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.amount forKey:@"amount"];
    [encoder encodeObject:self.date forKey:@"date"];
    [encoder encodeObject:self.note forKey:@"note"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.amount = [decoder decodeObjectForKey:@"amount"];
        self.date = [decoder decodeObjectForKey:@"date"];
        self.note = [decoder decodeObjectForKey:@"note"];
    }
    return self;
}


@end

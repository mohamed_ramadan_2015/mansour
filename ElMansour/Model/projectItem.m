//
//  projectItem.m
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "projectItem.h"

@implementation projectItem


-(projectItem*)initWithTextEn:(NSString*)textEn
                       textAr:(NSString*)textAr
                   subtitleEn:(NSString*)subtitleEn
                   subtitleAr:(NSString*)subtitleAr
                   locationEn:(NSString*)locationEn
                   locationAr:(NSString*)locationAr
                descriptionEn:(NSString*)descriptionEn
                descriptionAr:(NSString*)descriptionAr
               availableAreas:(NSString*)availableAreas
                     avgPrice:(NSString*)avgPrice
                   amountSold:(NSString*)amountSold
              amountRemaining:(NSString*)amountRemaining
                     isActive:(NSString*)isActive
                     inSlider:(NSString*)inSlider
                   inFeatured:(NSString*)inFeatured
                   youtubeStr:(NSString*)youtubeStr
                   mainImgStr:(NSString*)mainImgStr
             featuredPosition:(NSString*)featuredPosition
               sliderPosition:(NSString*)sliderPosition
                      noViews:(NSString*)noViews
                          lat:(NSString*)lat
                          lon:(NSString*)lon
                     metadata:(NSArray*)metadata
                  locationTag:(NSDictionary*)locationTag
                           id:(NSString*)id
createdAt:(NSString*)createdAt
updatedAt:(NSString*)updatedAt
externalImages:(NSArray*)externalImages
intersectionImages:(NSArray*)intersectionImages
shareAr:(NSString*)shareAr
shareEn:(NSString*)shareEn
mSquareImgUrl:(NSString *)m_square_img_url{
    
    self.mSquareImgUrl = m_square_img_url;
    
    self.textAr = textAr;
    
    self.textEn = textEn;
    
    self.subtitleEn = subtitleEn;
    
    self.subtitleAr = subtitleAr;
    
    self.locationEn = locationEn;
    
    self.locationAr = locationAr;
    
    self.descriptionEn = descriptionEn;
    
    self.descriptionAr = descriptionAr;
    
    self.availableAreas = availableAreas;
    
    self.avgPrice = avgPrice;
    
    self.amountSold = amountSold;
    
    self.amountRemaining = amountRemaining;
    
    self.isActive = isActive;
    
    self.inSlider = inSlider;
    
    self.inFeatured = inFeatured;
    
    self.youtubeStr = youtubeStr;
    
    self.mainImgStr = mainImgStr;
    
    self.featuredPosition = featuredPosition;
    
    self.sliderPosition = sliderPosition;
    
    self.noViews = noViews;
    
    self.lat = lat;
    
    self.lon = lon;
    
    self.metadata = metadata;
    
    self.locationTag = locationTag;
    
    self.id = id;
    
    self.createdAt = createdAt;
    
    self.updatedAt = updatedAt;
    
    self.externalImages = externalImages;
    
    self.intersectionImages = intersectionImages;
    
    self.shareAr = shareAr;
    
    self.shareEn = shareEn;
    
    return  self;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.textAr forKey:@"textAr"];
    [encoder encodeObject:self.textEn forKey:@"textEn"];
    [encoder encodeObject:self.subtitleAr forKey:@"subtitleAr"];
    [encoder encodeObject:self.subtitleEn forKey:@"subtitleEn"];
    [encoder encodeObject:self.locationAr forKey:@"locationAr"];
    [encoder encodeObject:self.locationEn forKey:@"locationEn"];
    [encoder encodeObject:self.descriptionAr forKey:@"descriptionAr"];
    [encoder encodeObject:self.descriptionEn forKey:@"descriptionEn"];
    
    [encoder encodeObject:self.youtubeStr forKey:@"youtubeStr"];
    [encoder encodeObject:self.mainImgStr forKey:@"mainImgStr"];
    
    [encoder encodeObject:self.availableAreas forKey:@"availableAreas"];
    [encoder encodeObject:self.avgPrice forKey:@"avgPrice"];
    [encoder encodeObject:self.amountSold forKey:@"amountSold"];
    
    [encoder encodeObject:self.amountRemaining forKey:@"amountRemaining"];
    [encoder encodeObject:self.isActive forKey:@"isActive"];
    [encoder encodeObject:self.inSlider forKey:@"inSlider"];
    [encoder encodeObject:self.inFeatured forKey:@"inFeatured"];
    [encoder encodeObject:self.featuredPosition forKey:@"featuredPosition"];
    [encoder encodeObject:self.sliderPosition forKey:@"sliderPosition"];
    
    [encoder encodeObject:self.lat forKey:@"lat"];
    [encoder encodeObject:self.lon forKey:@"lon"];
    
    [encoder encodeObject:self.metadata forKey:@"metadata"];
    [encoder encodeObject:self.locationTag forKey:@"locationTag"];
    [encoder encodeObject:self.id forKey:@"id"];
    [encoder encodeObject:self.createdAt forKey:@"createdAt"];
    [encoder encodeObject:self.updatedAt forKey:@"updatedAt"];
    
    [encoder encodeObject:self.externalImages forKey:@"externalImages"];
    [encoder encodeObject:self.intersectionImages forKey:@"intersectionImages"];
    
    [encoder encodeObject:self.shareEn forKey:@"shareEn"];
    [encoder encodeObject:self.shareAr forKey:@"shareAr"];
    [encoder encodeObject:self.mSquareImgUrl forKey:@"mSquareImgUrl"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        
        //decode properties, other class vars
        self.textAr = [decoder decodeObjectForKey:@"textAr"];
        self.textEn = [decoder decodeObjectForKey:@"textEn"];
        self.subtitleAr = [decoder decodeObjectForKey:@"subtitleAr"];
        self.subtitleEn = [decoder decodeObjectForKey:@"subtitleEn"];
        self.locationAr = [decoder decodeObjectForKey:@"locationAr"];
        self.locationEn = [decoder decodeObjectForKey:@"locationEn"];
        self.descriptionAr = [decoder decodeObjectForKey:@"descriptionAr"];
        self.descriptionEn = [decoder decodeObjectForKey:@"descriptionEn"];
        
        self.youtubeStr = [decoder decodeObjectForKey:@"youtubeStr"];
        self.mainImgStr = [decoder decodeObjectForKey:@"mainImgStr"];
        
        self.availableAreas = [decoder decodeObjectForKey:@"availableAreas"];
        self.avgPrice = [decoder decodeObjectForKey:@"avgPrice"];
        self.amountSold = [decoder decodeObjectForKey:@"amountSold"];
        
        self.amountRemaining = [decoder decodeObjectForKey:@"amountRemaining"];
        self.isActive = [decoder decodeObjectForKey:@"isActive"];
        self.inSlider = [decoder decodeObjectForKey:@"inSlider"];
        self.inFeatured = [decoder decodeObjectForKey:@"inFeatured"];
        self.featuredPosition = [decoder decodeObjectForKey:@"featuredPosition"];
        self.sliderPosition = [decoder decodeObjectForKey:@"sliderPosition"];
        self.lat = [decoder decodeObjectForKey:@"lat"];
        self.lon = [decoder decodeObjectForKey:@"lon"];
        self.metadata = [decoder decodeObjectForKey:@"metadata"];
        self.locationTag = [decoder decodeObjectForKey:@"locationTag"];
        self.id = [decoder decodeObjectForKey:@"id"];
        self.createdAt = [decoder decodeObjectForKey:@"createdAt"];
        self.updatedAt = [decoder decodeObjectForKey:@"updatedAt"];
        
        self.externalImages = [decoder decodeObjectForKey:@"externalImages"];
        self.intersectionImages = [decoder decodeObjectForKey:@"intersectionImages"];
        
        self.shareEn = [decoder decodeObjectForKey:@"shareEn"];
        self.shareAr = [decoder decodeObjectForKey:@"shareAr"];
        
        self.mSquareImgUrl = [decoder decodeObjectForKey:@"mSquareImgUrl"];
    }
    return self;
}




@end

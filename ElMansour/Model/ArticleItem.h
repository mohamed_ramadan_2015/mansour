//
//  ArticleItem.h
//  Mansour
//
//  Created by M R on 11/7/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArticleItem : NSObject

@property (nonatomic,strong) NSString* titleEn;
@property (nonatomic,strong) NSString* titleAr;
@property (nonatomic,strong) NSString* descriptionEn;
@property (nonatomic,strong) NSString* descriptionAr;
@property (nonatomic,strong) NSString* Id;
@property (nonatomic,strong) NSString* youtubeStr;
@property (nonatomic,strong) NSString* mainImgStr;
@property (nonatomic,strong) NSString* htmlAr;
@property (nonatomic,strong) NSString* htmlEn;
@property (nonatomic,strong) NSString* shareAr;
@property (nonatomic,strong) NSString* shareEn;
@property (nonatomic,strong) NSString* mSquareImgUrl;

-(ArticleItem*)initWithTitleEn:(NSString*)titleEn
                       titleAr:(NSString*)titleAr
                 descriptionEn:(NSString*)descriptionEn
                 descriptionAr:(NSString*)descriptionAr
                            Id:(NSString*)Id
                    youtubeStr:(NSString*)youtubeStr
                    mainImgStr:(NSString*)mainImgStr
                        htmlAr:(NSString*)htmlAr
                        htmlEn:(NSString*)htmlEn
                       shareAr:(NSString*)shareAr
                       shareEn:(NSString*)shareEn
                 mSquareImgUrl:(NSString*)m_square_img_url;

@end

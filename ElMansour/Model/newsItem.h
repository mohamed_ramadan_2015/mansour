//
//  projectItem.h
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface newsItem : NSObject<NSCoding>



@property (nonatomic,strong) NSString* titleEn;
@property (nonatomic,strong) NSString* titleAr;
@property (nonatomic,strong) NSString* descriptionEn;
@property (nonatomic,strong) NSString* descriptionAr;
@property (nonatomic,strong) NSString* Id;
@property (nonatomic,strong) NSString* youtubeStr;
@property (nonatomic,strong) NSString* mainImgStr;
@property (nonatomic,strong) NSString* htmlAr;
@property (nonatomic,strong) NSString* htmlEn;
@property (nonatomic,strong) NSString* shareAr;
@property (nonatomic,strong) NSString* shareEn;
@property (nonatomic,strong) NSString* mSquareImgUrl;

-(newsItem*)initWithTitleEn:(NSString*)titleEn
                    titleAr:(NSString*)titleAr
              descriptionEn:(NSString*)descriptionEn
              descriptionAr:(NSString*)descriptionAr
                         Id:(NSString*)Id
                 youtubeStr:(NSString*)youtubeStr
                 mainImgStr:(NSString*)mainImgStr
                     htmlAr:(NSString*)htmlAr
                     htmlEn:(NSString*)htmlEn
                    shareAr:(NSString*)shareAr
                    shareEn:(NSString*)shareEn
              mSquareImgUrl:(NSString*)m_square_img_url

 ;

@end

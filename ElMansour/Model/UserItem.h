//
//  UserItem.h
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserItem : NSObject<NSCoding>

@property (nonatomic) NSString* userId;
@property (nonatomic) NSString* firstName;
@property (nonatomic) NSString* lastName;
@property (nonatomic) NSString* phone;
@property (nonatomic) BOOL isGuest;

-(UserItem*)initWithUserId:(NSString*)userId
                 firstName:(NSString*)firstName
                  lastName:(NSString*)lastName
                     phone:(NSString*)phone
                   isGuest:(BOOL)isGuest;

@end

//
//  BankItem.m
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "BankItem.h"

@implementation BankItem

-(BankItem *)initWithEgyAccountNum:(NSString *)egyAccountNum
                     dolAccountNum:(NSString *)dolAccountNum
                          softCode:(NSString *)softCode
                         imageName:(NSString *)imageName {
    
    self.egyAccountNum = egyAccountNum;
    self.dolAccountNum = dolAccountNum;
    self.softCode = softCode;
    self.imageName = imageName;
    
    return self;
}

@end

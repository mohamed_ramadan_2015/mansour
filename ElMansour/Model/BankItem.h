//
//  BankItem.h
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankItem : NSObject

@property (nonatomic,strong) NSString* egyAccountNum;
@property (nonatomic,strong) NSString* dolAccountNum;
@property (nonatomic,strong) NSString* softCode;
@property (nonatomic,strong) NSString* imageName;


-(BankItem*)initWithEgyAccountNum:(NSString*)egyAccountNum
                    dolAccountNum:(NSString*)dolAccountNum
                         softCode:(NSString*)softCode
                        imageName:(NSString*)imageName;
@end

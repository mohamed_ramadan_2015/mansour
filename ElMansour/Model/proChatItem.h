//
//  projectItem.h
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface proChatItem : NSObject<NSCoding>



@property (nonatomic,strong) NSString* textEn;
@property (nonatomic,strong) NSString* textAr;


-(proChatItem*)initWithTextEn:(NSString*)textEn
                       textAr:(NSString*)textAr;


@end

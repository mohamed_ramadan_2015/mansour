//
//  projectItem.m
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "proChatItem.h"

@implementation proChatItem


-(proChatItem*)initWithTextEn:(NSString*)textEn
                       textAr:(NSString*)textAr
                   {
    
    self.textAr = textAr;
    
    self.textEn = textEn;
    
    
    
    return  self;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.textAr forKey:@"textAr"];
    [encoder encodeObject:self.textEn forKey:@"textEn"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        
        //decode properties, other class vars
        self.textAr = [decoder decodeObjectForKey:@"textAr"];
        self.textEn = [decoder decodeObjectForKey:@"textEn"];
        
    }
    return self;
}




@end

//
//  NewsViewController.m
//  Mansour
//
//  Created by M R on 11/6/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "NewsViewController.h"
#import "SWRevealViewController.h"
#import "NewsTableViewCell.h"
#import "NewsDetailsViewController.h"
#import "newsItem.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define KLiveNews @"KLiveNews"
#define cellIdentifier @"newsCell"
@import Firebase;
@interface NewsViewController (){
    
    NSString*filePathC;
}
@property(nonatomic,retain)NSMutableArray*dataDef;

@end

@implementation NewsViewController

#pragma mark : View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.clipsToBounds = YES;
    
    if(self.revealViewController != nil){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        if ([DataClass getInstance].ar) {
            [_menuBu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rightViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        } else {
            [_menuBu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rearViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        }
        
    } else {
        
        NSLog(@"dfsdfsdfs");
    }
    
    
    if([DataClass getInstance].ar)
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
        _newslb.text = @"اخبار";
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
        
        _newslb.text = @"News";
    }
    
    UINib *nib1 = [UINib nibWithNibName:
                   NSStringFromClass([NewsTableViewCell class])
                                 bundle:[NSBundle mainBundle]];;
    
    [self.newsTable registerNib:nib1 forCellReuseIdentifier:cellIdentifier];
    
    [self.newsTable setBounces:NO];
    
    self.newsTable.delegate=self;
    
    self.newsTable.dataSource=self;
    
    self.newsTable.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    
    self.newsTable.opaque = NO;
    
    self.newsTable.separatorColor = [UIColor clearColor];
    
    self.newsTable.showsVerticalScrollIndicator = NO;
    
    //self.newsTable.allowsSelection = NO;
    
    self.newsTable.estimatedRowHeight = 500;
    
    self.newsTable.rowHeight = UITableViewAutomaticDimension;
    
    
    NSError*error=nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/Application Support"];
    
    filePathC = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"LiveNews"]];
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:filePathC])
    {
        if(
           
           [[NSFileManager defaultManager] createDirectoryAtPath:filePathC
                                     withIntermediateDirectories:YES
                                                      attributes:nil
                                                           error:&error])
        {
            NSLog(@"created");
        }
        else
        {
            NSLog(@"not created");
        }
    }
    else
    {
        NSLog(@"exists");
    }
    
    
    
    
    if ([DataClass isNetworkAvailable]) {
        self.dataDef = [[NSMutableArray alloc] init];
        [self getNews];
    } else {
        
        NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:KLiveNews];
        
        NSArray *n2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
        
        self.dataDef =  [[NSMutableArray alloc] initWithArray:n2];
        
        NSLog(@"plaplapla %@", self.dataDef);
    }
    
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self animateBack];
    
    // log screen display
    [FIRAnalytics setScreenName:@"News List Screen" screenClass:@"NewsViewController"];
    
}

#pragma mark : table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.newsTable])
    {
        
        return [self.dataDef count] ;
        
    }
    
    return 0;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    if ([tableView isEqual:self.newsTable]) {
        
        NewsTableViewCell *cell =[_newsTable dequeueReusableCellWithIdentifier:cellIdentifier];
        
        newsItem*n1 = [self.dataDef objectAtIndex:indexPath.row];
        
        
        if([DataClass getInstance].ar) {
            cell.titleLb.text = n1.titleAr;
            
            cell.summaryLb.text = n1.descriptionAr;
            
        } else {
            cell.titleLb.text = n1.titleEn;
            
            cell.summaryLb.text = n1.descriptionEn;
        }
        
        if (![n1.youtubeStr isEqualToString:@""]) {
            [cell.youtubeImg setAlpha:1.0];
        } else {
            [cell.youtubeImg setAlpha:0.0];
        }
        
        NSString* imgUrlText = n1.mSquareImgUrl;
        
        NSString* urlWithoutExtension = [imgUrlText substringToIndex:[imgUrlText length]-4];
        NSString* imageExtension = [imgUrlText substringFromIndex:[imgUrlText length]-4];
        imgUrlText = [NSString stringWithFormat:@"%@_medium%@",urlWithoutExtension,imageExtension];
        NSString* imgUrlTextEscaped = [imgUrlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [cell.activity startAnimating];
        cell.activity.hidden=NO;
        
        [cell.leftImageV sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrlTextEscaped] placeholderImage:nil options:SDWebImageRefreshCached progress:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            [cell.activity stopAnimating];
            cell.activity.hidden=YES;
        }];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.newsTable]){
        newsItem* item = [self.dataDef objectAtIndex:[indexPath row]];
        
        NewsDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsViewController"];
        vc.item = item;
        vc.comeFrom = comeFromNewsList;
        
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark : buttons actions

- (IBAction)menuClicked:(id)sender {
    
}

- (IBAction)backClicked:(id)sender {
    
    UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark : Private Methods

-(void)animateBack
{
    self.botBackImageRightCon.constant = 0;
    
    self.botBackImageRightCon.constant+=self.view.bounds.size.width;
    
    [UIView animateWithDuration:40.0 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        self.botBackImageRightCon.constant = 0;
        
        [UIView animateWithDuration:40.0 animations:^{
            
            [self.view layoutIfNeeded];
            
            
        } completion:^(BOOL finished) {
            
            [self animateBack ];
            
        }] ;
        
    }] ;
    
}

-(void)getNews {
    
    [AppDelegate addHoverView:self.view];
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/News?active=true",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:10.0f];
    [urlRequest setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if (response == nil) {
             // timeout of failed
             [self getNews];
             
         } else {
             // continue
         }
             
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             NSLog(@"getNews respo : %@",responseDict);
             
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
             });
             
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"status_code"] integerValue]==200)
                 {
                     NSLog(@"Cancel request success");
                     
                     
                     
                     NSArray*all = responseDict[@"data"];
                     
                     for (int i = 0; i<all.count; i++)
                     {
                         
                         NSDictionary*dic = all[i];
                         
                         NSString*titleEn = dic[@"title_en"];
                         
                         NSString*titleAr = dic[@"title_ar"];
                         
                         NSString*descriptionEn = dic[@"description_en"];
                         
                         NSString*descriptionAr = dic[@"description_ar"];
                         
                         NSString*youtubeStr = dic[@"youtube_url"];
                         
                         NSString*mainImgStr = dic[@"basic_image_url"];
                         
                         NSString*Id = dic[@"id"];
                         
                         NSString*htmlAr = dic[@"html_ar"];
                         
                         NSString*htmlEn = dic[@"html_en"];
                         
                         NSString*shareAr = dic[@"shareable_ar_link"];
                         
                         NSString*shareEn = dic[@"shareable_en_link"];
                         
                         NSString*mSquareImgUrl = dic[@"m_square_img_url"];
                         
                         newsItem*n1 = [[newsItem alloc ] initWithTitleEn:titleEn titleAr:titleAr descriptionEn:descriptionEn descriptionAr:descriptionAr  Id:Id youtubeStr:youtubeStr mainImgStr:mainImgStr htmlAr:htmlAr htmlEn:htmlEn shareAr:shareAr shareEn:shareEn mSquareImgUrl:mSquareImgUrl];
                         
                         [self.dataDef addObject:n1];
                     }
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         [AppDelegate removeHoverView:self.view];
                         
                         [self.newsTable reloadData];
                         
                     });
                     
                     
                      NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.dataDef];
                      
                      NSUserDefaults*defaults =  [NSUserDefaults standardUserDefaults];
                      
                      [defaults setObject:data forKey:KLiveNews];
                      
                      [defaults synchronize];
                     
                 } else {
                     NSLog(@"getNews  fail");
                 }
             }
             else
             {
                 NSLog(@"getNews Dic ");
             }
             
         }
         else
         {
             NSLog(@"getNews error: %@",error);
         }
         
     }];
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

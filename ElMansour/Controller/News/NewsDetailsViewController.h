//
//  NewsDetailsViewController.h
//  Mansour
//
//  Created by M R on 11/6/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "newsItem.h"
#import "YTPlayerView.h"

@interface NewsDetailsViewController : UIViewController <UIWebViewDelegate,YTPlayerViewDelegate>

@property (strong, nonatomic) IBOutlet newsItem *item;
@property (nonatomic,strong) NSString* comeFrom;

@property (weak, nonatomic) IBOutlet UIButton *menuBu;
@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UILabel *articleTitleLbl;

@property (weak, nonatomic) IBOutlet UITextView *articleDescTxt;

@property (weak, nonatomic) IBOutlet UIButton *shareBu;

@property (weak, nonatomic) IBOutlet UIImageView * bottomBackImageV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botBackImageRightCon;

@property (weak, nonatomic) IBOutlet UILabel * shareIcon;

@property (weak, nonatomic) IBOutlet UILabel * newsLbl;

@property (weak, nonatomic) IBOutlet UIImageView * mainImg;

@property (weak, nonatomic) IBOutlet UIView * descView;


@property (weak, nonatomic) IBOutlet UIWebView *descWebView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeightCon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *youtubeViewHeightCon;
@property(nonatomic, strong) IBOutlet YTPlayerView *playerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *newsContentHeightCon;


- (IBAction)menuClicked:(id)sender;

- (IBAction)backClicked:(id)sender;

- (IBAction)shareClicked:(id)sender;


@end

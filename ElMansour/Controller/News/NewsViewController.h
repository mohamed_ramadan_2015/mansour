//
//  NewsViewController.h
//  Mansour
//
//  Created by M R on 11/6/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *menuBu;
@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *newslb;

@property (weak, nonatomic) IBOutlet UITableView *newsTable;

@property (weak, nonatomic) IBOutlet UIImageView * bottomBackImageV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botBackImageRightCon;


- (IBAction)menuClicked:(id)sender;

- (IBAction)backClicked:(id)sender;

@end

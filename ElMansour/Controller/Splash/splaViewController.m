//
//  splashViewController.m
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "splaViewController.h"
#import "newsItem.h"
#import "projectItem.h"
#import "proChatItem.h"
#import "downloadItem.h"
#import "NSObject+BVJSONString.h"
@import Firebase;
#define firstLoad @"firstLoad"

@interface splaViewController ()
{
    BOOL animateHappen ;
    
    NSUserDefaults*defaults;
    
}

@property(nonatomic,retain)NSMutableArray*dataDef;

@property(nonatomic,retain)NSMutableArray*projectsSlider;

@property(nonatomic,retain)NSMutableArray*projectschat;

@end

@implementation splaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    
//=======
//>>>>>>> master
    
    defaults = [ NSUserDefaults standardUserDefaults];
    
    self.logolb.text = NSLocalizedString(@"splWelcome", "");
    
    animateHappen = false ;
    
    self.dataDef = [NSMutableArray new];
    
    self.projectsSlider = [NSMutableArray new];

    self.projectschat = [NSMutableArray new];

    [_whiteImageV setHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    [self animateSplashImages];
    
    // log platform regirstration
    
    [FIRAnalytics setScreenName:@"splash screen" screenClass:@"splaViewController"];
    /*
     if(!animateHappen)
     {
     
     
     self.anim1TraCon.constant = -1*self.view.bounds.size.width*3;
     
     [UIView animateWithDuration:5.0 animations:^{
     
     
     [self.view layoutIfNeeded];
     
     
     } completion:^(BOOL finished) {
     
     
     self.logoImageVBottCon.constant = self.view.bounds.size.height/2.0 + self.logoImageV.bounds.size.height/2.0 ;
     
     self.logolbTopCon.constant = -1*(self.view.bounds.size.height/2.0 - self.logoImageV.bounds.size.height/2.0);
     
     
     [UIView animateWithDuration:2.0 animations:^{
     
     
     [self.view layoutIfNeeded];
     
     
     } completion:^(BOOL finished) {
     
     
     [self manageNav];
     
     [[NSUserDefaults standardUserDefaults] setBool:NO forKey:firstLoad];
     
     }] ;
     
     }] ;
     }
     */
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(![DataClass isNetworkAvailable])
    {
        UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                           message:NSLocalizedString(@"internetMessage", nil)
                                                          delegate:self
                                                 cancelButtonTitle:NSLocalizedString(@"langAlertTry", nil)
                                                 otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        [self getNews];
    }
    
}


-(void) animateSplashImages {
    
    
    if(!animateHappen)
    {
        
        self.anim1TraCon.constant = -1 * self.view.bounds.size.width;
        //move 1st pic
        [UIView animateWithDuration:0.75 animations:^{
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            //move 2nd pic
            self.anim1TraCon.constant = -1 * self.view.bounds.size.width * 2;
            
            [UIView animateWithDuration:0.75 animations:^{
                [self.view layoutIfNeeded];
                
            } completion:^(BOOL finished) {
                //move 3rd pic
                self.anim1TraCon.constant = -1 * self.view.bounds.size.width * 3;
                
                [UIView animateWithDuration:0.75 animations:^{
                    [self.view layoutIfNeeded];
                    
                } completion:^(BOOL finished) {
                    
                    [_whiteImageV setHidden:NO];
                    [UIView animateWithDuration:0.75 animations:^{
                        
                        [self.view layoutIfNeeded];
                        
                    } completion:^(BOOL finished) {
                        
                        //move logo down
                        self.logoImageVBottCon.constant = self.view.bounds.size.height/2.0 + self.logoImageV.bounds.size.height/2.0 ;
                        
                        [UIView animateWithDuration:2.0 animations:^{
                            [self.view layoutIfNeeded];
                            
                        } completion:^(BOOL finished) {
                            //move label up
                            self.logolbTopCon.constant = -1*(self.view.bounds.size.height/2.0 - self.logoImageV.bounds.size.height/2.0);
                            
                            [UIView animateWithDuration:2.0 animations:^{
                                [self.view layoutIfNeeded];
                                
                            } completion:^(BOOL finished) {
                                
                                [self manageNav];
                                
                                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:firstLoad];
                            }] ;
                            
                            
                        }] ;
                        
                    }];
                    
                }];
                
            }];
            
        }];
        
        /*
         [UIView animateWithDuration:4.0 animations:^{
         
         self.anim1TraCon.constant = -1*self.view.bounds.size.width;
         
         [UIView animateWithDuration:1.0 animations:^{
         [self.view layoutIfNeeded];
         
         } completion:^(BOOL finished) {
         
         
         }];
         
         } completion:^(BOOL finished) {
         
         self.logoImageVBottCon.constant = self.view.bounds.size.height/2.0 + self.logoImageV.bounds.size.height/2.0 ;
         
         self.logolbTopCon.constant = -1*(self.view.bounds.size.height/2.0 - self.logoImageV.bounds.size.height/2.0);
         
         
         [UIView animateWithDuration:2.0 animations:^{
         
         
         [self.view layoutIfNeeded];
         
         
         } completion:^(BOOL finished) {
         
         
         [self manageNav];
         
         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:firstLoad];
         
         }] ;
         
         }];
         */
        /*
         self.anim1TraCon.constant = -1*self.view.bounds.size.width*3;
         
         [UIView animateWithDuration:5.0 animations:^{
         
         
         [self.view layoutIfNeeded];
         
         
         } completion:^(BOOL finished) {
         
         
         self.logoImageVBottCon.constant = self.view.bounds.size.height/2.0 + self.logoImageV.bounds.size.height/2.0 ;
         
         self.logolbTopCon.constant = -1*(self.view.bounds.size.height/2.0 - self.logoImageV.bounds.size.height/2.0);
         
         
         [UIView animateWithDuration:2.0 animations:^{
         
         
         [self.view layoutIfNeeded];
         
         
         } completion:^(BOOL finished) {
         
         
         [self manageNav];
         
         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:firstLoad];
         
         }] ;
         
         }] ;
         */
    }
    
    
}

-(void)manageNav
{
    
    if([DataClass isNetworkAvailable])
    {
        [self getNews];
    
    }
    else
    {
        if([DataClass getInstance].newsArr != nil)
        {
            UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                               message:NSLocalizedString(@"internetMessage", nil)
                                                              delegate:self
                                                     cancelButtonTitle:NSLocalizedString(@"langAlertTry", nil)
                                                     otherButtonTitles:nil];
            [alert show];
        }
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)getNews
{
    
    [AppDelegate addHoverView:self.view];
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/News?active=true&featured=true",Domain];
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:10.0f];
    [urlRequest setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         
       /*  if (response == nil) {
             // timeout of failed
             [self getNews];
             
         } else {
             // continue
         }*/
         
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             NSLog(@"getNews respo : %@",responseDict);
             
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 
                 
                 
             });
             
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"status_code"] integerValue]==200)
                 {
                     NSLog(@"Cancel request success");
                     
                     
                     
                     NSArray*all = responseDict[@"data"];
                     
                     for (int i = 0; i<all.count; i++)
                     {
                         
                         NSDictionary*dic = all[i];
                         
                         NSString*titleEn = dic[@"title_en"];
                         
                         NSString*titleAr = dic[@"title_ar"];
                         
                         NSString*descriptionEn = dic[@"description_en"];
                         
                         NSString*descriptionAr = dic[@"description_ar"];
                         
                         NSString*youtubeStr = dic[@"youtube_url"];
                         
                         NSString*mainImgStr = dic[@"basic_image_url"];
                         
                         NSString*Id = dic[@"id"];
                         
                         NSString*htmlAr = dic[@"html_ar"];
                         
                         NSString*htmlEn = dic[@"html_en"];
                         
                         NSString*shareAr = dic[@"shareable_ar_link"];
                         
                         NSString*shareEn = dic[@"shareable_en_link"];
                         
                         NSString*mSquareImgUrl = dic[@"m_square_img_url"];
                         
                         newsItem*n1 = [[newsItem alloc ] initWithTitleEn:titleEn titleAr:titleAr descriptionEn:descriptionEn descriptionAr:descriptionAr  Id:Id youtubeStr:youtubeStr mainImgStr:mainImgStr htmlAr:htmlAr htmlEn:htmlEn shareAr:shareAr shareEn:shareEn mSquareImgUrl:mSquareImgUrl];
                         
                         
                         [self.dataDef addObject:n1];
                         
                     }
                     
                     
                     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.dataDef];
                     
                     NSUserDefaults*defaults =  [NSUserDefaults standardUserDefaults];
                     
                     [defaults setObject:data forKey:kNews];
                     
                     [defaults synchronize];
                     
                     [DataClass getInstance].newsArr = self.dataDef;
                     
                     [self getProjects];
                     
                     
                 } else {
                     NSLog(@"getNews  fail");
                 }
                 
             }
             else
             {
                 NSLog(@"getNews Dic ");
                 
                 
                 
             }
             
         }
         else
         {
             NSLog(@"getNews error: %@",error);
             
             
             
             
             
         }
         
     }];
    
    
}


-(void)getProjects {
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/Projects?active=true&slider=true",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:10.0f];
    [urlRequest setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         
         if (response == nil) {
             // timeout of failed
             [self getProjects];
             
         } else {
             // continue
         }
         
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
            // NSLog(@"getProjects respo : %@",responseDict);
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 
             });
             
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"status_code"] integerValue]==200)
                 {
                     NSLog(@"getProjects request success");
                     
                     
                     
                     NSArray*all = responseDict[@"data"];
                     
                     for (int i = 0; i<all.count; i++)
                     {
                         
                         NSDictionary*dic = all[i];
                         
                         NSString*textEn = dic[@"text_en"];
                         
                         NSString*textAr = dic[@"text_ar"];
                         
                         NSString*subtitleEn = dic[@"subtitle_en"];
                         
                         NSString*subtitleAr = dic[@"subtitle_ar"];
                         
                         NSString*locationEn = dic[@"location_en"];
                         
                         NSString*locationAr = dic[@"location_ar"];
                         
                         NSString*descriptionEn = dic[@"description_en"];
                         
                         NSString*descriptionAr = dic[@"description_ar"];
                         
                         
                         NSString*availableAreas = dic[@"available_areas"];
                         
                         NSString*avgPrice = dic[@"avg_price"];
                         
                         NSString*amountSold = dic[@"amount_sold"];
                         
                         NSString*amountRemaining = dic[@"amount_remaining"];
                         
                         NSString* isActive = dic[@"is_active"] ;
                         
                         NSString* inSlider = dic[@"in_slider"];
                         
                         NSString* inFeatured = dic[@"in_featured"] ;
                         
                         NSString*youtubeStr = dic[@"youtube_url"];
                         
                         NSString*mainImgStr = dic[@"main_img_url"];
                         
                         NSString*featuredPosition = dic[@"featured_position"];
                         
                         
                         NSString*sliderPosition = dic[@"slider_position"];
                         
                         NSString*noViews = dic[@"noViews"];
                         
                         NSArray *items = [dic[@"maps_location_latlng"] componentsSeparatedByString:@","];
                         
                         NSString*lat = items[0];
                         
                         NSString*lon = items[1];
                         
                         NSLog(@"lat145 %@",lat);
                         
                         NSLog(@"lon145 %@",lon);
                         
                         NSArray*metadata = dic[@"metadata"];
                         
                         NSDictionary*locationTag = dic[@"location_tag"];
                         
                         
                         NSString*id = dic[@"id"];
                         
                         NSString*createdAt = dic[@"created_at"];
                         
                         NSString*updatedAt = dic[@"updated_at"];
                         
                         NSArray* externalImages = dic[@"external_images_urls"];
                         
                         NSArray* intersectionImages = dic[@"intersection_images_urls"];
                         
                         NSString*shareAr = dic[@"shareable_ar_link"];
                         
                         NSString*shareEn = dic[@"shareable_en_link"];
                         
                         NSString*mSquareImgUrl = dic[@"m_square_img_url"];
                         
                         projectItem*pr = [[projectItem alloc] initWithTextEn:textEn textAr:textAr subtitleEn:subtitleEn subtitleAr:subtitleAr  locationEn:locationEn locationAr:locationAr descriptionEn:descriptionEn descriptionAr:descriptionAr availableAreas:availableAreas avgPrice:avgPrice amountSold:amountSold amountRemaining:amountRemaining isActive:isActive inSlider:inSlider inFeatured:inFeatured youtubeStr:youtubeStr mainImgStr:mainImgStr featuredPosition:featuredPosition sliderPosition:sliderPosition noViews:noViews lat:lat lon:lon metadata:metadata locationTag:locationTag id:id createdAt:createdAt updatedAt:updatedAt externalImages:externalImages intersectionImages:intersectionImages shareAr:shareAr shareEn:shareEn mSquareImgUrl:mSquareImgUrl];
                         
                         [self.projectsSlider addObject:pr];
                         
                     }
                     
                     
                     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.projectsSlider];
                     
                     NSUserDefaults*defaults =  [NSUserDefaults standardUserDefaults];
                     
                     [defaults setObject:data forKey:kProjectsSlider];
                     
                     [defaults synchronize];
                     
                     [DataClass getInstance].projectsSliderArr = self.projectsSlider;
                     
                     
                     
                     [self getConfig];
                     
                     
                 }
                 
                 else
                 {
                     NSLog(@"getProjects  fail");
                     
                     
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"getProjects Dic ");
                 
             }
             
         }
         else
         {
             NSLog(@"getProjects error: %@",error);
             
         }
         
     }];
    
    
}

-(void)getConfig {
    
    NSLog(@"Getting2001455555");
    
    NSString *urlAsString = @"http://46.101.143.146:7894/apis/v1/getConfig";
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@",[DataClassH shared].apiKey,@"ios"];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             
             NSLog(@"getConfig respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     NSLog(@"getConfig success");
                     
                     NSLog(@"titlelelelelelelellee success: %@",responseDict[@"title"]);
                     
                     NSString*title = responseDict[@"title"];
                     
                     NSString*color = @"#005752" ; //responseDict[@"color"];
                     
                     NSNumber*active = responseDict[@"active"];
                     
                     
                     NSArray*csInfo = responseDict[@"CS_info"];
                     
                     NSMutableArray*allCss = [NSMutableArray new];
                     
                     for (int i= 0 ; i <MIN(csInfo.count, 3); i++)
                     {
                         NSDictionary*rt = csInfo[i];
                         
                         NSString*id = rt[@"profile_id"];
                         
                         NSString*name = rt[@"name"];
                         
                         NSString*image = [rt[@"image"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                         
                         
                         csUser*cv = [[csUser alloc] initWithId:id name:name imageStr:image];
                         
                         [allCss addObject:cv];
                         
                         ////
                         
                         downloadItem*dw = [[downloadItem alloc]initWithId:cv.id url:cv.imageUrl sender:
                                            @"splash"];
                         
                         [dw startDownload];
                         
                         
                         
                         /////
                         
                     }
                     
                     
                     
                     settings*set = [[settings alloc]initWithTitle:title color:color active:active csUsers:allCss];
                     
                     [DataClassH shared].settings = set;
                     
                     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:set];
                     
                     NSUserDefaults*defaults = [NSUserDefaults standardUserDefaults];
                     
                     [defaults setObject:data forKey:ksettings];
                     
                     [defaults synchronize];
                     
                     
               
                      [self getProjectshat];
                     
                 }
                 
                 else
                 {
                     
                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {
                         
                         NSString*reason = responseDict[@"reason"] ;
                         
                         NSLog(@"getConfig request fail reason %@",reason);
                         
                         if([reason isEqualToString:@"emptyparameters"])
                         {
                             UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                message:reason
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                             [alert show];
                         }
                         
                         
                         
                         if([reason isEqualToString:@"notvalidparameters"])
                         {
                             UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                message:reason
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                             [alert show];
                         }
                         
                         
                         if([reason isEqualToString:@"unauthorized"])
                         {
                             UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                message:reason
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                             [alert show];
                         }
                         
                         
                         if([reason isEqualToString:@"notActive"])
                         {
                             UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                message:reason
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                             [alert show];
                         }
                         
                         
                         if([reason isEqualToString:@"invalidPlatform"])
                         {
                             UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                message:reason
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                             [alert show];
                         }
                         
                         
                         if([reason isEqualToString:@"invalidKey"])
                         {
                             UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                message:reason
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                             [alert show];
                         }
                         
                     });
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"getConfig nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"getConfig error : %@",error);
             
             
         }
         
     }];
    
    
}

-(void)getProjectshat {
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/Projects/chat",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:10.0f];
    [urlRequest setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         
     
         
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             NSLog(@"getProjectshat respo : %@",responseDict);
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 
             });
             
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"status_code"] integerValue]==200)
                 {
                     NSLog(@"getProjectshat request success");
                     
                     
                     
                     NSArray*all = responseDict[@"data"];
                     
                     for (int i = 0; i<all.count; i++)
                     {
                         
                         NSDictionary*dic = all[i];
                         
                         NSString*textEn = dic[@"en"];
                         
                         NSString*textAr = dic[@"ar"];
                         
                       
                         
                         proChatItem*pr = [[proChatItem alloc] initWithTextEn:textEn textAr:textAr];
                         
                         [self.projectschat addObject:pr];
                         
                     }
                   
                     
                    proChatItem*ww = [[proChatItem alloc] initWithTextEn:@"Project name" textAr:@"أسم المشروع"];
                     
                     [self.projectschat insertObject:ww atIndex:0];
                     
                     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.projectschat];
                     
                     NSUserDefaults*defaults =  [NSUserDefaults standardUserDefaults];
                     
                     [defaults setObject:data forKey:kProjectsChat];
                     
                     [defaults synchronize];
                     
                     [DataClass getInstance].projectsChatArr = self.projectschat;
                     
                     
                     if ([[NSUserDefaults standardUserDefaults] boolForKey:isLoggedIn])
                     {
                         
                         // get cashed user
                         NSData *userData = [[NSUserDefaults standardUserDefaults] objectForKey:kCashedUser];
                         IdentifiedUser *user = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
                         
                         [DataClassH shared].user = user;
                         
                         // check if guest or identified user
                         
                         NSLog(@"dsfasdfsadfasda : %@",user.id);
                         
                         if (![user.id isEqualToString:@""])
                         {
                             
                             [self getTickets];
                         
                         }
                         else
                         {
                             
                             dispatch_queue_t mainQueue = dispatch_get_main_queue();
                             
                             dispatch_async(mainQueue, ^(void) {
                                 
                                 
                                 UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
                                 
                                 [self.navigationController pushViewController:vc animated:YES];
                                 
                             });
                             
                         }
                         
                     } else {
                         dispatch_queue_t mainQueue = dispatch_get_main_queue();
                         
                         dispatch_async(mainQueue, ^(void) {
                             
                             
                             UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
                             
                             [self.navigationController pushViewController:vc animated:YES];
                             
                         });
                     }
                 }
                 else
                 {

                     NSLog(@"getProjectshat  fail");
                
                 }
                 
             }
             else {
                 NSLog(@"getProjectshat Dic ");
                 
             }
             
         } else {
             NSLog(@"getProjectshat error: %@",error);
             
         }
         
     }];
    
    
}


-(void)getTickets {
    
    NSData *data2 = [defaults objectForKey:kCashedUser];
    
    IdentifiedUser *user = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
    
    NSMutableDictionary*dic = [NSMutableDictionary new];
    
    [dic setObject:user.id forKey:@"user_id"];
    
    [dic setObject:[DataClassH shared].user.type forKey:@"type"];
    
    [dic setObject:user.firstName forKey:@"first_name"];
    
    [dic setObject:user.lastName forKey:@"last_name"];
    
    [dic setObject:user.phone forKey:@"phone"];
    
    
    NSString *urlAsString = @"http://46.101.143.146:7894/apis/v1/getTickets";
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_data=%@",[DataClassH shared].apiKey,@"ios", [dic jsonString]];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             NSLog(@"getTickets respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     NSLog(@"getTickets success");
                     
                     
                    NSInteger tUnread = [responseDict[@"total_unread_count"] integerValue];
                     
                     [DataClassH shared].unReadCount = tUnread;
                     
                     
                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {
                         
                         
                         UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
                         
                         [self.navigationController pushViewController:vc animated:YES];
                         
                     });
                     
                 
                 }
                 
                 else
                 {
                     
                     
                     
                     NSLog(@"getTickets request fail reason %@",responseDict[@"reason"]);
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"getTickets nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"getTickets error : %@",error);
             
             
         }
         
     }];
    
    
}

@end

//
//  splashViewController.h
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface splaViewController : UIViewController <UIAlertViewDelegate,SWRevealViewControllerDelegate>
  
@property (weak, nonatomic) IBOutlet UIImageView * anim1ImageVl;

@property (weak, nonatomic) IBOutlet UIImageView *whiteImageV;

@property (weak, nonatomic) IBOutlet UIImageView *anim3ImageVl;

@property (weak, nonatomic) IBOutlet UIImageView * anim4ImageVl;

@property (weak, nonatomic) IBOutlet UIImageView * logoImageV;

@property (weak, nonatomic) IBOutlet UILabel * logolb;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint * anim1TraCon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint * logoImageVBottCon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint * logolbTopCon;


@end

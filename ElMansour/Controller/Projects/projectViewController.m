//
//  projectsViewController.m
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "projectViewController.h"
#import "proDetViewController.h"
#import "proTableViewCell.h"
#import "filterTableViewCell.h"
#import "SWRevealViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "projectItem.h"

@import Firebase;

static NSString *CellIdentifier1 = @"ContentCell";
static NSString *CellIdentifier2 = @"filterCell";

@interface projectViewController () {
    NSMutableArray*dataDef;
    NSMutableArray*tempDataDef;
    
    NSMutableArray *ZoneTagsDef;
    
    NSMutableArray *filteredProjArr;
    
    NSString* selectedTag;
    NSIndexPath* selectedIndex;
    
    NSString*filePathC;
    
    BOOL displayFilterData;
    
    BOOL isArabic;
}


@end

@implementation projectViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[_backBu setAlpha:0];
    
    if(self.revealViewController != nil){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        if ([DataClass getInstance].ar) {
            [_menuBu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rightViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        } else {
            [_menuBu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rearViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        }
        
    } else {
        
        NSLog(@"dfsdfsdfs");
    }
    
    
    displayFilterData = NO;
    
    // hide filter view
    [_dimView setHidden:YES];
    [_filterView setHidden:YES];
    //-----
    _filterView.layer.cornerRadius = 5;
    _filterView.clipsToBounds = YES;
    
    // add tap gesure to dimView
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    tap1.numberOfTapsRequired = 1;
    _dimView.userInteractionEnabled = YES;
    [_dimView addGestureRecognizer:tap1];
    
    
    self.projectlb.text = NSLocalizedString(@"proProjects", "");
    
    self.projectsView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    
    self.projectsView.opaque = YES;
    
    dataDef = [NSMutableArray new];
    
    UINib *nib1 = [UINib nibWithNibName:@"proTableViewCell" bundle:nil];
    
    [self.areaSettTable registerNib:nib1 forCellReuseIdentifier:CellIdentifier1];
    
    [self.areaSettTable setBounces:NO];
    
    self.areaSettTable.delegate=self;
    
    self.areaSettTable.dataSource=self;
    
    self.areaSettTable.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    
    self.areaSettTable.opaque = NO;
    
    self.areaSettTable.separatorColor = [UIColor clearColor];
    
    self.areaSettTable.showsVerticalScrollIndicator = NO;
    
    
    
    UINib *nib2 = [UINib nibWithNibName:@"filterTableViewCell" bundle:nil];
    
    [self.filterTableView registerNib:nib2 forCellReuseIdentifier:CellIdentifier2];
    
   // [self.filterTableView setBounces:NO];
    
    self.filterTableView.delegate=self;
    
    self.filterTableView.dataSource=self;
    
    self.filterTableView.separatorColor = [UIColor clearColor];
    
    
    
    NSError*error=nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/Application Support"];
    
    filePathC = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"projectImages"]];
    
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:filePathC])
    {
        if(
           
           [[NSFileManager defaultManager] createDirectoryAtPath:filePathC
                                     withIntermediateDirectories:YES
                                                      attributes:nil
                                                           error:&error])
        {
            NSLog(@"created");
        }
        else
        {
            NSLog(@"not created");
        }
    }
    else
    {
        NSLog(@"exists");
    }
    
    
    if([DataClass getInstance].ar)
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
        
        _filterHeaderLbl.text = @"اختر مشروع";
        
        isArabic = YES;
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
        
        _filterHeaderLbl.text = @"Select Project";
        
        isArabic = NO;
    }
    
    
    [_indicator setHidden:YES];
    
    if ([DataClass isNetworkAvailable]) {
        [self getProjects];
    } else {
        
        NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:kProjects];
        
        NSArray *n2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
        
        dataDef =  [[NSMutableArray alloc] initWithArray:n2];
        
        NSLog(@"%@", dataDef);
   
        
        if ([dataDef count] > 0) {
            // get unic zone tags
            ZoneTagsDef = [NSMutableArray arrayWithArray:[[self getZoneTags] allObjects].mutableCopy];
            
            tempDataDef = [dataDef mutableCopy];
            
        }
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated {
    // log screen display
    [FIRAnalytics setScreenName:@"Project List Screen" screenClass:@"projectViewController"];

}

-(void)viewWillAppear:(BOOL)animated{
    
    if(self.revealViewController != nil){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        if ([DataClass getInstance].ar) {
            [_menuBu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rightViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        } else {
            [_menuBu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rearViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        }
        
    } else {
        
        NSLog(@"dfsdfsdfs");
    }
}

#pragma mark : Table view Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.areaSettTable])
    {
        return [dataDef count] ;
    
    } else if (tableView == _filterTableView) {
        return [ZoneTagsDef count];
        
    }
    
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath      *)indexPath
{

    if ([tableView isEqual:self.areaSettTable]) {
        
        proTableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        projectItem*pr = dataDef[indexPath.row];
        
        
        if([DataClass getInstance].ar)
        {
            
            cell.titlelb.text = pr.textAr;
            
            cell.descriptionlb.text = pr.descriptionAr;
            
        }
        else
        {
            
            cell.titlelb.text = pr.textEn;
            
            cell.descriptionlb.text = pr.descriptionEn;
            
            
        }
        

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        NSString* urlWithoutExtension = [pr.mSquareImgUrl substringToIndex:[pr.mSquareImgUrl length]-4];
        NSString* imageExtension = [pr.mSquareImgUrl substringFromIndex:[pr.mSquareImgUrl length]-4];
        NSString* imgUrlText = [NSString stringWithFormat:@"%@_medium%@",urlWithoutExtension,imageExtension];
        NSString* imgUrlTextEscaped = [imgUrlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        [cell.activity startAnimating];
        cell.activity.hidden=NO;
        
        [cell.leftImageV sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrlTextEscaped] placeholderImage:nil options:SDWebImageRefreshCached progress:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            [cell.activity stopAnimating];
            cell.activity.hidden=YES;
        }];
        
        return cell;
        
    } else if (tableView == _filterTableView) {
        filterTableViewCell * cell = [_filterTableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        
        if (selectedIndex == indexPath) {
            cell.checkLbl.text =  [NSString stringWithFormat:@"%C",0xf058];
        } else {
            cell.checkLbl.text = [NSString stringWithFormat:@"%C",0xf10c];
        }
        
        cell.tagLbl.text = [ZoneTagsDef objectAtIndex:indexPath.row];
        
        return cell;
        
    }
    
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if ([tableView isEqual:self.areaSettTable]) {
        // intialize pro det view controller
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        proDetViewController *pVC = [sb instantiateViewControllerWithIdentifier:@"proDetViewController"];
        pVC.project = [dataDef objectAtIndex:indexPath.row];
        pVC.comeFrom = comeFromProjList;
        
        [self.navigationController pushViewController:pVC animated:YES];
        
    } else if (tableView == _filterTableView) {
        
        if (selectedIndex != nil) {
            filterTableViewCell *selCell = [_filterTableView cellForRowAtIndexPath:selectedIndex];
            selCell.checkLbl.text = [NSString stringWithFormat:@"%C",0xf10c];
        }
        
        filterTableViewCell *cell = [_filterTableView cellForRowAtIndexPath:indexPath];
        cell.checkLbl.text =  [NSString stringWithFormat:@"%C",0xf058];
        
        selectedIndex = indexPath;
        selectedTag = [ZoneTagsDef objectAtIndex:indexPath.row];
        NSLog(@"Selected Tag: %@",selectedTag);
        
        // hide dim view
        [_dimView setHidden:YES];
        [_filterView setHidden:YES];
        
        [self filterProjectWithSelectedTag];
    }
  
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView isEqual:_areaSettTable] ) {
       
       
        
    } else if ([tableView isEqual:_filterTableView]) {
        
        filterTableViewCell *cell = [_filterTableView cellForRowAtIndexPath:indexPath];
        cell.checkLbl.text =  [NSString stringWithFormat:@"%C",0xf10c];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView isEqual:_areaSettTable]) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            return self.view.frame.size.height / 3;
            
        } else {
            
            return self.view.frame.size.height / 3;
        }
    }
    
    return UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark : Private Methods

-(void)getProjects
{
    [_indicator setHidden:NO];
    [_indicator startAnimating];
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/Projects?active=true",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:10.0f];
    [urlRequest setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         
         if (response == nil) {
             // timeout of failed
             [self getProjects];
             
         } else {
             // continue
         }
         
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
           //  NSLog(@"getProjects respo : %@",responseDict);
             
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 
             });
             
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"status_code"] integerValue]==200)
                 {
                     NSLog(@"getProjects request success");

                     NSArray*all = responseDict[@"data"];
                     
                     for (int i = 0; i<all.count; i++)
                     {
                         
                         NSDictionary*dic = all[i];
                         
                         NSString*textEn = dic[@"text_en"];
                         
                         NSString*textAr = dic[@"text_ar"];
                         
                         NSString*subtitleEn = dic[@"subtitle_en"];
                         
                         NSString*subtitleAr = dic[@"subtitle_ar"];
                         
                         NSString*locationEn = dic[@"location_en"];
                         
                         NSString*locationAr = dic[@"location_ar"];
                         
                         NSString*descriptionEn = dic[@"description_en"];
                         
                         NSString*descriptionAr = dic[@"description_ar"];

                         
                         NSString*availableAreas = dic[@"available_areas"];
                         
                         NSString*avgPrice = dic[@"avg_price"];
                         
                         NSString*amountSold = dic[@"amount_sold"];
                         
                         NSString*amountRemaining = dic[@"amount_remaining"];
                         
                         NSString* isActive = dic[@"is_active"];
                         
                         NSString* inSlider = dic[@"in_slider"] ;
                         
                         NSString* inFeatured = dic[@"in_featured"];
                         
                         
                         
                         NSString*youtubeStr = dic[@"youtube_url"];
                         
                         NSString*mainImgStr = dic[@"main_img_url"];
                         
                         NSString*featuredPosition = dic[@"featured_position"];
                         
                         
                         NSString*sliderPosition = dic[@"slider_position"];
                         
                         NSString*noViews = dic[@"noViews"];
                         
                         NSArray *items;
                         NSString*lat;
                         NSString*lon;
                         
                         if(dic[@"maps_location_latlng"] != nil){
                             items = [dic[@"maps_location_latlng"] componentsSeparatedByString:@","];
                             lat = items[0];
                             lon = items[1];
                             
                         } else {
                             items = [[NSArray alloc]init];
                             lat = @"";
                             lon = @"";
                         }

                         NSLog(@"lat145 %@",lat);
                         
                         NSLog(@"lon145 %@",lon);
                         
                         NSArray*metadata = dic[@"metadata"];
                         
                         NSDictionary*locationTag = dic[@"location_tag"];
                         
                         
                         NSString*id = dic[@"id"];
                         
                         NSString*createdAt = dic[@"created_at"];
                         
                         NSString*updatedAt = dic[@"updated_at"];
                         
                         NSArray* externalImages = dic[@"external_images_urls"];
                         
                         NSArray* intersectionImages = dic[@"intersection_images_urls"];
                         
                         NSString*shareAr = dic[@"shareable_ar_link"];
                         
                         NSString*shareEn = dic[@"shareable_en_link"];
                         
                         NSString*mSquareImgUrl = dic[@"m_square_img_url"];
                         
                         projectItem*pr = [[projectItem alloc] initWithTextEn:textEn textAr:textAr subtitleEn:subtitleEn subtitleAr:subtitleAr  locationEn:locationEn locationAr:locationAr descriptionEn:descriptionEn descriptionAr:descriptionAr availableAreas:availableAreas avgPrice:avgPrice amountSold:amountSold amountRemaining:amountRemaining isActive:isActive inSlider:inSlider inFeatured:inFeatured youtubeStr:youtubeStr mainImgStr:mainImgStr featuredPosition:featuredPosition sliderPosition:sliderPosition noViews:noViews lat:lat lon:lon metadata:metadata locationTag:locationTag id:id createdAt:createdAt updatedAt:updatedAt externalImages:externalImages intersectionImages:intersectionImages shareAr:shareAr shareEn:shareEn mSquareImgUrl:mSquareImgUrl];
  
                         [dataDef addObject:pr];
                         
                     }
                     
                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {

                         if ([dataDef count] > 0) {
                             // get unic zone tags
                             ZoneTagsDef = [NSMutableArray arrayWithArray:[[self getZoneTags] allObjects].mutableCopy];
                             
                             tempDataDef = [dataDef mutableCopy];
                
                         }
                         
                         [_indicator stopAnimating];
                         [_indicator setHidden:YES];
                         
                         
                         // cash projects in user defaults
                         NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dataDef];
                         
                         NSUserDefaults*defaults =  [NSUserDefaults standardUserDefaults];
                         
                         [defaults setObject:data forKey:kProjects];
                         [defaults synchronize];
                         
                         
                         [self.areaSettTable reloadData];
  
                     });
                     
                     
                     
                 }
                 
                 else
                 {
                     NSLog(@"getProjects  fail");
   
                 }
                 
             }
             else
             {
                 NSLog(@"getProjects Dic ");
                 
                 
                 
             }
             
         }
         else
         {
             NSLog(@"getProjects error: %@",error);
             
             
             
             
             
         }
         
     }];
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void) handleTap :(UITapGestureRecognizer*) sender {
    
    if (sender.view == _dimView) {
        
        // hide dim view
        [_dimView setHidden:YES];
        [_filterView setHidden:YES];
        
        
        [self filterProjectWithSelectedTag];
    }
    
}

-(NSMutableSet*) getZoneTags {
    
    NSMutableSet * zones = [[NSMutableSet alloc]init];
    
    for (projectItem *proj in dataDef) {
       
        [zones addObject:isArabic ? proj.locationTag[@"name_ar"] : proj.locationTag[@"name"]];
    }
    
    return zones;
}

-(void) filterProjectWithSelectedTag {
    
    if (selectedTag != nil) {
        
        // set back button visable
        //[_backBu setAlpha:1];
        
        filteredProjArr = [[NSMutableArray alloc]init];
        
        dataDef = [tempDataDef mutableCopy];
        
        for (projectItem *proj in dataDef) {
            if ([isArabic ? proj.locationTag[@"name_ar"] : proj.locationTag[@"name"]  isEqualToString:selectedTag]){
                [filteredProjArr addObject:proj];
            }
        }
        
        displayFilterData = YES;
        
        [dataDef removeAllObjects];
        dataDef = [filteredProjArr mutableCopy];
        
        [_areaSettTable reloadData];
        [_areaSettTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
    
}
#pragma mark : Button Actions

- (IBAction)filterClicked:(id)sender {
    [_filterTableView reloadData];

    // display filter view
    [_dimView setHidden:NO];
    [_filterView setHidden:NO];
}

- (IBAction)menuClicked:(id)sender {
    
}

- (IBAction)backClicked:(id)sender {
    
    if (displayFilterData) {
        [dataDef removeAllObjects];
        dataDef = tempDataDef;
        
        displayFilterData = !displayFilterData;
        
        [_areaSettTable reloadData];
        
        // set back button invisable
       // [_backBu setAlpha:0];
        
    } else {
        
        UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
        [self.navigationController pushViewController:vc animated:YES];
        //[self.navigationController popViewControllerAnimated:YES];
    }

}
@end

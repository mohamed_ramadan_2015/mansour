//
//  ShowImageViewController.h
//  Mansour
//
//  Created by M R on 10/31/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowImageViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (strong, nonatomic) NSString * imgString;

@property (strong, nonatomic) UIImage * img;

@property (weak, nonatomic) IBOutlet UICollectionView * imagesCollection;
@property (nonatomic) NSArray * imagesArr;
@property (nonatomic) NSInteger currentIndex;
@end

//
//  projectsViewController.h
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@interface projectViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UIView *dimView;
@property (weak, nonatomic) IBOutlet UIView *filterView;
@property (weak, nonatomic) IBOutlet UITableView *filterTableView;
@property (weak, nonatomic) IBOutlet UILabel *filterHeaderLbl;

@property (weak, nonatomic) IBOutlet UIImageView * anim1ImageV;

@property (weak, nonatomic) IBOutlet UIImageView * anim2ImageV;

@property (weak, nonatomic) IBOutlet UIImageView * anim3ImageV;

@property (weak, nonatomic) IBOutlet UIImageView * anim4ImageV;

@property (weak, nonatomic) IBOutlet UIView *projectsView;
@property (weak, nonatomic) IBOutlet UILabel *projectlb;
@property (weak, nonatomic) IBOutlet UIButton *menuBu;
@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UITableView *areaSettTable;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)menuClicked:(id)sender;

- (IBAction)backClicked:(id)sender;

@end

//
//  ShowImageViewController.m
//  Mansour
//
//  Created by M R on 10/31/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "ShowImageViewController.h"
#import "FullScreenCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@import Firebase;

#define cellId @"cell"

@interface ShowImageViewController ()

@end

@implementation ShowImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([DataClass getInstance].ar)
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
    }
    
    if (_img != nil) {
        _imageView.image = _img;
    }
    
    // register collection cell
    UINib *nib = [UINib nibWithNibName:@"FullScreenCollectionViewCell" bundle:[NSBundle mainBundle]];
    [_imagesCollection registerNib:nib forCellWithReuseIdentifier:cellId];
    
    _imagesCollection.dataSource = self;
    _imagesCollection.delegate = self;
    _imagesCollection.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    _imagesCollection.opaque = NO;
    _imagesCollection.showsHorizontalScrollIndicator=NO;
    _imagesCollection.pagingEnabled = YES;
}

-(void)viewDidAppear:(BOOL)animated {
    // log screen display
    [FIRAnalytics setScreenName:@"Display Iamge Screen" screenClass:@"ShowImageViewController"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark : collection view data source
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if ([collectionView isEqual:_imagesCollection]) {
        return [self.imagesArr count];
    }
    
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([collectionView isEqual:_imagesCollection]) {
        FullScreenCollectionViewCell *cell = [_imagesCollection dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    
        NSString* imgUrlText = [self.imagesArr objectAtIndex:indexPath.row];
        
        [cell.indicator startAnimating];
        cell.indicator.hidden=NO;
        
        [cell setup];
        
        [cell.imageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrlText] placeholderImage:nil options:SDWebImageRefreshCached progress:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            [cell.indicator stopAnimating];
            cell.indicator.hidden=YES;
        }];
        
        return cell;
    }
    
    return nil;
}

#pragma mark : collection view delegate

#pragma mark : collection view delegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0f;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self sizingForRowAtIndexPath:indexPath];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0f,0.0f, 0.0, 0.0f);
}

- (CGSize)sizingForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CGSize cellSize = CGSizeZero;
    
    cellSize.width=self.view.bounds.size.width ;
    
    cellSize.height=self.view.bounds.size.height ;
    
    NSLog(@"cellSize: %@", NSStringFromCGSize(cellSize));
    
    return cellSize;
}



#pragma mark - scroll view delegate
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return _imageView;
}

@end

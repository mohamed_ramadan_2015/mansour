//
//  proDetailViewController.h
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "projectItem.h"
#import "YTPlayerView.h"
#import "ShowImageViewController.h"

@interface proDetViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,YTPlayerViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView * anim1ImageV;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint * anim1ImageLedCon;

@property (weak, nonatomic) IBOutlet UICollectionView *upCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *dowCollectionView;

@property (weak, nonatomic) IBOutlet UIImageView *bottomView;

@property (weak, nonatomic) IBOutlet UIImageView *topView;

@property (weak, nonatomic) IBOutlet UIButton *menuBu;

@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UILabel *proTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *mapMarkerLbl;

@property (weak, nonatomic) IBOutlet UIButton *mapLocationLbl;

@property (weak, nonatomic) IBOutlet UILabel *fromToLbl;

@property (weak, nonatomic) IBOutlet UILabel *priceIconLbl;

@property (weak, nonatomic) IBOutlet UILabel *boughtLbl;

@property (weak, nonatomic) IBOutlet UILabel *remLbl;

@property (weak, nonatomic) IBOutlet UIButton *shareBu;


@property (weak, nonatomic) IBOutlet UIButton *collRightArrowBtn;
@property (weak, nonatomic) IBOutlet UIButton *collLiftArrowBtn;

@property (weak, nonatomic) IBOutlet UIView *collRightArrowView;
@property (weak, nonatomic) IBOutlet UIView *collLiftArrowView;

@property (weak, nonatomic) IBOutlet UILabel *descLbl;

@property (weak, nonatomic) IBOutlet UILabel *areaLbl;

@property (weak, nonatomic) IBOutlet UILabel *avilAreaLbl;
@property (weak, nonatomic) IBOutlet UILabel *availableAreaLbl;

@property (weak, nonatomic) IBOutlet UILabel *mapMarkerLbl2;
@property (weak, nonatomic) IBOutlet UILabel *projAdreesLbl;

@property (weak, nonatomic) IBOutlet UIView *priceView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *upCollViewHeightCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dowCollViewHeightCons;
@property (weak, nonatomic) IBOutlet UIView *dowView;

@property(nonatomic, strong) IBOutlet YTPlayerView *playerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bigViewHeightCons;

- (IBAction)menuClicked:(id)sender;

@property (nonatomic, strong) projectItem* project;
@property (nonatomic,strong) NSString* comeFrom;

@end

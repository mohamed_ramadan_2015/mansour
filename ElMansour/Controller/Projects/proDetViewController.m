//
//  proDetailViewController.m
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "proDetViewController.h"
#import "upCollectionViewCell.h"
#import "dowCollectionViewCell.h"
#import "SWRevealViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSObject+BVJSONString.h"

#define external @"external"
#define intersection @"intersection"

#define iPhoneFontSize 12
#define iPadFontSize 17

@import Firebase;

static NSString*kCollectionViewCellIdentifier1 = @"up";
static NSString*kCollectionViewCellIdentifier2 = @"dow";
@interface proDetViewController (){
    
    NSString*filePathC;
    NSString *libraryDirectory;
    
    CGFloat upCollViewHeightConsTemp;
    CGFloat dowCollViewHeightConsTemp;
    
    int scrollIndex;
    int maxDisplayedCellCount;
    
    NSInteger visiblePage;
    NSInteger collectionScrollDirection;
    
    NSUserDefaults*defaults;
    
    bool isIPhone;
    
    int i;
}

@end

@implementation proDetViewController

#pragma mark : View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];

    defaults = [NSUserDefaults standardUserDefaults];
    
    i = 1;
    
    if(self.revealViewController != nil){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        if ([DataClass getInstance].ar) {
            [_menuBu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rightViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        } else {
            [_menuBu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rearViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        }
        
    } else {
        
        NSLog(@"dfsdfsdfs");
    }
    
    
    // increase project views
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/Projects/%@/viewed",Domain,_project.id];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         
         if (error == nil) {
             NSLog(@"project views increased successfully");
         }
         
     }];
    
    
    //********
    [self setupProjectImage];
    
    [self setup];
    
    
    if([DataClass getInstance].ar)
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
        
        collectionScrollDirection = UICollectionViewScrollPositionRight;
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
        
        collectionScrollDirection = UICollectionViewScrollPositionLeft;
    }
    
    
    
    
    self.playerView.delegate = self;
    NSDictionary *playerVars = @{
                                 @"playsinline" : @1,
                                 };
    // NSString * youtubeString = _item.youtubeStr;
    if (![_project.youtubeStr isEqualToString:@""]) {
        
        [self.playerView loadWithVideoId:[self getYoutubeIdFromLink:_project.youtubeStr] playerVars:playerVars];
        
    }else {
        
    }
    
    // create folder for current project external images if not exist
    [self createSubFolder:external ForProject:_project.id];
    
    // create folder for current project intersection images if not exist
    [self createSubFolder:intersection ForProject:_project.id];
    
    
    _mapMarkerLbl.text = [NSString stringWithFormat:@"%C",0xf041];
    _mapMarkerLbl2.text = [NSString stringWithFormat:@"%C",0xf041];
    
    if ([DataClass getInstance].ar) {
        _proTitleLbl.text = _project.textAr;
        _descLbl.text = _project.descriptionAr;
        _avilAreaLbl.text = @"المساحات المتاحة";
        _projAdreesLbl.text = _project.locationAr;
        _areaLbl.text = _project.locationTag[@"name_ar"];
        _fromToLbl.text = [NSString stringWithFormat:@"تبدأ من %@ جنية",_project.avgPrice] ;
        [_mapLocationLbl setTitle:@"مكان الموقع على الخريطة" forState:UIControlStateNormal];
        
        [self.shareBu setTitle:[NSString stringWithFormat:@"%C مشاركة",0xf1e1] forState:UIControlStateNormal];
        
        [_collLiftArrowBtn setTitle:[NSString stringWithFormat:@"%C",0xf061] forState:UIControlStateNormal];
        [_collRightArrowBtn setTitle:[NSString stringWithFormat:@"%C",0xf060] forState:UIControlStateNormal];
    } else {
        _proTitleLbl.text = _project.textEn;
        _descLbl.text = _project.descriptionEn;
        _avilAreaLbl.text = @"Available Areas";
        _projAdreesLbl.text = _project.locationEn;
        _areaLbl.text = _project.locationTag[@"name"];
        _fromToLbl.text = [NSString stringWithFormat:@"Start From %@ EGY",_project.avgPrice] ;
        [_mapLocationLbl setTitle:@"Location on map" forState:UIControlStateNormal];
        
        [_shareBu setTitle:[NSString stringWithFormat:@"Share %C",0xf1e1] forState:UIControlStateNormal];
        
        [_collLiftArrowBtn setTitle:[NSString stringWithFormat:@"%C",0xf060] forState:UIControlStateNormal];
        [_collRightArrowBtn setTitle:[NSString stringWithFormat:@"%C",0xf061] forState:UIControlStateNormal];
    }
    
    _boughtLbl.text = [NSString stringWithFormat:@"%@ %@",_project.amountSold,NSLocalizedString(@"unit", nil)];
    
    _remLbl.text = [NSString stringWithFormat:@"%@ %@",_project.amountRemaining,NSLocalizedString(@"unit", nil)];
    
    
    
    _availableAreaLbl.text = _project.availableAreas;
    
    
    
    UINib *nib = [UINib nibWithNibName:
                  NSStringFromClass([upCollectionViewCell class])
                                bundle:[NSBundle mainBundle]];
    
    [_upCollectionView registerNib:nib forCellWithReuseIdentifier:kCollectionViewCellIdentifier1];
    
    
    _upCollectionView.dataSource = self;
    _upCollectionView.delegate = self;
    _upCollectionView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    _upCollectionView.opaque = NO;
    _upCollectionView.showsHorizontalScrollIndicator=NO;
    _upCollectionView.scrollEnabled = NO;
    
    
    UINib *nib1 = [UINib nibWithNibName:
                   NSStringFromClass([dowCollectionViewCell class])
                                 bundle:[NSBundle mainBundle]];
    
    [_dowCollectionView registerNib:nib1 forCellWithReuseIdentifier:kCollectionViewCellIdentifier2];
    
    
    
    _dowCollectionView.dataSource = self;
    _dowCollectionView.delegate = self;
    _dowCollectionView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    _dowCollectionView.opaque = NO;
    _dowCollectionView.showsHorizontalScrollIndicator=NO;

}

-(void)viewWillAppear:(BOOL)animated {
    // initailize scrolling index with Zero
    scrollIndex = 2;
    
    //[_collLiftArrowBtn setAlpha:0.5];
    //[_collLiftArrowBtn setEnabled:NO];
    [_collLiftArrowView setHidden:YES];
    
    if([_project.externalImages count] == 0){
        //[_collRightArrowBtn setAlpha:0.5];
        //[_collRightArrowBtn setEnabled:NO];
        [_collRightArrowView setHidden:YES];
    }
    
    if([_project.externalImages count] == 0){
        upCollViewHeightConsTemp = _upCollViewHeightCons.constant;
        _upCollViewHeightCons.constant = 0;
    }
    
    if([_project.intersectionImages count] == 0){
        
        _dowView.clipsToBounds = YES;
        dowCollViewHeightConsTemp = _dowCollViewHeightCons.constant;
        _dowCollViewHeightCons.constant = 0;
    }
}

-(void)viewDidAppear:(BOOL)animated {
    
    // log screen display
    [FIRAnalytics setScreenName:@"Project Details Screen" screenClass:@"proDetViewController"];
    
    
    
    if([_project.externalImages count] == 0){
        _bigViewHeightCons.constant -= upCollViewHeightConsTemp;
    }
    
    if([_project.intersectionImages count] == 0){
        _bigViewHeightCons.constant -= dowCollViewHeightConsTemp;
    }

    
    // determine description text height
    CGFloat desTextHeight;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        isIPhone = YES;
        
        desTextHeight = [self getProjectDescTextHeight];
        
        self.bigViewHeightCons.constant += desTextHeight;
        
        // last cell index displayed on the screen
        visiblePage = 1;
        maxDisplayedCellCount = 2;
    } else {
        isIPhone = NO;
        
        desTextHeight = [self getProjectDescTextHeight];
        
        self.bigViewHeightCons.constant += desTextHeight;
        
        // last cell index displayed on the screen
        visiblePage = 2;
        maxDisplayedCellCount = 3;
    }
    
}

#pragma mark : Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView :(UICollectionView *)collectionView {
    return 1   ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    
    if(collectionView == self.upCollectionView) {
        return [self.project.externalImages count];
    } else if (collectionView == self.dowCollectionView) {
        return [self.project.intersectionImages count];
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(collectionView == self.upCollectionView)
    {
        upCollectionViewCell *cell =
        [collectionView
         dequeueReusableCellWithReuseIdentifier:kCollectionViewCellIdentifier1
         forIndexPath:indexPath];
        
        cell.previewImageV.image = [UIImage imageNamed:@"logo.png"];
        
        filePathC = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"projectImages/%@/%@",_project.id,external]];
        
        NSString* igmUrlText = [_project.externalImages objectAtIndex:indexPath.item];
        
        NSString* urlWithoutExtension = [igmUrlText substringToIndex:[igmUrlText length]-4];
        NSString* imageExtension = [igmUrlText substringFromIndex:[igmUrlText length]-4];
        NSString* imgUrlText = [NSString stringWithFormat:@"%@_medium%@",urlWithoutExtension,imageExtension];
        NSString* imgUrlTextEscaped = [imgUrlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        [cell.activity startAnimating];
        cell.activity.hidden=NO;
        
        [cell.previewImageV sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrlTextEscaped] placeholderImage:[UIImage imageNamed:@"logo.png"] options:SDWebImageRefreshCached progress:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            [cell.activity stopAnimating];
            cell.activity.hidden=YES;
        }];

        return cell;
        
    } else  if(collectionView == self.dowCollectionView) {
        
        dowCollectionViewCell *cell =
        [collectionView
         dequeueReusableCellWithReuseIdentifier:kCollectionViewCellIdentifier2
         forIndexPath:indexPath];
        
        cell.previewImageV.image = [UIImage imageNamed:@"logo.png"];
        
        filePathC = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"projectImages/%@/%@",_project.id,intersection]];
        
        NSString* imgUrlText = [_project.intersectionImages objectAtIndex:indexPath.item];
        
        NSString* urlWithoutExtension = [imgUrlText substringToIndex:[imgUrlText length]-4];
        NSString* imageExtension = [imgUrlText substringFromIndex:[imgUrlText length]-4];
        imgUrlText = [NSString stringWithFormat:@"%@_medium%@",urlWithoutExtension,imageExtension];
        NSString* imgUrlTextEscaped = [imgUrlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [cell.activity startAnimating];
        cell.activity.hidden=NO;
        
        [cell.previewImageV sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrlTextEscaped] placeholderImage:[UIImage imageNamed:@"logo.png"] options:SDWebImageRefreshCached progress:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            [cell.activity stopAnimating];
            cell.activity.hidden=YES;
        }];
        
        return cell;
        
    }
    
    return nil;
}


#pragma mark : Collection View Delegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0f;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0f;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize cellSize = CGSizeZero;
    
    if (collectionView == _upCollectionView) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            cellSize.width = (self.upCollectionView.bounds.size.width-10) / 2;
            cellSize.height = self.upCollectionView.bounds.size.height;
        } else {
            cellSize.width = (self.upCollectionView.bounds.size.width-15) / 3;
            cellSize.height = self.upCollectionView.bounds.size.height;
        }
        
        NSLog(@"cellSize: %@", NSStringFromCGSize(cellSize));
        
    } else if (collectionView == _dowCollectionView) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            cellSize.width = (self.dowCollectionView.bounds.size.width-10) / 2;
            cellSize.height = self.dowCollectionView.bounds.size.height;
        } else {
            cellSize.width = (self.dowCollectionView.bounds.size.width-15) / 3;
            cellSize.height = self.dowCollectionView.bounds.size.height;
        }
        
        NSLog(@"cellSize: %@", NSStringFromCGSize(cellSize));
        
    }

    return cellSize;
    
    //return [self sizingForRowAtIndexPath:indexPath];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5.0f, 5.0f, 5, 5.0f);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ShowImageViewController *vc = [sb instantiateViewControllerWithIdentifier:@"ShowImageViewController"];
    
    if (collectionView == _upCollectionView) {
        
        upCollectionViewCell *cell = (upCollectionViewCell*) [collectionView cellForItemAtIndexPath:indexPath];
        vc.img = cell.previewImageV.image;
        vc.imgString = [_project.externalImages objectAtIndex:indexPath.item];
        vc.imagesArr = _project.externalImages;
    
    } else if (collectionView == _dowCollectionView) {
       
        dowCollectionViewCell *cell = (dowCollectionViewCell*) [collectionView cellForItemAtIndexPath:indexPath];
        vc.img = cell.previewImageV.image;
        vc.imgString = [_project.intersectionImages objectAtIndex:indexPath.item];
        vc.imagesArr = _project.intersectionImages;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(collectionView == _upCollectionView) {
        /*
        if(indexPath.item == [_project.externalImages count]-1){
            _collRightArrowBtn.alpha = 0.5;
            [_collRightArrowBtn setEnabled:NO];
            [_collRightArrowView setHidden:YES];
        }else {
            _collRightArrowBtn.alpha = 1.0;
            [_collRightArrowBtn setEnabled:YES];
            
            [_collRightArrowView setHidden:NO];
        }
        
        if(indexPath.item == 0){
            _collLiftArrowBtn.alpha = 0.5;
            [_collLiftArrowBtn setEnabled:NO];
            [_collLiftArrowView setHidden:YES];
        }else {
            _collLiftArrowBtn.alpha = 1.0;
            [_collLiftArrowBtn setEnabled:YES];
            [_collLiftArrowView setHidden:NO];
        }
        */
    } else if (collectionView == _dowCollectionView) {
        
        
    }

    
}

- (CGSize)sizingForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CGSize cellSize = CGSizeZero;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        cellSize.width = (self.upCollectionView.bounds.size.width-10) / 2;
        cellSize.height = self.upCollectionView.bounds.size.height;
    } else {
        cellSize.width = (self.upCollectionView.bounds.size.width-15) / 3;
        cellSize.height = self.upCollectionView.bounds.size.height;
    }
    
    NSLog(@"cellSize: %@", NSStringFromCGSize(cellSize));
    
    return cellSize;
}

#pragma mark : Button Actions
- (IBAction)scrollRightClicked:(id)sender {
    int countOfCells = isIPhone ? 2 : 3;
    if (i < [_project.externalImages count]-2) {
        [_upCollectionView setContentOffset:CGPointMake(i * (_upCollectionView.frame.size.width/ countOfCells), 0) animated:YES];
        i += 1;
        
        [_collLiftArrowView setHidden:NO];
        
        if (i == [_project.externalImages count]-1) {
            [_collRightArrowView setHidden:YES];
            i -= 1;
        }

    } else {
        [_collRightArrowView setHidden:YES];
    }

}

- (IBAction)scrollLiftClicked:(id)sender {
    // number of visible cells in device screen
    int countOfCells = isIPhone ? 2 : 3;
    if (i > 0) {
        i -= 1;
        [_upCollectionView setContentOffset:CGPointMake(i * (_upCollectionView.frame.size.width/ countOfCells), 0) animated:YES];
        
        [_collRightArrowView setHidden:NO];
        
        if (i == 0) {
            [_collLiftArrowView setHidden:YES];
            i += 1;
        }
    } else {
        [_collLiftArrowView setHidden:YES];
    }
}


- (IBAction)menuClicked:(id)sender {
    
}

- (IBAction)shareClicked:(id)sender {
    NSString *projURL= @"";;
    
    if ([DataClass getInstance].ar) {
        projURL = _project.shareAr;
    } else {
        projURL = _project.shareEn;
    }
    
    
    NSArray *objectsToShare = @[projURL];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)inView:self.view permittedArrowDirections:0 animated:YES];
 
    } else {
        
        [self presentViewController:activityVC animated:YES completion:nil];
    }
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)locationClicked:(id)sender {
    if (![_project.lat isEqualToString:@""]) {
        [self showLocOnMap];
    }
    
}


- (IBAction)recerveUnitClicked:(id)sender {
    
    // check if user logged in
    if ([[NSUserDefaults standardUserDefaults] boolForKey:isLoggedIn]) {
        
        // get cashed user
        NSData *userData = [[NSUserDefaults standardUserDefaults] objectForKey:kCashedUser];
        IdentifiedUser *user = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
        
        [DataClassH shared].user = user;
        
        // check if guest or identified user
        if ([user.id isEqualToString:@""]) {
            // guest user just have name and phone number.
            
            [self createUser:user];
            
        } else {
            // real (identified) user have id, name and phone number.
            
            UIStoryboard*stor = [UIStoryboard storyboardWithName:@"Helprox" bundle:nil];
            
            UIViewController*vc = [stor instantiateViewControllerWithIdentifier:@"ticketsView"];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    } else {
        // user not logged in
        
        UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginView"];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
    /*
     UIStoryboard*stor = [UIStoryboard storyboardWithName:@"Helprox" bundle:nil];
     
     UIViewController*vc = [stor instantiateViewControllerWithIdentifier:@"splashView"];
     
     [self.navigationController pushViewController:vc animated:YES];
     */
    
}

#pragma mark : private functions

-(void)createUser:(IdentifiedUser*)user
{
    
    NSLog(@"fcasdasdasdasdasd : %@",user);
    
    [AppDelegate addHoverView:self.view];
    
    NSMutableDictionary*dic = [NSMutableDictionary new];
    
    [dic setObject:@"" forKey:@"user_id"];
    
    [dic setObject:user.type forKey:@"type"];
    
    [dic setObject:user.firstName forKey:@"first_name"];
    
    [dic setObject:user.lastName forKey:@"last_name"];
    
    [dic setObject:user.phone forKey:@"phone"];
    
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@apis/v1/createUser",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_data=%@",[DataClassH shared].apiKey,@"ios", [dic jsonString]];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             NSLog(@"createUser respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     NSLog(@"createUser success");
                     
                     
                     
                     user.id = responseDict[@"user_id"];
                     
                     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
                     
                     [defaults setObject:data forKey:kCashedUser];
                     
                     [defaults synchronize];
                     
                     
                     [DataClassH shared].user = user ;
                     
                     [self saveToken];
                     
                     
                 }
                 
                 else
                 {
                     
                     
                     
                     NSLog(@"createUser request fail reason %@",responseDict[@"reason"]);
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"createUser nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"createUser error : %@",error);
             
             dispatch_queue_t mainQueue = dispatch_get_main_queue();
             
             dispatch_async(mainQueue, ^(void) {
                 
                 [AppDelegate removeHoverView:self.view];
                 
             });
             
         }
         
     }];
    
    
}


-(void)saveToken
{
    
    NSLog(@"saveToken2001455555");
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@apis/v1/saveToken",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_type=%@&user_id=%@&fcm_token=%@",[DataClassH shared].apiKey,@"ios",[DataClassH shared].user.type,[DataClassH shared].user.id,[defaults valueForKey:kFCMTokenHelprox]];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             
             NSLog(@"saveToken respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     
                     
                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                     
                     dispatch_async(mainQueue, ^(void) {
                         
                         [AppDelegate removeHoverView:self.view];
                         
                         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Helprox" bundle:nil];
                         
                         UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ticketsView"];
                         
                         [self.navigationController pushViewController:vc animated:YES];
                     });
                     
                     
                 }
                 
                 else
                 {
                     
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"saveToken nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"saveToken error : %@",error);
             
             
             dispatch_queue_t mainQueue = dispatch_get_main_queue();
             
             dispatch_async(mainQueue, ^(void) {
                 
                 [AppDelegate removeHoverView:self.view];
                 
             });
             
             
         }
         
     }];
    
    
}

-(void) showLocOnMap {
    NSString *longtude = _project.lon;
    NSString *latitude = _project.lat;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=&daddr=%@,%@",latitude,longtude]];
    [[UIApplication sharedApplication] openURL:url];
}

- (NSString *)getYoutubeIdFromLink:(NSString *)link {
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,[link length])];
    if (array.count > 0) {
        NSTextCheckingResult *result = array.firstObject;
        
        return [link substringWithRange:result.range];
    }
    
    return nil;
}


-(float) getProjectDescTextHeight{
    
    if([DataClass getInstance].ar)
    {
        return [self getTextHeight:_project.descriptionAr withFont:isIPhone ? iPhoneFontSize : iPadFontSize];
    }
    else
    {
        return [self getTextHeight:_project.descriptionEn withFont:isIPhone ? iPhoneFontSize : iPadFontSize];
    }
}

-(float) getTextHeight:(NSString*)text withFont:(CGFloat) fontSize {
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width*0.8, CGFLOAT_MAX)];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.font = [UIFont fontWithName:@"CoconNextArabic-Regular" size:fontSize];
    lbl.text = text;
    
    [lbl sizeToFit];
    
    NSLog(@"fontSize = %f\tbounds = (%f x %f)",
          fontSize,
          lbl.frame.size.width,
          lbl.frame.size.height);
    
    return lbl.frame.size.height;
}

-(void) setup {

    _priceView.layer.cornerRadius = 3;
    _priceView.clipsToBounds = YES;
    
    _collRightArrowView.layer.cornerRadius = 5;
    _collRightArrowView.clipsToBounds = YES;
    
    _collLiftArrowView.layer.cornerRadius = 5;
    _collLiftArrowView.clipsToBounds = YES;
}

-(void) setupProjectImage {
    
    NSString* urlWithoutExtension = [_project.mainImgStr substringToIndex:[_project.mainImgStr length]-4];
    NSString* imageExtension = [_project.mainImgStr substringFromIndex:[_project.mainImgStr length]-4];
    NSString* imgUrlText = [NSString stringWithFormat:@"%@_medium%@",urlWithoutExtension,imageExtension];
    NSString* imgUrlTextEscaped = [imgUrlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [_anim1ImageV sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrlTextEscaped] placeholderImage:nil options:SDWebImageRefreshCached progress:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
    }];
    
}


-(void) createSubFolder:(NSString*)subFolder ForProject:(NSString*) pro {
    NSError*error=nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    libraryDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/Application Support"];
    
    filePathC = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"projectImages/%@/%@",pro,subFolder]];
    
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:filePathC])
    {
        if(
           
           [[NSFileManager defaultManager] createDirectoryAtPath:filePathC
                                     withIntermediateDirectories:YES
                                                      attributes:nil
                                                           error:&error])
        {
            
            NSLog(@"%@ created",subFolder);
        }
        else
        {
            
            NSLog(@"%@ not created",subFolder);
        }
    }
    else
    {
        
        NSLog(@"%@ exists",subFolder);
    }
}

@end

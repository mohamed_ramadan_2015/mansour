//
//  SideMenuViewController.h
//  Mansour
//
//  Created by M R on 11/1/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AccountKit/AccountKit.h>

@interface SideMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,AKFViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *logoView;

@property (weak, nonatomic) IBOutlet UIView *userView;

@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@property (weak, nonatomic) IBOutlet UIButton *loginBu;

@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;

@end

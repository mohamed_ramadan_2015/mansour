//
//  SideMenuViewController.m
//  Mansour
//
//  Created by M R on 11/1/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "SideMenuViewController.h"
#import "DataClass.h"
#import "SideMenuTableViewCell.h"
#import "projectViewController.h"
#import "mainCollectionViewCell.h"
#import "SWRevealViewController.h"
#import "mainNavController.h"

#define menuCellIdentifier @"menuCell"
#define mainSegue @"mainSegue"
#define projectSegue @"projectSegue"
#define newsSegue @"newsSegue"
#define articleSegue @"articleSegue"
#define aboutSegue @"aboutSegue"
#define loginSegue @"loginSegue"
#define instalmentSegue @"instalmentSegue"

@import Firebase;

@interface SideMenuViewController (){
    
    NSMutableArray *images;
    NSMutableArray *titlesAr;
    
    NSMutableArray *titlesEn;
    
    AKFAccountKit *_accountKit;
}

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self setupUI];
    
    // initialize Account Kit
    if (_accountKit == nil) {
        // may also specify AKFResponseTypeAccessToken
        _accountKit = [[AKFAccountKit alloc] initWithResponseType:AKFResponseTypeAuthorizationCode];
    }
    
    images = [[NSMutableArray alloc] initWithObjects:[UIImage imageNamed:@"home"],
              [UIImage imageNamed:@"projects"],
              [UIImage imageNamed:@"news"],
              [UIImage imageNamed:@"articles"],
              [UIImage imageNamed:@"contact"],
              [UIImage imageNamed:@"about"],
              [UIImage imageNamed:@"Instalments"],
              [UIImage imageNamed:@"share"],nil];
    
    titlesAr = [[NSMutableArray alloc] initWithObjects:@"الرئيسية",
                @"المشاريع",
              @"الاخبار",
              @"المقالات",
              @"تواصل معنا",
              @"عن الشركة",
              @"الاقساط",
              @"ارسال التطبيق",nil];
    
    titlesEn = [[NSMutableArray alloc] initWithObjects:@"Main Page",
                @"Projects",
                @"News",
                @"Articles",
                @"Contact Us",
                @"About",
                @"Installment",
                @"Share App",nil];
    
    
    UINib * nib = [UINib nibWithNibName:@"SideMenuTableViewCell" bundle:nil];
    [_menuTableView registerNib:nib forCellReuseIdentifier:menuCellIdentifier];
    
    _menuTableView.delegate = self;
    
    _menuTableView.dataSource = self;
    
    [_menuTableView setBounces:NO];
    
    _menuTableView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    
    _menuTableView.opaque = NO;
    
    _menuTableView.separatorColor = [UIColor clearColor];
    
    _menuTableView.showsVerticalScrollIndicator = NO;
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    if ([NSUserDefaults.standardUserDefaults boolForKey:isLoggedIn]) {
        if ([DataClass getInstance].ar) {
            [_loginBu setTitle:@"تسجيل الخروج" forState:UIControlStateNormal];
        } else {
            [_loginBu setTitle:@"Logout" forState:UIControlStateNormal];
        }
        
        [_userNameLbl setHidden:NO];
        
        if ([NSUserDefaults.standardUserDefaults boolForKey:isGuestUser]) {
            _userNameLbl.text = [NSUserDefaults.standardUserDefaults objectForKey:userPhoneNumber];
        } else {
            _userNameLbl.text = [NSUserDefaults.standardUserDefaults objectForKey:userName];
        }
    } else {
        
        [_userNameLbl setHidden:YES];
        
        if ([DataClass getInstance].ar) {
            [_loginBu setTitle:@"تسجيل الدخول" forState:UIControlStateNormal];
        } else {
            [_loginBu setTitle:@"Login" forState:UIControlStateNormal];
        }
        
        [_userNameLbl setHidden:YES];
    }
    
}

-(void)viewDidAppear:(BOOL)animated {
    [self setupUI];
    
    [FIRAnalytics setScreenName:@"Side Menu Screen" screenClass:@"SideMenuViewController"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark : Table View Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [titlesAr count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:_menuTableView]) {
        
        SideMenuTableViewCell *cell = [_menuTableView dequeueReusableCellWithIdentifier:menuCellIdentifier];
        
        cell.image.image = [images objectAtIndex:indexPath.row];
        
        if ([DataClass getInstance].ar) {
            cell.title.text = [titlesAr objectAtIndex:indexPath.row];
        } else {
            cell.title.text = [titlesEn objectAtIndex:indexPath.row];
        }
        
        return cell;
    }
    
    return nil;
}

#pragma mark : Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        return 70;
//    } else {
//        return 50;
//    }
    return (self.view.frame.size.height * 0.5)/[titlesEn count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //projectViewController *pvc = [sb instantiateViewControllerWithIdentifier:@"projectsView"];
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:mainSegue sender:self];
            break;
            
        case 1:
            [self performSegueWithIdentifier:projectSegue sender:self];
            break;
            
        case 2:
            [self performSegueWithIdentifier:newsSegue sender:self];
            break;
            
        case 3:
            [self performSegueWithIdentifier:articleSegue sender:self];
            break;
            
        case 4:
            
            break;
            
        case 5:
            [self performSegueWithIdentifier:aboutSegue sender:self];
            break;
            
        case 6:
            [self performSegueWithIdentifier:instalmentSegue sender:self];
            break;
            
        case 7:
           
            [[NSNotificationCenter defaultCenter] postNotificationName:@"shareApp" object:nil];
            break;
            
        default:
            break;
    }
    
}

#pragma mark : Private Methods

- (void)shareApp {
    NSString *AppStoreLink = @"https://itunes.apple.com/us/app/meamar-elmansour/id1323990629?ls=1&mt=8";
    
    NSArray *objectsToShare = @[AppStoreLink];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)inView:self.view permittedArrowDirections:0 animated:YES];
        
    } else {
        
        [self presentViewController:activityVC animated:YES completion:nil];
    }
}

-(void) setupUI {
    
    _logoView.layer.cornerRadius = 30;
    _logoView.clipsToBounds = YES;
    
    _userView.layer.cornerRadius = _userView.frame.size.width/2;
    _userView.clipsToBounds = YES;
    
}

// send device_id empty to handle push notification problem in api,
// the problem is delevering the notification twice because the device_id register twice
// in api. so when the user logout, sending the device_id empty will handle this.

-(void) registerPlatform {
    
    NSLog(@"send platform");
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/client/register",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *userName = [NSString stringWithFormat:@"%@ %@",[DataClassH shared].user.firstName,[DataClassH shared].user.lastName];
    
    NSString *body =[NSString stringWithFormat:@"name=%@&platform=%@&number=%@&device_id=%@",userName,@"ios",[DataClassH shared].user.phone,@""];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             NSLog(@"send platform respo is : %@",responseDict);
             if(responseDict!=nil)
             {
                 if([[responseDict valueForKey:@"success"] integerValue]==1)  {
                     
                 } else {
                     
                 }
             } else {
                 NSLog(@"send platform nil Dic ");
             }
         } else {
             NSLog(@"send platform error : %@",error);
         }
     }];
}

#pragma mark : button actions

- (IBAction)loginClicked:(id)sender {
    if (![NSUserDefaults.standardUserDefaults boolForKey:isLoggedIn]) {
        [self performSegueWithIdentifier:loginSegue sender:self];
    } else {
        // sign out user
        [NSUserDefaults.standardUserDefaults setBool:NO forKey:isLoggedIn];
        [NSUserDefaults.standardUserDefaults removeObjectForKey:isRealUser];
        [NSUserDefaults.standardUserDefaults removeObjectForKey:isGuestUser];
        [NSUserDefaults.standardUserDefaults removeObjectForKey:userId];
        [NSUserDefaults.standardUserDefaults removeObjectForKey:userPhoneNumber];
        [NSUserDefaults.standardUserDefaults removeObjectForKey:userName];
        
        // remove user model
        [NSUserDefaults.standardUserDefaults removeObjectForKey:kCashedUser];
        
        [_accountKit logOut];
        
        // send device_id empty to api
        [self registerPlatform];
        
        [_userNameLbl setHidden:YES];
        
        if ([DataClass getInstance].ar) {
            [_loginBu setTitle:@"تسجيل الدخول" forState:UIControlStateNormal];
        } else {
            [_loginBu setTitle:@"Login" forState:UIControlStateNormal];
        }
    }
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    /*
    if ([segue isKindOfClass:[SWRevealViewControllerSeguePushController class]]) {
        SWRevealViewControllerSeguePushController *swSegue = (SWRevealViewControllerSeguePushController*) segue;
        
        swSegue.perform = ^(SWRevealViewControllerSeguePushController* rvc_segue, UIViewController *svc, UIViewController *dvc){
            UINavigationController *navCon = (UINavigationController*)self.revealViewController.frontViewController;
            [navCon setViewControllers:@[dvc] animated:NO];
            [self.revealViewController setFrontViewPosition:FrontViewPositionRight animated:YES];
        };
    }
    */
    
}


@end

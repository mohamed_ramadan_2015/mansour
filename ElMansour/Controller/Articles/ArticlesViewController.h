//
//  ArticlesViewController.h
//  Mansour
//
//  Created by M R on 11/6/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticlesViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *menuBu;
@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *articleslb;

@property (weak, nonatomic) IBOutlet UICollectionView *articlesCollection;

@property (weak, nonatomic) IBOutlet UIImageView * bottomBackImageV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botBackImageRightCon;


- (IBAction)menuClicked:(id)sender;

- (IBAction)backClicked:(id)sender;


@end

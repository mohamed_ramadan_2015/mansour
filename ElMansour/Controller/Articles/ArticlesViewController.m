//
//  ArticlesViewController.m
//  Mansour
//
//  Created by M R on 11/6/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "ArticlesViewController.h"
#import "SWRevealViewController.h"
#import "ArticleCollectionViewCell.h"
#import "ArticleDetailsViewController.h"
#import "ArticleItem.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define cellIdentifier @"articleCell"
#define KLiveArticles @"KLiveArticles"
@import Firebase;

@interface ArticlesViewController (){
    
    NSString*filePathC;
}
@property(nonatomic,retain)NSMutableArray*dataDef;


@end

@implementation ArticlesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.revealViewController != nil){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        if ([DataClass getInstance].ar) {
            [_menuBu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rightViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        } else {
            [_menuBu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rearViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        }
        
    } else {
        
        NSLog(@"dfsdfsdfs");
    }
    
    if([DataClass getInstance].ar)
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
        
        _articleslb.text = @"المقالات";
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
        
        _articleslb.text = @"Articles";
    }
    
    UINib *nib = [UINib nibWithNibName:
                   NSStringFromClass([ArticleCollectionViewCell class])
                                 bundle:[NSBundle mainBundle]];
    
    [_articlesCollection registerNib:nib forCellWithReuseIdentifier:cellIdentifier];
    
    _articlesCollection.dataSource = self;
    _articlesCollection.delegate = self;
    _articlesCollection.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    _articlesCollection.opaque = NO;
    _articlesCollection.showsHorizontalScrollIndicator=NO;
    
    
    NSError*error=nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/Application Support"];
    
    filePathC = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"LiveArticles"]];
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:filePathC])
    {
        if(
           
           [[NSFileManager defaultManager] createDirectoryAtPath:filePathC
                                     withIntermediateDirectories:YES
                                                      attributes:nil
                                                           error:&error])
        {
            NSLog(@"created");
        }
        else
        {
            NSLog(@"not created");
        }
    }
    else
    {
        NSLog(@"exists");
    }
    
    
    
    
    if ([DataClass isNetworkAvailable]) {
        self.dataDef = [[NSMutableArray alloc] init];
        [self getArticles];
    } else {
        
        NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:KLiveArticles];
        
        NSArray *n2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
        
        self.dataDef =  [[NSMutableArray alloc] initWithArray:n2];
        
        NSLog(@"plaplapla %@", self.dataDef);
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated {
    self.view.clipsToBounds = YES;
    
    [self animateBack];
    
    // log screen display
    [FIRAnalytics setScreenName:@"Articles List Screen" screenClass:@"ArticlesViewController"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark : buttons actions

- (IBAction)menuClicked:(id)sender {
    
}

- (IBAction)backClicked:(id)sender {
    
    UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark : collection view data source
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if ([collectionView isEqual:self.articlesCollection]) {
        return [self.dataDef count];
    }
    
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([collectionView isEqual:_articlesCollection]) {
        ArticleCollectionViewCell *cell = [_articlesCollection dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

        ArticleItem *a1 = [self.dataDef objectAtIndex:indexPath.row];
        
        
        if([DataClass getInstance].ar) {
            cell.titleLb.text = a1.titleAr;
            
            cell.summaryLb.text = a1.descriptionAr;
            
        } else {
            cell.titleLb.text = a1.titleEn;
            
            cell.summaryLb.text = a1.descriptionEn;
        }
        
        if (![a1.youtubeStr isEqualToString:@""]) {
            [cell.youtubeImg setAlpha:1.0];
        } else {
            [cell.youtubeImg setAlpha:0.0];
        }
        
        NSString* imgUrlText = a1.mSquareImgUrl;
        
        NSString* urlWithoutExtension = [imgUrlText substringToIndex:[imgUrlText length]-4];
        NSString* imageExtension = [imgUrlText substringFromIndex:[imgUrlText length]-4];
        imgUrlText = [NSString stringWithFormat:@"%@_medium%@",urlWithoutExtension,imageExtension];
        NSString* imgUrlTextEscaped = [imgUrlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        [cell.activity startAnimating];
        cell.activity.hidden=NO;
        
        [cell.leftImageV sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrlTextEscaped] placeholderImage:nil options:SDWebImageRefreshCached progress:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            [cell.activity stopAnimating];
            cell.activity.hidden=YES;
        }];
        
        return cell;
    }
    
    return nil;
}

#pragma mark : collection view delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView == _articlesCollection){
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            // The device is an iPad running iOS 3.2 or later.
            
            return CGSizeMake(0.47*self.view.bounds.size.width,0.47*self.view.bounds.size.width);
            
            
        }
        else
        {
            // The device is an iPhone or iPod touch.
            
            return CGSizeMake(0.45*self.view.bounds.size.width,0.45*self.view.bounds.size.width);
        }
        
      }

    return CGSizeMake(0.4*self.view.bounds.size.width,0.4*self.view.bounds.size.width);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([collectionView isEqual:_articlesCollection]) {
        ArticleDetailsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ArticleDetailsViewController"];
        
        ArticleItem *item = [self.dataDef objectAtIndex:indexPath.item];
        
        vc.item = item;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark: Private Methods

-(void)animateBack
{
    self.botBackImageRightCon.constant = 0;
    
    self.botBackImageRightCon.constant+=self.view.bounds.size.width;
    
    [UIView animateWithDuration:40.0 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        self.botBackImageRightCon.constant = 0;
        
        [UIView animateWithDuration:40.0 animations:^{
            
            [self.view layoutIfNeeded];
            
            
        } completion:^(BOOL finished) {
            
            [self animateBack ];
            
        }] ;
        
    }] ;
    
}

-(void)getArticles {
    
    [AppDelegate addHoverView:self.view];
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/Articles?active=true",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:10.0f];
    [urlRequest setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if (response == nil) {
             // timeout of failed
             [self getArticles];
             
         } else {
             // continue
         }
         
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             NSLog(@"getNews respo : %@",responseDict);
             
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 
                 
                 
             });
             
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"status_code"] integerValue]==200)
                 {
                     NSLog(@"Cancel request success");
                     
                     
                     
                     NSArray*all = responseDict[@"data"];
                     
                     for (int i = 0; i<all.count; i++)
                     {
                         
                         NSDictionary*dic = all[i];
                         
                         NSString*titleEn = dic[@"title_en"];
                         
                         NSString*titleAr = dic[@"title_ar"];
                         
                         NSString*descriptionEn = dic[@"description_en"];
                         
                         NSString*descriptionAr = dic[@"description_ar"];
                         
                         NSString*youtubeStr = dic[@"youtube_url"];
                         
                         NSString*mainImgStr = dic[@"basic_image_url"];
                         
                         NSString*Id = dic[@"id"];
                         
                         NSString*htmlAr = dic[@"html_ar"];
                         
                         NSString*htmlEn = dic[@"html_en"];
                         
                         NSString*shareAr = dic[@"shareable_ar_link"];
                         
                         NSString*shareEn = dic[@"shareable_en_link"];
                         
                         NSString*mSquareImgUrl = dic[@"m_square_img_url"];
                         
                         ArticleItem *n1 = [[ArticleItem alloc ] initWithTitleEn:titleEn titleAr:titleAr descriptionEn:descriptionEn descriptionAr:descriptionAr  Id:Id youtubeStr:youtubeStr mainImgStr:mainImgStr htmlAr:htmlAr htmlEn:htmlEn shareAr:shareAr shareEn:shareEn mSquareImgUrl:mSquareImgUrl];
                         
                         [self.dataDef addObject:n1];
                     }
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         [AppDelegate removeHoverView:self.view];
                         
                         [self.articlesCollection reloadData];
                         
                     });
                     
                     
                     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.dataDef];
                     
                     NSUserDefaults*defaults =  [NSUserDefaults standardUserDefaults];
                     
                     [defaults setObject:data forKey:KLiveArticles];
                     
                     [defaults synchronize];
                     
                 } else {
                     NSLog(@"getNews  fail");
                 }
             }
             else
             {
                 NSLog(@"getNews Dic ");
             }
             
         }
         else
         {
             NSLog(@"getNews error: %@",error);
         }
         
     }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

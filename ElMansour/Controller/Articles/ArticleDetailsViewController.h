//
//  ArticleDetailsViewController.h
//  Mansour
//
//  Created by M R on 11/6/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleItem.h"
#import "YTPlayerView.h"

@interface ArticleDetailsViewController : UIViewController <UIWebViewDelegate,YTPlayerViewDelegate>

@property (strong, nonatomic) IBOutlet ArticleItem *item;

@property (weak, nonatomic) IBOutlet UIButton *menuBu;
@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UILabel *articleTitleLbl;

@property (weak, nonatomic) IBOutlet UIButton *shareBu;

@property (weak, nonatomic) IBOutlet UIImageView * bottomBackImageV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botBackImageRightCon;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *artContentHeightCon;

@property (weak, nonatomic) IBOutlet UILabel * shareIcon;

@property (weak, nonatomic) IBOutlet UILabel *descLbl;

@property (weak, nonatomic) IBOutlet UIImageView * mainImg;

@property (weak, nonatomic) IBOutlet UIWebView *descWebView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeightCon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *youtubeViewHeightCon;


@property(nonatomic, strong) IBOutlet YTPlayerView *playerView;

- (IBAction)menuClicked:(id)sender;

- (IBAction)backClicked:(id)sender;

- (IBAction)shareClicked:(id)sender;

@end

//
//  ArticleDetailsViewController.m
//  Mansour
//
//  Created by M R on 11/6/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "ArticleDetailsViewController.h"
#import "SWRevealViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@import Firebase;

#define iPhoneFontSize 14
#define iPadFontSize 17

@interface ArticleDetailsViewController (){
    
    NSString*filePathC;
    NSString *libraryDirectory;
    CGFloat fontSize;
}

@end

@implementation ArticleDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // increase Article views
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/ArticlesNews/%@/viewed",Domain,_item.Id];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         
         if (error == nil) {
             NSLog(@"Article views increased successfully");
         }
         
     }];
    
    if(self.revealViewController != nil){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        if ([DataClass getInstance].ar) {
            [_menuBu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rightViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        } else {
            [_menuBu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rearViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        }
        
    } else {
        
        NSLog(@"dfsdfsdfs");
    }
    
    self.playerView.delegate = self;
    NSDictionary *playerVars = @{
                                 @"playsinline" : @1,
                                 };
   // NSString * youtubeString = _item.youtubeStr;
    
    if (![_item.youtubeStr isEqualToString:@""]) {
        
        [self.playerView loadWithVideoId:[self getYoutubeIdFromLink:_item.youtubeStr] playerVars:playerVars];
        
    }else {
        
    }
    
    _shareIcon.text = [NSString stringWithFormat:@"%C",0xf064];
    
    NSString * htmlString;
    NSString * htmlPragraph;
    
    if([DataClass getInstance].ar)
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
        _articleTitleLbl.text = _item.titleAr;
        [_shareBu setTitle:@"مشاركة" forState:UIControlStateNormal];
        
         htmlPragraph = _item.htmlAr;
        
    } else {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
        _articleTitleLbl.text = _item.titleEn;
        [_shareBu setTitle:@"Share" forState:UIControlStateNormal];
    
        htmlPragraph = _item.htmlEn;
    
    }
    

    //_descLbl.text = _item.htmlAr;
    _descWebView.delegate = self;
    [[_descWebView scrollView] setScrollEnabled:NO];
    
    htmlString = [NSString stringWithFormat:@"<body style='text-align: right;direction: rtl;'>%@</body>",htmlPragraph];
    
    [_descWebView loadHTMLString:htmlString baseURL:nil];

    [self setupMainImage];
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (![_item.youtubeStr isEqualToString:@""]) {
        
    }else {
        _artContentHeightCon.constant -= _youtubeViewHeightCon.constant;
        _youtubeViewHeightCon.constant = 0;
    }
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    self.view.clipsToBounds = YES;

    [self animateBack];
    
    // log screen display
    [FIRAnalytics setScreenName:@"Article Details Screen" screenClass:@"ArticleDetailsViewController"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark : buttons actions

- (IBAction)menuClicked:(id)sender {
    
}

- (IBAction)backClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)shareClicked:(id)sender {
    NSString *newsURL;
    if ([DataClass getInstance].ar) {
        newsURL = _item.shareAr;
    } else {
        newsURL = _item.shareEn;
    }
    
    NSArray *objectsToShare = @[newsURL];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)inView:self.view permittedArrowDirections:0 animated:YES];
        
    } else {
        
        [self presentViewController:activityVC animated:YES completion:nil];
    }
}

#pragma mark: Private Methods

- (NSString *)getYoutubeIdFromLink:(NSString *)link {
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,[link length])];
    if (array.count > 0) {
        NSTextCheckingResult *result = array.firstObject;
       
        return [link substringWithRange:result.range];
    }
    
    return nil;
}

-(void)animateBack
{
    self.botBackImageRightCon.constant = 0;
    
    self.botBackImageRightCon.constant+=self.view.bounds.size.width;
    
    [UIView animateWithDuration:40.0 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        self.botBackImageRightCon.constant = 0;
        
        [UIView animateWithDuration:40.0 animations:^{
            
            [self.view layoutIfNeeded];
            
            
        } completion:^(BOOL finished) {
            
            [self animateBack ];
            
        }] ;
        
    }] ;
    
}

-(void) setupMainImage {
    
    NSString* urlWithoutExtension = [_item.mainImgStr substringToIndex:[_item.mainImgStr length]-4];
    NSString* imageExtension = [_item.mainImgStr substringFromIndex:[_item.mainImgStr length]-4];
    NSString* imgUrlText = [NSString stringWithFormat:@"%@_medium%@",urlWithoutExtension,imageExtension];
    NSString* imgUrlTextEscaped = [imgUrlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [_mainImg sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrlTextEscaped] placeholderImage:nil options:SDWebImageRefreshCached progress:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
    }];
}


#pragma mark : web view Delegate

-(void)webViewDidFinishLoad:(UIWebView *)webView {
   
    CGRect frame = _descWebView.frame;
    frame.size.height = 1;
    _descWebView.frame = frame;
    CGSize fittingSize = [_descWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    _descWebView.frame = frame;
    
    _artContentHeightCon.constant += fittingSize.height+_youtubeViewHeightCon.constant+200;
    _webViewHeightCon.constant = fittingSize.height+15;
    
    NSLog(@"size: %f, %f", fittingSize.width, fittingSize.height);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

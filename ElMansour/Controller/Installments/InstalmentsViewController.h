//
//  InstalmentsViewController.h
//  Mansour
//
//  Created by M R on 11/20/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstalmentsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *menuBu;
@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UILabel *headerlb;

@property (weak, nonatomic) IBOutlet UIImageView * bottomBackImageV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botBackImageRightCon;


- (IBAction)menuClicked:(id)sender;

- (IBAction)backClicked:(id)sender;

@end

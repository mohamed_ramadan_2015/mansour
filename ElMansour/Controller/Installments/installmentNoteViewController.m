//
//  installmentNoteViewController.m
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "installmentNoteViewController.h"
#import "RevealViewController.h"
#import "BankAccountTableViewCell.h"
#import "BankItem.h"

@import Firebase;

#define bankCell @"bankCell"

@interface installmentNoteViewController (){
    BOOL isArabic;
    
    NSMutableArray *banksData;
}

@end

@implementation installmentNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    self.view.clipsToBounds = YES;
    
    if ([DataClass getInstance].ar){
        isArabic = YES;
    } else {
        isArabic = NO;
    }
    
    if(self.revealViewController != nil){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        if ([DataClass getInstance].ar) {
            [_menuBu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rightViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        } else {
            [_menuBu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rearViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        }
    } else {
        
        NSLog(@"dfsdfsdfs");
    }
    
    
    if([DataClass getInstance].ar)
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
        _headerlb.text = @"ملاحظة";
        _dateStrLbl.text = @"تاريخ القسط";
        _amountStrLbl.text = @"قيمة القسط";
    
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
        
        _headerlb.text = @"Note";
        _dateStrLbl.text = @"Due Date";
        _amountStrLbl.text = @"Ammount";
    }
    
    _titleLbl.text = _instalmentItem.note;
    _amountLbl.text = _instalmentItem.amount;
    _dateLbl.text = _instalmentItem.date;
    
    
    UINib * nib = [UINib nibWithNibName:@"BankAccountTableViewCell" bundle:[NSBundle mainBundle]];
    [_bankTableView registerNib:nib forCellReuseIdentifier:bankCell];
    
    _bankTableView.delegate = self;
    _bankTableView.dataSource = self;
    _bankTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _bankTableView.estimatedRowHeight = UITableViewAutomaticDimension;
    _bankTableView.allowsSelection = NO;
    _bankTableView.showsVerticalScrollIndicator = NO;
    
    banksData = [DataClass getInstance].banksArr;
    
}

-(void)viewWillAppear:(BOOL)animated{
    _contentView.layer.cornerRadius = 10;
    _contentView.clipsToBounds = YES;
}

-(void)viewDidAppear:(BOOL)animated {
    // log screen display
    [FIRAnalytics setScreenName:@"Instalment Note screen" screenClass:@"installmentNoteViewController"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark : buttons actions

- (IBAction)menuClicked:(id)sender {
    
}

- (IBAction)backClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark : Table View Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [banksData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView isEqual:_bankTableView]) {
        BankAccountTableViewCell *cell = [_bankTableView dequeueReusableCellWithIdentifier:bankCell];
        
        BankItem *bankItem = [banksData objectAtIndex:indexPath.row];
        
        [cell initWithBankItem:bankItem];
        
        return cell;
    }
    
    return nil;
}

#pragma mark : Table View Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    BankItem *item = [banksData objectAtIndex:indexPath.row];
    if ([item.softCode isEqualToString:@""] && ([item.egyAccountNum isEqualToString:@""] || [item.dolAccountNum isEqualToString:@""])) {
        return 220;
    } else if ([item.softCode isEqualToString:@""]){
        return 250;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return 300;
    } else {
        return 300;
    }
    
    return UITableViewAutomaticDimension;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

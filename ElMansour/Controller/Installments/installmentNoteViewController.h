//
//  installmentNoteViewController.h
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstallmentItem.h"

@interface installmentNoteViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) InstallmentItem* instalmentItem;

@property (weak, nonatomic) IBOutlet UIButton *menuBu;
@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UILabel *headerlb;

@property (weak, nonatomic) IBOutlet UIImageView * bottomBackImageV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botBackImageRightCon;

@property (weak, nonatomic) IBOutlet UITableView *bankTableView;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *amountLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@property (weak, nonatomic) IBOutlet UILabel *amountStrLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateStrLbl;

@property (weak, nonatomic) IBOutlet UIView *contentView;

- (IBAction)menuClicked:(id)sender;

- (IBAction)backClicked:(id)sender;


@end

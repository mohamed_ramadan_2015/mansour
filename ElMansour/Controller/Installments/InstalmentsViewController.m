//
//  InstalmentsViewController.m
//  Mansour
//
//  Created by M R on 11/20/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "InstalmentsViewController.h"
#import "RevealViewController.h"
#import "instalmentsTableViewCell.h"
#import "LoginViewController.h"
#import "InstallmentItem.h"
#import "installmentNoteViewController.h"

@import Firebase;

#define _RGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define instCell @"instalmentsCell"

@interface InstalmentsViewController () {
    UIView* noInstalmentsView;
    UIView* notUserView;
    UIView* loginView;
    UIButton * loginBu;
    UIView* instView;
    UITableView* instalmentsTableView;
    NSMutableArray* instalmentsData;
    BOOL isArabic;
}

@end

@implementation InstalmentsViewController

// Hello Bro,
// this view controller interface is built by code.
// so any change in design will be done here :)
// Do not worry, its fun

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.clipsToBounds = YES;
    
    // cancel old scheduled notifications
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    if ([DataClass getInstance].ar){
        isArabic = YES;
    } else {
        isArabic = NO;
    }
    
    if(self.revealViewController != nil){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        if ([DataClass getInstance].ar) {
            [_menuBu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rightViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        } else {
            [_menuBu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rearViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        }
        
    } else {
        
        NSLog(@"dfsdfsdfs");
    }
    
    
    if([DataClass getInstance].ar)
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
        _headerlb.text = @"الاقساط";
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
        
        _headerlb.text = @"Instalments";
    }
    
    
    [self createSubViewViews];
    [self setupInstView];
    
    instalmentsData = [[NSMutableArray alloc]init];
    
    // handle user states
    // if user not logedin, show not loggedin user view
    if (![[NSUserDefaults standardUserDefaults] boolForKey:isLoggedIn]) {
        [loginView setHidden:NO];
        [loginBu setHidden:NO];
    } else {
        // check if guest or verified user
        if ([[NSUserDefaults standardUserDefaults] boolForKey:isGuestUser]) {
            // guest user
            [notUserView setHidden:NO];
        } else {
            //verfied user
            // check if there is network or not
            if ([DataClass isNetworkAvailable]) {
                instalmentsData = [[NSMutableArray alloc] init];
                if ([[NSUserDefaults standardUserDefaults] objectForKey:userId]) {
                    [self getInstallmentsForUserId:[[NSUserDefaults standardUserDefaults] valueForKey:userId]];
                } else {
                    // no user id found
                }
            } else {
                NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:Kinstallments];
                NSArray *n2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
                instalmentsData =  [[NSMutableArray alloc] initWithArray:n2];
                NSLog(@"plaplapla %@", instalmentsData);
                
                if ([instalmentsData count] == 0) {
                    [noInstalmentsView setHidden:NO];
                } else {
                    [instView setHidden:NO];
                }
            }
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self animateBack];
    
    // log screen display
    [FIRAnalytics setScreenName:@"Instalment list screen" screenClass:@"InstalmentsViewController"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark : buttons actions

-(void) loginClicked:(UIButton*)sender {
    LoginViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginView"];
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (IBAction)menuClicked:(id)sender {
    
}

- (IBAction)backClicked:(id)sender {
    
    UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark : Private Methods

-(void) getInstallmentsForUserId : (NSString*)userId {
    
    [AppDelegate addHoverView:self.view];
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/client/%@/installments",Domain,userId];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:10.0f];
    [urlRequest setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if (response == nil) {
             // timeout of failed
             [self getInstallmentsForUserId:userId];
             
         } else {
             // continue
         }
         
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             NSLog(@"getInstallments respo : %@",responseDict);
             
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 
                 
                 
             });
             
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"status_code"] integerValue]==200)
                 {
                     NSLog(@"Cancel request success");
                     
                     
                     
                     NSArray*all = responseDict[@"data"];
                     
                     for (int i = 0; i<all.count; i++)
                     {
                         
                         NSDictionary*dic = all[i];
                         
                         NSString*amount = dic[@"amount"];
                         
                         NSString*date = dic[@"due_date"];
                         
                         NSString*note = dic[@"notes"];
                         
                         InstallmentItem*n1 = [[InstallmentItem alloc ] initWithAmount:amount date:date note:note];
                         
                         [instalmentsData addObject:n1];
                     
                     }
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         [AppDelegate removeHoverView:self.view];
                         
                         if ([instalmentsData count] == 0) {
                             [noInstalmentsView setHidden:NO];
                         } else {
                             [instView setHidden:NO];
                             [instalmentsTableView reloadData];
                             
                             [self registerInstalmentsNotofications];
                             
                         }
                     });
                     
                     
                     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:instalmentsData];
                     
                     NSUserDefaults*defaults =  [NSUserDefaults standardUserDefaults];
                     
                     [defaults setObject:data forKey:Kinstallments];
                     
                     [defaults synchronize];
                     
                 } else {
                     
                     NSLog(@"getInstallments  fail");
                 
                 }
             } else {
                 
                 NSLog(@"getInstallments Dic ");
             }
         } else {
             
             NSLog(@"getInstallments error: %@",error);
         }
         
     }];
    
    
}

-(void) registerInstalmentsNotofications {
    // register local notifications for all installments which due date more than 5 days.
    int instalmentCount = [instalmentsData count];
    if (instalmentCount != 0) {
        
        for (int i=0; i< instalmentCount; ++i) {
            
            InstallmentItem* item = [instalmentsData objectAtIndex:i];
            
            // Convert string to date object
            NSString* dateStr = item.date;
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd/MM/yyyy hh:mm a"];
            NSDate *date = [dateFormat dateFromString:dateStr];
            
            NSLog(@"%@",date);
            
            [self schedualeThisNotification:date item:item];
            
        }
        
    }
}

-(void) schedualeThisNotification:(NSDate*)date item:(InstallmentItem*)item {
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *component = [calender components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:date];
    
    [component setDay:component.day];
    [component setMonth:component.month];
    [component setYear:component.year];
    
    // due date of installment
    NSDate *exactDate = [calender dateFromComponents:component];
    NSLog(@"%@",exactDate);
    
    // fire date of installment (befor 5 days of installemnts due date)
    NSDate *fireDate = [NSDate dateWithTimeInterval:-5*24*60*60 sinceDate:exactDate];
    NSLog(@"%@",fireDate);
    
    // date to compare with fireDate
    NSDate *currentDate = [NSDate dateWithTimeIntervalSinceNow:0];
    NSDate *comparedDate = [NSDate dateWithTimeInterval:-5*24*60*60 sinceDate:currentDate];
    NSLog(@"%@",comparedDate);
    
    // schedule notification if fireDate comes before currentDate - 5Days
    if ([fireDate compare:comparedDate] == NSOrderedAscending) {
        UILocalNotification *notification1 = [[UILocalNotification alloc] init];
        notification1.fireDate = [calender dateFromComponents:component];;
        notification1.timeZone = [NSTimeZone localTimeZone];
        if([notification1 respondsToSelector:@selector(alertTitle)])
        {
           /* if (@available(iOS 8.2, *))
            {
                notification1.alertTitle = [NSString stringWithFormat:@"Installment Reminder"];
            } else {
                // Fallback on earlier versions
            }*/
        }else {
            NSLog(@"Not respondddd1200");
        }
        notification1.alertBody = item.note;
        notification1.soundName=UILocalNotificationDefaultSoundName;
        notification1.applicationIconBadgeNumber = 1;
        [[UIApplication sharedApplication] scheduleLocalNotification:notification1];
    }
}

-(void)animateBack
{
    self.botBackImageRightCon.constant = 0;
    
    self.botBackImageRightCon.constant+=self.view.bounds.size.width;
    
    [UIView animateWithDuration:40.0 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        self.botBackImageRightCon.constant = 0;
        
        [UIView animateWithDuration:40.0 animations:^{
            
            [self.view layoutIfNeeded];
            
            
        } completion:^(BOOL finished) {
            
            [self animateBack ];
            
        }] ;
        
    }] ;
    
}

-(void) createSubViewViews {
    
    // create NoInstalmentsView
    noInstalmentsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width*0.9, 150)];
    [noInstalmentsView setBackgroundColor:[UIColor whiteColor]];
    [noInstalmentsView setAlpha:0.7];
    noInstalmentsView.layer.cornerRadius = 15;
    noInstalmentsView.center = self.view.center;
    [self.view addSubview:noInstalmentsView];
    
    // create NOInstalmentLabel
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, noInstalmentsView.frame.size.width*0.8, noInstalmentsView.frame.size.height*0.8)];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [lbl setFont:[UIFont fontWithName:@"CoconNextArabic-Regular" size:17.0]];
    } else {
        [lbl setFont:[UIFont fontWithName:@"CoconNextArabic-Regular" size:20.0]];
    }
    
    lbl.numberOfLines = 0;
    lbl.text = [DataClass getInstance].ar ? @"لا يوجد اقساط متأخرة" : @"No instalments founded" ;
    lbl.textColor = [UIColor blackColor];
    [lbl setCenter:CGPointMake(noInstalmentsView.frame.size.width / 2, noInstalmentsView.frame.size.height / 2)];
    lbl.textAlignment = NSTextAlignmentCenter;
    [noInstalmentsView addSubview:lbl];
    [noInstalmentsView setHidden:YES];
    
    // create loginView
    loginView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width*0.9, 150)];
    [loginView setBackgroundColor:[UIColor whiteColor]];
    [loginView setAlpha:0.7];
    loginView.layer.cornerRadius = 15;
    loginView.center = self.view.center;
    [self.view addSubview:loginView];

    // create loginLabel
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, loginView.frame.size.width*0.8, loginView.frame.size.height*0.8)];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [lbl setFont:[UIFont fontWithName:@"CoconNextArabic-Regular" size:17.0]];
    } else {
        [lbl setFont:[UIFont fontWithName:@"CoconNextArabic-Regular" size:20.0]];
    }
    
    lbl1.numberOfLines = 0;
    lbl1.text = [DataClass getInstance].ar ? @"انت غير مسجل لدينا , قم بالتسجيل" : @"you are not in our system, please login" ;
    lbl1.textColor = [UIColor blackColor];
    [lbl1 setCenter:CGPointMake(loginView.frame.size.width / 2, loginView.frame.size.height / 2)];
    lbl1.textAlignment = NSTextAlignmentCenter;
    [loginView addSubview:lbl1];
    [loginView setHidden:YES];
    
    // create Loggin button
    loginBu = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 231)];
    loginBu.backgroundColor = _RGB(25, 105, 98, 1.0);
    loginBu.translatesAutoresizingMaskIntoConstraints = NO;
    loginBu.userInteractionEnabled = YES;
    loginBu.layer.cornerRadius = 10;
    [loginBu addTarget:self action:@selector(loginClicked:) forControlEvents:UIControlEventTouchUpInside];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        loginBu.titleLabel.font = [UIFont fontWithName:@"CoconNextArabic-Regular" size:17.0];
    } else {
        loginBu.titleLabel.font = [UIFont fontWithName:@"CoconNextArabic-Regular" size:20.0];
    }
    
    if (isArabic) {
        [loginBu setTitle:[NSString stringWithFormat:@"تسجيل الدخول"] forState:UIControlStateNormal];
    } else {
        [loginBu setTitle:[NSString stringWithFormat:@"Login"] forState:UIControlStateNormal];
    }
    
    [loginBu setCenter:CGPointMake(loginView.frame.size.width / 2, loginView.frame.size.height / 2)];
    [self.view addSubview:loginBu];
    
    NSLayoutConstraint*con1=[NSLayoutConstraint constraintWithItem:loginBu attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:0.9 constant:0.0];
    [self.view addConstraint:con1];
    
    NSLayoutConstraint*con=[NSLayoutConstraint constraintWithItem:loginBu attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:0.1 constant:0.0];
    [self.view addConstraint:con];
    
    NSLayoutConstraint*con3=[NSLayoutConstraint constraintWithItem:loginBu attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-20.0];
    [self.view addConstraint:con3];
    
    NSLayoutConstraint*con4=[NSLayoutConstraint constraintWithItem:loginBu attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    [self.view addConstraint:con4];
    
    [loginBu setHidden:YES];
    
    

    // create notMansoutUser
    notUserView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width*0.9, 150)];
    [notUserView setBackgroundColor:[UIColor whiteColor]];
    [notUserView setAlpha:0.7];
    notUserView.layer.cornerRadius = 15;
    notUserView.center = self.view.center;
    [self.view addSubview:notUserView];
    
    // create loginLabel
    UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, notUserView.frame.size.width*0.8, notUserView.frame.size.height*0.8)];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [lbl2 setFont:[UIFont fontWithName:@"CoconNextArabic-Regular" size:17.0]];
    } else {
        [lbl2 setFont:[UIFont fontWithName:@"CoconNextArabic-Regular" size:20.0]];
    }
    
    lbl2.numberOfLines = 0;
    lbl2.text = [DataClass getInstance].ar ? @"لا يوجد اقساط لانك لست عميل عند المنصور" : @"No instalments because you are not Mansour user" ;
    lbl2.textColor = [UIColor blackColor];
    [lbl2 setCenter:CGPointMake(notUserView.frame.size.width / 2, notUserView.frame.size.height / 2)];
    lbl2.textAlignment = NSTextAlignmentCenter;
    [notUserView addSubview:lbl2];
    [notUserView setHidden:YES];
}

-(void) setupInstView {
    // create instalments content view
    
    instView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width*0.9, self.view.frame.size.height*0.7)];
    [instView setBackgroundColor:[UIColor whiteColor]];
    instView.layer.cornerRadius = 10;
    instView.center = self.view.center;
    instView.clipsToBounds = YES;
    [self.view addSubview:instView];
    
    instView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint*con1=[NSLayoutConstraint constraintWithItem:instView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:0.9 constant:0.0];
    [self.view addConstraint:con1];
    
    NSLayoutConstraint*con=[NSLayoutConstraint constraintWithItem:instView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_menuBu attribute:NSLayoutAttributeBottom multiplier:1.0 constant:30.0];
    [self.view addConstraint:con];
    
    NSLayoutConstraint*con3=[NSLayoutConstraint constraintWithItem:instView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-20.0];
    [self.view addConstraint:con3];
    
    NSLayoutConstraint*con4=[NSLayoutConstraint constraintWithItem:instView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    [self.view addConstraint:con4];
    
    
    // create table header view
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, instView.frame.size.width, instView.frame.size.height*0.1)];
    headView.backgroundColor = _RGB(25, 105, 98, 1.0);
    [instView addSubview:headView];
    
    
    // create header titles
    UILabel *costLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 23, 90)];
    [headView addSubview:costLbl];
    costLbl.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint*costLblCenterYCon=[NSLayoutConstraint constraintWithItem:costLbl attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [headView addConstraint:costLblCenterYCon];
    
    NSLayoutConstraint*costLblTopCon=[NSLayoutConstraint constraintWithItem:costLbl attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    [headView addConstraint:costLblTopCon];
    
    NSLayoutConstraint*costLblBottomCon=[NSLayoutConstraint constraintWithItem:costLbl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [headView addConstraint:costLblBottomCon];
    
    NSLayoutConstraint*costLblLeadCon=[NSLayoutConstraint constraintWithItem:costLbl attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    [headView addConstraint:costLblLeadCon];
    
    NSLayoutConstraint*costLblWidthCon=[NSLayoutConstraint constraintWithItem:costLbl attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeWidth multiplier:0.3 constant:0.0];
    [headView addConstraint:costLblWidthCon];
    
    costLbl.text = isArabic ? @"المبلغ" : @"Money";
    costLbl.textColor = [UIColor whiteColor];
    costLbl.textAlignment = NSTextAlignmentCenter;
    
    
    UILabel *dateLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 23, 90)];
    [headView addSubview:dateLbl];
    dateLbl.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint*dateLblCenterYCon=[NSLayoutConstraint constraintWithItem:dateLbl attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [headView addConstraint:dateLblCenterYCon];
    
    NSLayoutConstraint*dateLblTopCon=[NSLayoutConstraint constraintWithItem:dateLbl attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    [headView addConstraint:dateLblTopCon];
    
    NSLayoutConstraint*dateLblBottomCon=[NSLayoutConstraint constraintWithItem:dateLbl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [headView addConstraint:dateLblBottomCon];
    
    NSLayoutConstraint*dateLblLeadCon=[NSLayoutConstraint constraintWithItem:dateLbl attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:costLbl attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    [headView addConstraint:dateLblLeadCon];
    
    NSLayoutConstraint*dateLblWidthCon=[NSLayoutConstraint constraintWithItem:dateLbl attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeWidth multiplier:0.3 constant:0.0];
    [headView addConstraint:dateLblWidthCon];
    
    dateLbl.text = isArabic ? @"التاريخ" : @"Date";
    dateLbl.textColor = [UIColor whiteColor];
    dateLbl.textAlignment = NSTextAlignmentCenter;
    
    
    UILabel *noteLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 23, 90)];
    [headView addSubview:noteLbl];
    noteLbl.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint*noteLblCenterYCon=[NSLayoutConstraint constraintWithItem:noteLbl attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [headView addConstraint:noteLblCenterYCon];
    
    NSLayoutConstraint*noteLblTopCon=[NSLayoutConstraint constraintWithItem:noteLbl attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    [headView addConstraint:noteLblTopCon];
    
    NSLayoutConstraint*noteLblBottomCon=[NSLayoutConstraint constraintWithItem:noteLbl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [headView addConstraint:noteLblBottomCon];
    
    NSLayoutConstraint*noteLblLeadCon=[NSLayoutConstraint constraintWithItem:noteLbl attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:dateLbl attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    [headView addConstraint:noteLblLeadCon];
    
    NSLayoutConstraint*noteLblWidthCon=[NSLayoutConstraint constraintWithItem:noteLbl attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeWidth multiplier:0.4 constant:0.0];
    [headView addConstraint:noteLblWidthCon];
    
    noteLbl.text = isArabic ? @"ملاحظة" : @"Note";
    noteLbl.textColor = [UIColor whiteColor];
    noteLbl.textAlignment = NSTextAlignmentCenter;
    
    
    
    // create table header view
    instalmentsTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 34, 45)];
    instalmentsTableView.backgroundColor = _RGB(25, 105, 98, 0.7);
    
    [instView addSubview:instalmentsTableView];
    
    instalmentsTableView.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    NSLayoutConstraint*con5=[NSLayoutConstraint constraintWithItem:instalmentsTableView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:instView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0];
    [instView addConstraint:con5];
    
    
    NSLayoutConstraint*con6=[NSLayoutConstraint constraintWithItem:instalmentsTableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:headView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [instView addConstraint:con6];
    
    
    NSLayoutConstraint*con7=[NSLayoutConstraint constraintWithItem:instalmentsTableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:instView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [instView addConstraint:con7];
    
    NSLayoutConstraint*con8=[NSLayoutConstraint constraintWithItem:instalmentsTableView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:instView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    [instView addConstraint:con8];
    
    instalmentsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UINib *nib = [UINib nibWithNibName:@"instalmentsTableViewCell" bundle:[NSBundle mainBundle]];
    [instalmentsTableView registerNib:nib forCellReuseIdentifier:instCell];
    
    instalmentsTableView.delegate = self;
    instalmentsTableView.dataSource = self;
    instalmentsTableView.estimatedRowHeight = 60;
    
    
    [instView setHidden:YES];
}


-(void) displayNoInstalmentsView {
    
}

-(void) displayNotMansourUserView {
    
}

-(void) displayNotLogedinView {
    
}


#pragma mark : Table View Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [instalmentsData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView isEqual:instalmentsTableView]) {
        
        instalmentsTableViewCell *cell = [instalmentsTableView dequeueReusableCellWithIdentifier:instCell];
        
        InstallmentItem *item = [instalmentsData objectAtIndex:indexPath.row];
        
        cell.amountLbl.text = item.amount;
        cell.dateLbl.text = item.date;
        
        if (item.note != nil) {
            NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
            cell.noteLbl.attributedText = [[NSAttributedString alloc] initWithString:isArabic ? @"ملاحظة" : @"Note"
                                                                     attributes:underlineAttribute];
           // cell.noteLbl.text = isArabic ? @"ملاحظة" : @"Note";
        } else {
            cell.noteLbl.text = @"-";
        }
        
        return cell;
    }
    
    return nil;
}

#pragma mark : Table View Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return 60;
    } else {
        return 80;
    }
    
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:instalmentsTableView]) {
        installmentNoteViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"InstalmentNoteView"];
        vc.instalmentItem = [instalmentsData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  InstalmentRiminderViewController.m
//  ElMansour
//
//  Created by M R on 11/27/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "InstalmentRiminderViewController.h"
#import "RevealViewController.h"
#import "BankAccountTableViewCell.h"
#import "BankItem.h"
@import Firebase;
#define bankCell @"bankCell"
#define _RGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

@interface InstalmentRiminderViewController (){
    BOOL isArabic;
    NSMutableArray *banksData;
}

@end

@implementation InstalmentRiminderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.clipsToBounds = YES;
    
    if ([DataClass getInstance].ar){
        isArabic = YES;
    } else {
        isArabic = NO;
    }
    
    if(self.revealViewController != nil){
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        if ([DataClass getInstance].ar) {
            [_menuBu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rightViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        } else {
            [_menuBu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rearViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        }
        
    } else {
        
        NSLog(@"dfsdfsdfs");
    }
    
    
    NSDictionary*greenColorString = @{NSForegroundColorAttributeName: _RGB(25, 105, 98, 1.0)};
     NSDictionary*blackColorString = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    
    if([DataClass getInstance].ar)
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
        _headerlb.text = @"تذكير";
       
        NSAttributedString *spaceStr = [[NSAttributedString alloc]initWithString:@" " attributes:blackColorString];
        NSAttributedString *friStr = [[NSAttributedString alloc]initWithString:@"نذكرك بأن هناك تاريخ استحقاق بعد" attributes:blackColorString];
        NSAttributedString *_5daysStr = [[NSAttributedString alloc]initWithString:@"٥ يوم " attributes:greenColorString];
        NSAttributedString *daysStr = [[NSAttributedString alloc]initWithString:@"يوم " attributes:blackColorString];
        NSAttributedString *dateStr = [[NSAttributedString alloc]initWithString:@" ٢٥/١١/٢٠١٧" attributes:greenColorString];
        NSAttributedString *amountStr = [[NSAttributedString alloc]initWithString:@" قيمته" attributes:blackColorString];
        NSAttributedString *costStr = [[NSAttributedString alloc]initWithString:@"٢٥٠٠٠ جنية" attributes:greenColorString];
        
        NSMutableAttributedString *allStr = [[NSMutableAttributedString alloc]init];
        [allStr appendAttributedString:friStr];
        [allStr appendAttributedString:spaceStr];
        [allStr appendAttributedString:_5daysStr];
        [allStr appendAttributedString:daysStr];
        [allStr appendAttributedString:dateStr];
        [allStr appendAttributedString:amountStr];
        [allStr appendAttributedString:costStr];
        
        _titleLbl.attributedText = allStr;
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
        
        _headerlb.text = @"Riminder";
    }
    
    
    
    UINib * nib = [UINib nibWithNibName:@"BankAccountTableViewCell" bundle:[NSBundle mainBundle]];
    [_bankTableView registerNib:nib forCellReuseIdentifier:bankCell];
    
    _bankTableView.delegate = self;
    _bankTableView.dataSource = self;
    _bankTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _bankTableView.estimatedRowHeight = UITableViewAutomaticDimension;
    _bankTableView.allowsSelection = NO;
    _bankTableView.showsVerticalScrollIndicator = NO;
    
    banksData = [DataClass getInstance].banksArr;
   
}

-(void)viewWillAppear:(BOOL)animated{
    _contentView.layer.cornerRadius = 10;
    _contentView.clipsToBounds = YES;
}

-(void)viewDidAppear:(BOOL)animated {
    // log screen display
    [FIRAnalytics setScreenName:@"Instalment Riminder Screen" screenClass:@"InstalmentRiminderViewController"];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark : Table View Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [banksData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView isEqual:_bankTableView]) {
        BankAccountTableViewCell *cell = [_bankTableView dequeueReusableCellWithIdentifier:bankCell];
        
        BankItem *bankItem = [banksData objectAtIndex:indexPath.row];
        
        [cell initWithBankItem:bankItem];
        
        return cell;
    }
    
    return nil;
}

#pragma mark : Table View Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    BankItem *item = [banksData objectAtIndex:indexPath.row];
    if ([item.softCode isEqualToString:@""] && ([item.egyAccountNum isEqualToString:@""] || [item.dolAccountNum isEqualToString:@""])) {
        return 220;
    } else if ([item.softCode isEqualToString:@""]){
        return 250;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return 300;
    } else {
        return 300;
    }
    
    return UITableViewAutomaticDimension;
}

#pragma mark : buttons actions

- (IBAction)menuClicked:(id)sender {
    
}

- (IBAction)backClicked:(id)sender {
    
    UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self.navigationController pushViewController:vc animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

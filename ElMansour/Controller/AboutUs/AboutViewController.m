//
//  AboutViewController.m
//  Mansour
//
//  Created by M R on 11/9/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "AboutViewController.h"
@import Firebase;
#define _RGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]


@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if([DataClass getInstance].ar) {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
    }
    else {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
    }
    
    [_youtubeBtn setTitle:[NSString stringWithFormat:@"%C",0xf16a] forState:UIControlStateNormal];
    
    _comDescLbl.text = NSLocalizedString(@"aboutUsUpText", nil);
    _dowImgLbl.text = NSLocalizedString(@"aboutUsWord", nil);
    _managerTitleLbl.text = NSLocalizedString(@"ManagingDirector", nil);
    _managerNameLbl.text = NSLocalizedString(@"ManagingDirectorName", nil);
    _adressLbl.text = NSLocalizedString(@"aboutUsAddress", nil);
    _aboutComLbl.text = NSLocalizedString(@"aboutCompany", nil);
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tap.numberOfTapsRequired = 1;
    _adressImg.userInteractionEnabled = YES;
    [_adressImg addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tap1.numberOfTapsRequired = 1;
    _phoneLbl.userInteractionEnabled = YES;
    [_phoneLbl addGestureRecognizer:tap1];
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    _contentView.layer.cornerRadius = 10;
    _contentView.clipsToBounds = YES;
    
    _upScrollView.layer.cornerRadius = 10;
    _upScrollView.clipsToBounds = YES;
    
    _adressImg.layer.cornerRadius = 10;
    _adressImg.clipsToBounds = YES;
    
    _aymanImageV.layer.cornerRadius = _aymanImageV.frame.size.height/2;
    _aymanImageV.layer.borderColor = _RGB(25, 105, 98, 1.0).CGColor;
    _aymanImageV.layer.borderWidth = 2;
    _aymanImageV.clipsToBounds = YES;
    
    // log screen display
    [FIRAnalytics setScreenName:@"About US Screen" screenClass:@"AboutViewController"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark : Button Actions

- (IBAction)backClicked:(id)sender {
    
    UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)twitterClicked:(id)sender {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://twitter.com/memarelmansour"]];
    [[UIApplication sharedApplication] openURL:url];
    
}

- (IBAction)facebookClicked:(id)sender {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.facebook.com/MeamarElmansour/"]];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)instagramClicked:(id)sender {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.instagram.com/m.elmansour.gallery/"]];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)youtubeClicked:(id)sender {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/channel/UCxlnwuBaZSHxbhlYtbyV5XQ"]];
    [[UIApplication sharedApplication] openURL:url];
}
    
#pragma mark : private methods

-(void) handleTap: (UIGestureRecognizer*) sender {
    
    if ([sender.view isEqual:_adressImg]) {
        [self showLocOnMap];
    } else if ([sender.view isEqual:_phoneLbl]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:+19441"]];
    }
}

-(void) showLocOnMap {
    NSString *longtude = @"31.3748022";
    NSString *latitude = @"29.9915585";
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%@,%@",latitude,longtude]];
    [[UIApplication sharedApplication] openURL:url];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

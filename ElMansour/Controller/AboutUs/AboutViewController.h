//
//  AboutViewController.h
//  Mansour
//
//  Created by M R on 11/9/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *upScrollView;

@property (weak, nonatomic) IBOutlet UILabel *comDescLbl;

@property (weak, nonatomic) IBOutlet UILabel *dowImgLbl;

@property (weak, nonatomic) IBOutlet UILabel *adressLbl;

@property (weak, nonatomic) IBOutlet UIImageView *adressImg;

@property (weak, nonatomic) IBOutlet UILabel *aboutComLbl;

@property (weak, nonatomic) IBOutlet UILabel * phoneLbl;

@property (weak, nonatomic) IBOutlet UIImageView *aymanImageV;

@property (weak, nonatomic) IBOutlet UILabel * managerTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel * managerNameLbl;

@property (weak, nonatomic) IBOutlet UIButton *youtubeBtn;
    
@end

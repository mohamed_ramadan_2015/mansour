//
//  mainViewController.m
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "maiViewController.h"
#import "maiTableViewCell.h"
#import "mainCollectionViewCell.h"
#import "SWRevealViewController.h"
#import "proDetViewController.h"
#import "NewsDetailsViewController.h"
#import "NewsViewController.h"
#import "UserItem.h"
@import Firebase;
#import <SDWebImage/UIImageView+WebCache.h>

#import "NSObject+BVJSONString.h"

#import "projectItem.h"

static NSString *CellIdentifier1 = @"ContentCell";
static NSString *kCollectionViewCellIdentifier2 = @"kCollectionViewCellIdentifier2";
@interface maiViewController ()
{
    CGFloat currentIndex;
    
    NSMutableArray* allPages ;
    
    BOOL animationRunning ;
    
    NSString*filePathC;
    
    BOOL drawOnce;
    
    NSString*fileProject;
    
    int sliderProjectsCount;
    
    NSUserDefaults*defaults;
}

@end

@implementation maiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    // get broadcast notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"shareApp"
                                               object:nil];

    
    defaults = [NSUserDefaults standardUserDefaults];

    
    _bottomView.clipsToBounds = YES;
    
    
    
    if(self.revealViewController != nil){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        if ([DataClass getInstance].ar) {
            [_menuBu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rightViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        } else {
            [_menuBu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            self.revealViewController.rearViewController = [sb instantiateViewControllerWithIdentifier:@"sideMenu"];
        }  
    } else {
        
        NSLog(@"dfsdfsdfs");
    }
    
    //    self.revealViewController.rightViewRevealOverdraw = 0.0f;
    
   
    allPages = [NSMutableArray new];
    
    [self.moreBu setTitle:NSLocalizedString(@"mainMore", " ") forState:UIControlStateNormal];
    
    self.newslb.text = NSLocalizedString(@"mainNews", " ");
    
    
    NSLog(@"dscascasdcasdcasdcasd %@",[DataClass getInstance].newsArr);
    
    NSLog(@"dscascasdcasdcasdcasd 1255 %@",[DataClass getInstance].projectsSliderArr);
    
    sliderProjectsCount = (int)[[DataClass getInstance].projectsSliderArr count];
    
    _colPageControl.transform = CGAffineTransformMakeScale(1.0, 1.0);
    
    drawOnce = YES;
    
    self.pageView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    
    self.pageView.opaque = YES;
    
    animationRunning = NO;
    
    self.pageDot1.layer.cornerRadius = 1.5;
    
    UINib *nib1 = [UINib nibWithNibName:@"maiTableViewCell" bundle:nil];
    
    [self.areaSettTable registerNib:nib1 forCellReuseIdentifier:CellIdentifier1];
    
    [self.areaSettTable setBounces:NO];
    
    self.areaSettTable.delegate=self;
    
    self.areaSettTable.dataSource=self;
    
    self.areaSettTable.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    
    self.areaSettTable.opaque = NO;
    
    self.areaSettTable.separatorColor = [UIColor clearColor];
    
    self.areaSettTable.showsVerticalScrollIndicator = NO;
    
    self.areaSettTable.estimatedRowHeight = 250;
    
    self.areaSettTable.rowHeight = UITableViewAutomaticDimension;
    
    
    [allPages insertObject:self.pageDot1 atIndex:0];
    
    
    UINib *nib2 = [UINib nibWithNibName:
                   NSStringFromClass([mainCollectionViewCell class])
                                 bundle:[NSBundle mainBundle]];
    
    [_dyCollection registerNib:nib2 forCellWithReuseIdentifier:kCollectionViewCellIdentifier2];
    
    
    
    _dyCollection.dataSource = self;
    _dyCollection.delegate = self;
    _dyCollection.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    _dyCollection.opaque = NO;
    _dyCollection.showsHorizontalScrollIndicator=NO;
    _dyCollection.pagingEnabled = YES;
    
    
    NSError*error=nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/Application Support"];
    
    filePathC = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"News"]];
    
    
    fileProject = [libraryDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"ProjectSlider"]];
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:filePathC])
    {
        if(
           
           [[NSFileManager defaultManager] createDirectoryAtPath:filePathC
                                     withIntermediateDirectories:YES
                                                      attributes:nil
                                                           error:&error])
        {
            NSLog(@"created");
        }
        else
        {
            NSLog(@"not created");
        }
    }
    else
    {
        NSLog(@"exists");
    }
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:fileProject])
    {
        if(
           
           [[NSFileManager defaultManager] createDirectoryAtPath:fileProject
                                     withIntermediateDirectories:YES
                                                      attributes:nil
                                                           error:&error])
        {
            NSLog(@"created");
        }
        else
        {
            NSLog(@"not created");
        }
    }
    else
    {
        NSLog(@"existed");
    }
    
    /*
     UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAll:)];
     [swipeLeft setDirection: UISwipeGestureRecognizerDirectionLeft ];
     [self.dyCollection addGestureRecognizer:swipeLeft];
     swipeLeft=nil;
     
     UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAll:)];
     [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
     [self.dyCollection addGestureRecognizer:swipeRight];
     */
    
    
    
    if ([DataClass getInstance].ar) {
        
        _logoImageV.image = [UIImage imageNamed:@"logoar.png"];
    } else {
        self.topView.tag = 1 ;
        
        _logoImageV.image = [UIImage imageNamed:@"logoen.png"];
    }
    
}

-(void)viewDidLayoutSubviews
{
    if(drawOnce)
    {
        [self drawDots];
        
        self.notificationsNumlb.layer.cornerRadius = self.notificationsNumlb.bounds.size.width/2.0;
        
        self.notificationsNumlb.clipsToBounds = true;
        
        if( [DataClassH shared].unReadCount == 0)
        {
            self.notificationsNumlb.hidden = true;
        }
        
        drawOnce = NO;
    }
    
}

-(void)drawDots
{
    if ([DataClass getInstance].ar) {
        [self.pageView removeConstraint:self.pageDot1TraCon];
    } else {
        [self.pageView removeConstraint:self.pageDot1LeadCon];
    }
    
    
    NSInteger num =  [[DataClass getInstance].projectsSliderArr  count] -1;
    
    UIView*lastView = self.pageDot1;
    
    for (int i = 0; i<num; i++)
    {
        
        UIView*v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 5 )];
        
        v.backgroundColor = [UIColor whiteColor];
        
        v.layer.cornerRadius = 1.5;
        
        v.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.pageView addSubview:v];
        
        
        NSLayoutConstraint*con1=[NSLayoutConstraint constraintWithItem:v attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:lastView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0];
        
        [self.pageView addConstraint:con1];
        
        
        NSLayoutConstraint*con2=[NSLayoutConstraint constraintWithItem:v attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:lastView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0];
        
        [self.pageView addConstraint:con2];
        
        
        NSLayoutConstraint*con3=[NSLayoutConstraint constraintWithItem:v attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:lastView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
        
        [self.pageView addConstraint:con3];
        
        NSLayoutConstraint*con4;
        
        if ([DataClass getInstance].ar) {
            con4  =[NSLayoutConstraint constraintWithItem:v attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:lastView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-5.0];
        } else {
            con4=[NSLayoutConstraint constraintWithItem:v attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:lastView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:5.0];
        }
        
        
        [self.pageView addConstraint:con4];
        
        [allPages addObject:v];
        
        lastView = v;
    }
    
    NSLayoutConstraint*con5;
    if ([DataClass getInstance].ar) {
       con5 =[NSLayoutConstraint constraintWithItem:lastView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:_pageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:5.0];
        [self.pageView addConstraint:con5];
    } else {
       // con5=[NSLayoutConstraint constraintWithItem:_pageView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:lastView attribute:NSLayoutAttributeRight multiplier:1.0 constant:5.0];
        
    }
    
    if ([DataClass getInstance].ar) {
        [self manageRects:[DataClass getInstance].projectsSliderArr.count-1 ];
    } else {
        [self manageRects:0];
    }
    
    _pageView.center = self.view.center;
    
    [self.view layoutSubviews];
    
    
}

-(void) manageRects:(NSInteger)index {
    
    for (int i = 0; i < (int)allPages.count; ++i) {
        
        UIView* v = allPages[i];
        
        if(i==(index))
        {
            v.backgroundColor = [UIColor colorWithRed:2/255.0 green:107/255.0 blue:101/255.0 alpha:1];
        }
        else
        {
            v.backgroundColor = [UIColor whiteColor];
        }
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    if ([DataClass getInstance].ar) {
        
        _colPageControl.currentPage = sliderProjectsCount - 1;
        
    } else {
        _colPageControl.currentPage = 0;
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{

    [self animateBack];
    
    // log screen display
    [FIRAnalytics setScreenName:@"Main Screen" screenClass:@"maiViewController"];
    
}

-(void)animateBack
{
    self.botBackImageRightCon.constant = 0;
    
    self.botBackImageRightCon.constant+=self.view.bounds.size.width;
    
    [UIView animateWithDuration:40.0 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        self.botBackImageRightCon.constant = 0;
        
        [UIView animateWithDuration:40.0 animations:^{
            
            [self.view layoutIfNeeded];
            
            
        } completion:^(BOOL finished) {
            
            [self animateBack ];
            
        }] ;
        
    }] ;
    
}

#pragma mark : collection view delegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0f;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self sizingForRowAtIndexPath:indexPath];
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0f,0.0f, 0.0, 0.0f);
}
/*
-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    _colPageControl.currentPage = indexPath.row;
}
*/

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    int index = ((int)scrollView.contentOffset.x / (int)scrollView.frame.size.width);
    
//    if ([DataClass getInstance].ar) {
//        _colPageControl.currentPage = sliderProjectsCount - index - 1;
//    } else {
//        _colPageControl.currentPage = index;
//    }
    [self manageRects:index];
    
}

- (CGSize)sizingForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CGSize cellSize = CGSizeZero;
    
    cellSize.width=self.topView.bounds.size.width ;
    
    cellSize.height=self.topView.bounds.size.height ;
    
    NSLog(@"cellSize: %@", NSStringFromCGSize(cellSize));
    
    return cellSize;
}

#pragma mark : collection view data source

- (NSInteger)numberOfSectionsInCollectionView :(UICollectionView *)collectionView
{
    return 1   ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    _colPageControl.numberOfPages = [[DataClass getInstance].projectsSliderArr  count];
    
    if ([DataClass getInstance].ar) {
        
        _colPageControl.currentPage = sliderProjectsCount - 1;
        
    } else {
        _colPageControl.currentPage = 0;
        
    }
    
    return [[DataClass getInstance].projectsSliderArr  count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(collectionView == self.dyCollection)
    {
        mainCollectionViewCell *cell =
        [collectionView
         dequeueReusableCellWithReuseIdentifier:kCollectionViewCellIdentifier2
         forIndexPath:indexPath];
        
        projectItem*pr = [[DataClass getInstance].projectsSliderArr objectAtIndex:indexPath.row];
        
        if([DataClass getInstance].ar)
        {
            cell.titlelb.text = pr.textAr;
            
            cell.descriptionlb.text = pr.subtitleAr;
        }
        else
        {
            cell.titlelb.text = pr.textEn;
            
            cell.descriptionlb.text = pr.subtitleEn;
        }

        NSString* urlWithoutExtension = [pr.mSquareImgUrl substringToIndex:[pr.mSquareImgUrl length]-4];
        NSString* imageExtension = [pr.mSquareImgUrl substringFromIndex:[pr.mSquareImgUrl length]-4];
        NSString* imgUrlText = [NSString stringWithFormat:@"%@_medium%@",urlWithoutExtension,imageExtension];
        NSString* imgUrlTextEscaped = [imgUrlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [cell.activity startAnimating];
        cell.activity.hidden=NO;
        
        [cell.previewImageV sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrlTextEscaped] placeholderImage:nil options:SDWebImageRefreshCached progress:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            [cell.activity stopAnimating];
            cell.activity.hidden=YES;
        }];
        
        NSLog(@"Cell # %ld",(long)indexPath.row);
        
        return cell;
    }
    
    return nil;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView isEqual:self.dyCollection]) {
        // intialize pro det view controller
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        proDetViewController *pVC = [sb instantiateViewControllerWithIdentifier:@"proDetViewController"];
        pVC.project = [[DataClass getInstance].projectsSliderArr objectAtIndex:indexPath.row];
        pVC.comeFrom = comeFromMain;
        
        [self.navigationController pushViewController:pVC animated:YES];
        
    }
}


#pragma mark : private methods

-(void)receiveNotification:(NSNotification *)paramNotification
{
    
    if ([paramNotification.name isEqualToString:@"shareApp"])
    {
        NSString *AppStoreLink = @"https://itunes.apple.com/us/app/meamar-elmansour/id1323990629?ls=1&mt=8";
        
        NSArray *objectsToShare = @[AppStoreLink];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        
        UIViewController *topViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        while (topViewController.presentedViewController) {
            topViewController = topViewController.presentedViewController;
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
            [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)inView:topViewController.view permittedArrowDirections:0 animated:YES];
            
        } else {
            
            [topViewController presentViewController:activityVC animated:YES completion:nil];
        }
    }
}

-(void)swipeAll:(UISwipeGestureRecognizer *)gesture
{
    
    if(gesture.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self pushLeft];
    }
    else if(gesture.direction == UISwipeGestureRecognizerDirectionRight) {
        [self pushRight];
    }
}

-(void)pushLeft
{
    
    if(animationRunning)
    {
        return;
    }
    
    if(self.topView.tag >= [[DataClass getInstance].projectsSliderArr  count])
    {
        return;
    }
    
    animationRunning = YES;
    
    CGFloat sd = (CGFloat)(self.topView.tag);
    
    self.dyCollLefCon.constant = -1 * sd * self.view.bounds.size.width;
    
    
    [UIView animateWithDuration:1.0 animations:^{
        
        [self.view   layoutSubviews];
        [self.view   layoutIfNeeded];
        
        
    } completion:^(BOOL finished) {
        
        self.topView.tag = self.topView.tag + 1 ;
        
        animationRunning = NO;
        
        [self manageRects:self.topView.tag];
        
    }];
    
    
}

-(void)pushRight
{
    if(animationRunning)
    {
        return;
    }
    
    if(self.topView.tag <= 1)
    {
        return;
    }
    
    animationRunning = true;
    
    self.dyCollLefCon.constant +=    self.view.bounds.size.width;
    
    [UIView animateWithDuration:1.0 animations:^{
        
        [self.view   layoutSubviews];
        [self.view   layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        self.topView.tag = self.topView.tag - 1 ;
        
        animationRunning = NO;
        
        [self manageRects:self.topView.tag];
        
    }];
    
    
}

#pragma mark : table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.areaSettTable])
    {
        
        return [[DataClass getInstance].newsArr count] ;
        
    }
    
    return 0;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath      *)indexPath
{
    
    
    
    if ([tableView isEqual:self.areaSettTable]) {
        
        maiTableViewCell *cell =[self.areaSettTable dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        newsItem*n1 = [[DataClass getInstance].newsArr objectAtIndex:indexPath.row];
        
        
        if([DataClass getInstance].ar) {
            cell.titleLb.text = n1.titleAr;
            cell.summaryLb.text = n1.descriptionAr;
        
        } else {
            cell.titleLb.text = n1.titleEn;
            
            cell.summaryLb.text = n1.descriptionEn;
        }
        
        if (![n1.youtubeStr isEqualToString:@""]) {
            [cell.youtubeImg setAlpha:1.0];
        } else {
            [cell.youtubeImg setAlpha:0.0];
        }
        
        NSString* urlWithoutExtension = [n1.mSquareImgUrl substringToIndex:[n1.mSquareImgUrl length]-4];
        NSString* imageExtension = [n1.mSquareImgUrl substringFromIndex:[n1.mSquareImgUrl length]-4];
        NSString* imgUrlText = [NSString stringWithFormat:@"%@_medium%@",urlWithoutExtension,imageExtension];
        NSString* imgUrlTextEscaped = [imgUrlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

        [cell.activity startAnimating];
        cell.activity.hidden=NO;
    
        [cell.leftImageV sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:imgUrlTextEscaped] placeholderImage:nil options:SDWebImageRefreshCached progress:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            [cell.activity stopAnimating];
            cell.activity.hidden=YES;
        }];

        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView isEqual:self.areaSettTable]) {
        
        newsItem* item = [[DataClass getInstance].newsArr objectAtIndex:indexPath.row];
        
        NewsDetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsViewController"];
        vc.item = item;
        vc.comeFrom = comeFromMain;
        
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return 100;
    } else {
        return 200;
    }
    
    return UITableViewAutomaticDimension;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark : button actions

- (IBAction)callClicked:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:+219441"]];
}

- (IBAction)messageClicked:(id)sender {
    
    // check if user logged in
    if ([[NSUserDefaults standardUserDefaults] boolForKey:isLoggedIn]) {
        
        // get cashed user
        NSData *userData = [[NSUserDefaults standardUserDefaults] objectForKey:kCashedUser];
        IdentifiedUser *user = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
        
        [DataClassH shared].user = user;
        
        // check if guest or identified user
        if ([user.id isEqualToString:@""]) {
            // guest user just have name and phone number.
            
            [self createUser:user];
            
        } else {
            // real (identified) user have id, name and phone number.
            
            [self saveHelproxToken];
            
            UIStoryboard*stor = [UIStoryboard storyboardWithName:@"Helprox" bundle:nil];
            
            UIViewController*vc = [stor instantiateViewControllerWithIdentifier:@"ticketsView"];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    } else {
        // user not logged in
        
       UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginView"];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
    /*
    UIStoryboard*stor = [UIStoryboard storyboardWithName:@"Helprox" bundle:nil];
    
    UIViewController*vc = [stor instantiateViewControllerWithIdentifier:@"splashView"];
    
    [self.navigationController pushViewController:vc animated:YES];
     */
    
}

- (IBAction)moreClicked:(id)sender {
    NewsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"newsView"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)menuClicked:(id)sender {
    
    
    
    
    
}

- (IBAction)changeLangClicked:(id)sender
{
    NSUserDefaults*defaults = [NSUserDefaults standardUserDefaults];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"langTitle", nil) message:NSLocalizedString(@"langMsg", nil) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"langAlertOk", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([DataClass getInstance].ar) {
            // change language to en
            [defaults setObject:@[@"en"] forKey:@"AppleLanguages"];
        } else {
            // change language to ar
            [defaults setObject:@[@"ar"] forKey:@"AppleLanguages"];
        }
        
        [defaults synchronize];
        
        exit(0);
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"langAlertCancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:okAction];
    [alert addAction:cancelAction];

    [self presentViewController:alert animated:YES completion:nil];
    
}




-(void)createUser:(IdentifiedUser*)user
{
    
    NSLog(@"fcasdasdasdasdasd : %@",user);
    
    [AppDelegate addHoverView:self.view];
    
    NSMutableDictionary*dic = [NSMutableDictionary new];
    
    [dic setObject:@"" forKey:@"user_id"];
    
    [dic setObject:user.type forKey:@"type"];
    
    [dic setObject:user.firstName forKey:@"first_name"];
    
    [dic setObject:user.lastName forKey:@"last_name"];
    
    [dic setObject:user.phone forKey:@"phone"];
    
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@apis/v1/createUser",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_data=%@",[DataClassH shared].apiKey,@"ios", [dic jsonString]];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             NSLog(@"createUser respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     NSLog(@"createUser success");
                     
                     
                         
                         user.id = responseDict[@"user_id"];
                         
                         NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
                         
                         [defaults setObject:data forKey:kCashedUser];
                         
                         [defaults synchronize];
                         
                         
                         [DataClassH shared].user = user ;
                         
                         [self saveMansourToken];
                         
                    
                 } else {
                     NSLog(@"createUser request fail reason %@",responseDict[@"reason"]);
                 }
             } else {
                 NSLog(@"createUser nil Dic ");
             }
         } else {
             NSLog(@"createUser error : %@",error);
             
             dispatch_queue_t mainQueue = dispatch_get_main_queue();
             
             dispatch_async(mainQueue, ^(void) {
                 
                 [AppDelegate removeHoverView:self.view];
                 
             });
             
         }
         
     }];
    
    
}

-(void)saveHelproxToken {
    NSString *urlAsString = @"http://46.101.143.146:7894/apis/v1/saveToken";
    
    [self saveToken:urlAsString];
}

-(void)saveMansourToken {
    NSString *urlAsString = [NSString stringWithFormat:@"%@apis/v1/saveToken",Domain];
    
    [self saveToken:urlAsString];
}

-(void)saveToken:(NSString*)urlAsString {
    
    NSLog(@"saveToken2001455555");
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_type=%@&user_id=%@&fcm_token=%@",[DataClassH shared].apiKey,@"ios",[DataClassH shared].user.type,[DataClassH shared].user.id,[defaults valueForKey:kFCMTokenHelprox]];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             
             NSLog(@"saveToken respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     
                     
                      dispatch_queue_t mainQueue = dispatch_get_main_queue();
                      
                      dispatch_async(mainQueue, ^(void) {
                          /*
                          [AppDelegate removeHoverView:self.view];
                      
                          UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Helprox" bundle:nil];
                          
                          UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ticketsView"];
                          
                          [self.navigationController pushViewController:vc animated:YES];
                          */
                      });
                      
                     
                 }
                 
                 else
                 {
                     
                     
                     
                 }
                 
             }
             else
             {
                 NSLog(@"saveToken nil Dic ");
             }
             
         }
         else
         {
             NSLog(@"saveToken error : %@",error);
             
             
             dispatch_queue_t mainQueue = dispatch_get_main_queue();
             
             dispatch_async(mainQueue, ^(void) {
                 
                 [AppDelegate removeHoverView:self.view];
               
             });
             
             
         }
         
     }];
    
    
}


@end

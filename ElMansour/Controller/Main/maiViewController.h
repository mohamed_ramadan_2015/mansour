//
//  mainViewController.h
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface maiViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>


@property (weak, nonatomic) IBOutlet UITableView*areaSettTable;

@property (weak, nonatomic) IBOutlet UIImageView * anim1ImageV;

@property (weak, nonatomic) IBOutlet UIImageView * anim2ImageV;

@property (weak, nonatomic) IBOutlet UIImageView * anim3ImageV;

@property (weak, nonatomic) IBOutlet UIImageView * anim4ImageV;

@property (weak, nonatomic) IBOutlet UIView *pageView;

@property (weak, nonatomic) IBOutlet UIView * pageDot1;

@property (weak, nonatomic) IBOutlet UIView * pageDot2;

@property (weak, nonatomic) IBOutlet UIView * pageDot3;

@property (weak, nonatomic) IBOutlet UIView * pageDot4;

@property (weak, nonatomic) IBOutlet UIView * topView;

@property (weak, nonatomic) IBOutlet UIView * bottomView;

@property (weak, nonatomic) IBOutlet UIImageView * bottomBackImageV;

@property (weak, nonatomic) IBOutlet UIButton * menuBu;

@property (weak, nonatomic) IBOutlet UIImageView * logoImageV;

@property (weak, nonatomic) IBOutlet UILabel * titlelb;

@property (weak, nonatomic) IBOutlet UILabel * descriptionlb;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint * anim1ImageLedCon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dyCollLefCon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botBackImageRightCon;

@property (weak, nonatomic) IBOutlet UICollectionView *dyCollection;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageDot1TraCon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageDot1LeadCon;
 
@property (weak, nonatomic) IBOutlet UILabel *newslb;

@property (weak, nonatomic) IBOutlet UIButton *moreBu;

@property (weak, nonatomic) IBOutlet UIPageControl *colPageControl;


@property (weak, nonatomic) IBOutlet UILabel *notificationsNumlb;


- (IBAction)callClicked:(id)sender;

- (IBAction)messageClicked:(id)sender;

- (IBAction)moreClicked:(id)sender;

- (IBAction)menuClicked:(id)sender;
 
@end

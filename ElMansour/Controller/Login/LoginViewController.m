//
//  LoginViewController.m
//  Mansour
//
//  Created by M R on 11/12/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "LoginViewController.h"
#import "UserItem.h"
#import "IdentifiedUser.h"
@import Firebase;

@interface LoginViewController ()

@end

@implementation LoginViewController {
    AKFAccountKit *_accountKit;
    UIViewController<AKFViewController> *_pendingLoginViewController;
    NSString *_authorizationCode;
    NSString* phoneNumStr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _phoneTxt.delegate = self;
    phoneNumStr = nil;
    
    // initialize Account Kit
    if (_accountKit == nil) {
        // may also specify AKFResponseTypeAccessToken
        _accountKit = [[AKFAccountKit alloc] initWithResponseType:AKFResponseTypeAuthorizationCode];
        
    }
    
    // view controller for resuming login
    _pendingLoginViewController = [_accountKit viewControllerForLoginResume];
    [_pendingLoginViewController setAccessibilityLanguage:@"en_US"];
    
    
    if([DataClass getInstance].ar)
    {
        [self.backBu setTitle:[DataClass getInstance].leftArrow forState:UIControlStateNormal];
        [_loginBu setTitle:@"دخول" forState:UIControlStateNormal];
        _loginLbl.text = @"تسجيل الدخول";
        _phoneTxt.placeholder = @"رقم الموبايل";
       
        _welcomeLbl.text = @"مرحباً بك فى منصور يا";
        [_continueBu setTitle:@"استمرار" forState:UIControlStateNormal];
        _loadingLbl.text = @"انتظر...";
        _errorLbl.text = @"خطأ, حاول مرة أخرى";
        
        _firstNameTextField.placeholder = @"الاسم الاول";
        _lastNameTextField.placeholder = @"الاسم الاخير";
    }
    else
    {
        [self.backBu setTitle:[DataClass getInstance].rightArrow forState:UIControlStateNormal];
        [_loginBu setTitle:@"Login" forState:UIControlStateNormal];
        _loginLbl.text = @"Login";
        _phoneTxt.placeholder = @"Mobile Number";
        _welcomeLbl.text = @"Welcome";
        [_continueBu setTitle:@"Continue" forState:UIControlStateNormal];
        _loadingLbl.text = @"Loading...";
        _errorLbl.text = @"Login Error, Try again.";
        
        _firstNameTextField.placeholder = @"First Name";
        _lastNameTextField.placeholder = @"Last Name";
    }
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tap1.numberOfTapsRequired = 1;
    _phoneLbl.userInteractionEnabled = YES;
    [_phoneLbl addGestureRecognizer:tap1];
    
    
    [_welcomeView setHidden:YES];
    [_loadingView setHidden:YES];
    [_errorView setHidden:YES];
    [_getNameView setHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    
    _contentView.layer.cornerRadius = 10;
    _contentView.clipsToBounds = YES;
    
    _upScrollView.layer.cornerRadius = 10;
    _upScrollView.clipsToBounds = YES;
    
    _welcomeView.layer.cornerRadius = 10;
    _welcomeView.clipsToBounds = YES;
    
    _continueBu.layer.cornerRadius = 2;
    
    _loadingView.layer.cornerRadius = 10;
    _loadingView.clipsToBounds = YES;
    
    _errorView.layer.cornerRadius = 10;
    _errorView.clipsToBounds = YES;
    
    _getNameView.layer.cornerRadius = 10;
    _getNameView.clipsToBounds = YES;

    // log screen display
    [FIRAnalytics setScreenName:@"Login Screen" screenClass:@"LoginViewController"];
    
}

#pragma mark : Button Actions

- (IBAction)backClicked:(id)sender {
    
    UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)continueClicked:(id)sender {
    
    UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)continueWithNameClicked:(id)sender {
    
    if([_firstNameTextField.text isEqualToString:@""]
        || [_lastNameTextField.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message" message:@"Please enter the name to continue" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    IdentifiedUser *user = [[IdentifiedUser alloc] initWithId:@""
                                            firstName:_firstNameTextField.text
                                             lastName:_lastNameTextField.text
                                                phone:[NSUserDefaults.standardUserDefaults objectForKey:userPhoneNumber]
                                              type:@"undefined"];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
    NSUserDefaults*defaults =  [NSUserDefaults standardUserDefaults];
    [defaults setObject:data forKey:kCashedUser];
    [defaults synchronize];
    
    
    UIViewController*vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)twitterClicked:(id)sender {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://twitter.com/MeamarElmansour"]];
    [[UIApplication sharedApplication] openURL:url];
    
}

- (IBAction)facebookClicked:(id)sender {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.facebook.com/MeamarElmansour"]];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)loginClicked:(id)sender {
    
    AKFPhoneNumber* phoneNum = [[AKFPhoneNumber alloc] initWithCountryCode:@"+02" phoneNumber:[_phoneTxt text]];
    
    NSString *inputState = [[NSUUID UUID] UUIDString];
    UIViewController<AKFViewController> *viewController = [_accountKit viewControllerForPhoneLoginWithPhoneNumber:phoneNum
                                                                                            state:inputState];
    viewController.enableSendToFacebook = YES; // defaults to NO
    [self _prepareLoginViewController:viewController]; // see below
    [self presentViewController:viewController animated:YES completion:NULL];

    
    
    /*
    */
}

- (IBAction)okErrorClicked:(id)sender {
    
    [_errorView setHidden:YES];
    [_contentView setHidden:NO];
}

#pragma mark : Text Field Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    
    if ([textField isEqual:_phoneTxt]) {
        
        if ([textField.text isEqualToString:@""]) {
            phoneNumStr = nil;
            
            return YES;
        } else {
            phoneNumStr = [textField text];
        }
        
        AKFPhoneNumber* phoneNum = [[AKFPhoneNumber alloc] initWithCountryCode:@"+02" phoneNumber:[_phoneTxt text]];
        
        NSString *inputState = [[NSUUID UUID] UUIDString];
        UIViewController<AKFViewController> *viewController = [_accountKit viewControllerForPhoneLoginWithPhoneNumber:phoneNum
                                                                                                                state:inputState];
        viewController.enableSendToFacebook = YES; // defaults to NO
        [self _prepareLoginViewController:viewController]; // see below
        [self presentViewController:viewController animated:YES completion:NULL];
    
    } else if([textField isEqual:_firstNameTextField]){
        [_firstNameTextField becomeFirstResponder];
        
    } else if ([textField isEqual:_lastNameTextField]) {
        
    }
   
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    
    if ([_phoneTxt.text isEqualToString:@""]) {
        phoneNumStr = nil;
    } else {
        phoneNumStr = [_phoneTxt text];
    }
    
}

#pragma mark : private methods

-(void) handleTap: (UIGestureRecognizer*) sender {
    
    if ([sender.view isEqual:_phoneLbl]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:+19441"]];
    }
}

-(void) sendAuthorizationCodeToServer :(NSString*) code {
    
    if (_phoneTxt.text != nil) {
        
        // hide content view
        [_contentView setHidden:YES];
        
        //display loading view
        [_loadingView setHidden:NO];
        [_indicatorView startAnimating];
        
        __block NSMutableDictionary *resultsDictionary;
        
        NSDictionary *userDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:code, @"code", nil];
        
        if ([NSJSONSerialization isValidJSONObject:userDictionary]) {//validate it
            NSError* error;
            NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userDictionary options:NSJSONWritingPrettyPrinted error: &error];
            
            NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@api/v1/client/login",Domain]];
            NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
            
            [request setHTTPMethod:@"POST"];//use POST
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-length"];
            [request setHTTPBody:jsonData];//set data
            
            __block NSError *error1 = [[NSError alloc] init];
            
            //use async way to connect network
            [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
             {
                 if ([data length]>0 && error == nil) {
                     resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
                     NSLog(@"resultsDictionary is %@",resultsDictionary);
                     
                     if ([resultsDictionary[@"status_code"]  isEqual: @200]) {
                         // user exist in system
                         NSLog(@"%@ : %@", resultsDictionary[@"message"],resultsDictionary[@"description"]);
                        
                         
                         NSDictionary *userData = resultsDictionary[@"data"][0];
                         
                         // set current user as a real user
                         [NSUserDefaults.standardUserDefaults setBool:YES forKey:isRealUser];
                         [NSUserDefaults.standardUserDefaults setBool:NO forKey:isGuestUser];
                         [NSUserDefaults.standardUserDefaults setBool:YES forKey:isLoggedIn];
                         [NSUserDefaults.standardUserDefaults setObject:userData[@"number"] forKey:userPhoneNumber];
                         [NSUserDefaults.standardUserDefaults setObject:userData[@"name"] forKey:userName];
                         [NSUserDefaults.standardUserDefaults setObject:userData[@"id"] forKey:userId];
                         
                         
                         IdentifiedUser *user = [[IdentifiedUser alloc] initWithId:userData[@"id"]
                                                                 firstName:userData[@"name"]
                                                                  lastName:@""
                                                                     phone:userData[@"number"]
                                                                   type:@"defined"];
                         
                         NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
                         NSUserDefaults*defaults =  [NSUserDefaults standardUserDefaults];
                         [defaults setObject:data forKey:kCashedUser];
                         [defaults synchronize];
                         
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             _userNameLbl.text = userData[@"name"];
                             
                             //hide loading view
                             [_indicatorView stopAnimating];
                             [_loadingView setHidden:YES];
                             
                             //display welcome View
                             [_welcomeView setHidden:NO];
                         });
                         
                         
                         
                     } else if ([resultsDictionary[@"status_code"]  isEqual: @404]){
                         // user not exist or not verified
                         NSLog(@"%@ : %@", resultsDictionary[@"message"],resultsDictionary[@"description"]);
                         
                         // set current user as guest
                         [NSUserDefaults.standardUserDefaults setBool:NO forKey:isRealUser];
                         [NSUserDefaults.standardUserDefaults setBool:YES forKey:isGuestUser];
                         [NSUserDefaults.standardUserDefaults setBool:YES forKey:isLoggedIn];
                         [NSUserDefaults.standardUserDefaults setObject:resultsDictionary[@"number"] forKey:userPhoneNumber];
                         
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             _userNameLbl.text = resultsDictionary[@"number"];
                             
                             //hide loading view
                             [_indicatorView stopAnimating];
                             [_loadingView setHidden:YES];
                             
                             //get guest user full name
                             [_getNameView setHidden:NO];
                             
                         });
                         
                     } else {
                         // something else ...
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [_indicatorView stopAnimating];
                             [_loadingView setHidden:YES];
                             
                             [_errorView setHidden:NO];
                         });
                     }
                     
                 } else if ([data length]==0 && error ==nil) {
                     NSLog(@" download data is null");
                 } else if( error!=nil) {
                     NSLog(@" error is %@",error);
                 }
             }];
        }
        
    }
}

#pragma mark : Acount kit delegate

- (void)_prepareLoginViewController:(UIViewController<AKFViewController> *)loginViewController
{
    loginViewController.delegate = self;
    // Optionally, you may use the Advanced UI Manager or set a theme to customize the UI.
   // loginViewController.advancedUIManager = _advancedUIManager;
    //loginViewController.theme = [Themes bicycleTheme];
}


// handle callback on successful login to show authorization code
- (void)                 viewController:(UIViewController<AKFViewController> *)viewController
  didCompleteLoginWithAuthorizationCode:(NSString *)code
                                  state:(NSString *)state {
    
    // Pass the code to your own server and have your server exchange it for a user access token.
    // You should wait until you receive a response from your server before proceeding to the main screen.
    
    [self sendAuthorizationCodeToServer:code];
    //[self proceedToMainScreen];
}


- (void)viewController:(UIViewController<AKFViewController> *)viewController didFailWithError:(NSError *)error {
    // ... implement appropriate error handling ...
    NSLog(@"%@ did fail with error: %@", viewController, error);
}

- (void)viewControllerDidCancel:(UIViewController<AKFViewController> *)viewController {
    // ... handle user cancellation of the login process ...
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

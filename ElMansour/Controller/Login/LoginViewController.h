//
//  LoginViewController.h
//  Mansour
//
//  Created by M R on 11/12/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AccountKit/AccountKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate,AKFViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backBu;

@property (weak, nonatomic) IBOutlet UIButton *loginBu;

@property (weak, nonatomic) IBOutlet UIButton *continueBu;

@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *welcomeView;

@property (weak, nonatomic) IBOutlet UIView *getNameView;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *continueNamesBu;

@property (weak, nonatomic) IBOutlet UIView *loadingView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;

@property (weak, nonatomic) IBOutlet UILabel *loadingLbl;

@property (weak, nonatomic) IBOutlet UIView *upScrollView;

@property (weak, nonatomic) IBOutlet UILabel *loginLbl;

@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;

@property (weak, nonatomic) IBOutlet UILabel *welcomeLbl;

@property (weak, nonatomic) IBOutlet UITextField *phoneTxt;

@property (weak, nonatomic) IBOutlet UILabel * phoneLbl;


@property (weak, nonatomic) IBOutlet UIView *errorView;

@property (weak, nonatomic) IBOutlet UILabel *errorLbl;

@property (weak, nonatomic) IBOutlet UIButton *okErrorBu;



@end

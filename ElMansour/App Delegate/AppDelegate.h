//
//  AppDelegate.h
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import <UIKit/UIKit.h>
static NSString *kNews = @"kNews";
static NSString *kProjectsSlider = @"kProjectsSlider";
static NSString *kProjectsChat = @"kProjectsChat";

static NSString *kProjectName = @"kProjectName";

static NSString *kChatTitle = @"kChatTitle";


static NSString *kProjects = @"kProjects";
static NSString *kCashedUser = @"kCashedUser";
static NSString *Kinstallments = @"Kinstallments";
static NSString *isGuestUser = @"isGuestUser";
static NSString *isVirifiedUser = @"isVirifiedUser";
static NSString *userPhoneNumber = @"userPhoneNumber";
static NSString *userName = @"userName";
static NSString *userId = @"userId";
static NSString *isRealUser = @"isRealUser";
static NSString *isLoggedIn = @"isLoggedIn";

static NSString *comeFromMain = @"comeFromMain";
static NSString *comeFromProjList = @"comeFromProjList";
static NSString *comeFromNewsList = @"comeFromNewsList";

static NSString *kFCMTokenHelprox = @"kFCMToken";

static NSString *Domain = @"https://admin.melmansour.com/";

#import <UserNotificationsUI/UserNotificationsUI.h>

#import <UserNotifications/UserNotifications.h>

@import FirebaseMessaging;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;

+(void) addHoverView:(UIView*)v;

+(void)  removeHoverView:(UIView*)vd;
@end


//
//  AppDelegate.m
//  Mansour
//
//  Created by ShKhan on 10/24/17.
//  Copyright © 2017 Approcks. All rights reserved.
//

#import "AppDelegate.h"
#import "IdentifiedUser.h"
#import <Crashlytics/Crashlytics.h>
#import <Fabric/Fabric.h>

@import Firebase;

#define firstLoad @"firstLoad"

@interface AppDelegate ()

{
    NSUserDefaults*defaults;
    
}

@property (strong, nonatomic) NSTimer *timer;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    // Register platform, user name, number, device_id
    if ([DataClass isNetworkAvailable] && [defaults boolForKey:isLoggedIn]) {
        [self registerPlatform];
        
        // TODO : implement this after megre with shehata
        [self retrieveCleintData];
    }
    
    
    // app loaded
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:firstLoad];
    
    
    
    // Use Firebase library to configure APIs
    [FIRApp configure];
    
    // set crash debuging mode
    [Fabric.sharedSDK setDebug:YES];
    
    // create crash manually
    //[[Crashlytics sharedInstance] crash];
    //assert(0);
    
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max)
    {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    else
    {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        
        
#endif
    }
    
    
    [FIRMessaging messaging].delegate = self;
    
    
    [application registerForRemoteNotifications];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tokenRefreshNotification:)
                                                 name:FIRMessagingRegistrationTokenRefreshedNotification
                                               object:nil];
    
    
    
    
    /*    for (NSString* family in [UIFont familyNames])
     {
     NSLog(@"Family %@", family);
     
     for (NSString* name in [UIFont fontNamesForFamilyName: family])
     {
     NSLog(@"Name  %@", name);
     }
     }
     
     // set Cocon font
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
     [[UILabel appearance] setFont:[UIFont fontWithName:@"CoconNextArabic-Regular" size:12.0]];
     } else {
     [[UILabel appearance] setFont:[UIFont fontWithName:@"CoconNextArabic-Regular" size:17.0]];
     }*/
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerCalled) userInfo:nil repeats:YES];
    
    
    return YES;
}

-(void)timerCalled
{
    
    NSString*token = [FIRInstanceID instanceID].token;
    
    NSLog(@"Timer Called token : %@",[FIRInstanceID instanceID].token);
    
    if(token!=nil)
    {
        
        [defaults setObject:token forKey:kFCMTokenHelprox];
        
        NSString*idf = [DataClassH shared].user.id;
        
        if(idf!=nil)
        {
            [self saveToken];
        }
        
        [self.timer invalidate];
        
        self.timer = nil ;
    }
    
    
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
 
}

- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    /* Each byte in the data will be translated to its hex value like 0x01 or
     0xAB excluding the 0x part, so for 1 byte, we will need 2 characters to
     represent that byte, hence the * 2 */
    NSMutableString *tokenAsString = [[NSMutableString alloc]
                                      initWithCapacity:deviceToken.length * 2];
    char *bytes = malloc(deviceToken.length);
    [deviceToken getBytes:bytes length:deviceToken.length*2];
    for (NSUInteger byteCounter = 0; byteCounter < deviceToken.length; byteCounter++){
        char byte = bytes[byteCounter];
        [tokenAsString appendFormat:@"%02hhX", byte];
    }
    free(bytes);
    NSLog(@"Tokeneereeffefe = %@", tokenAsString);
    
    [defaults setObject:tokenAsString forKey:@"deviceToken"];
    
    
    [FIRMessaging messaging].APNSToken = deviceToken;
    
#if DEBUG
    
    // for development
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
    
#else
    // for production
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
    
#endif
    
}


+(void) addHoverView:(UIView*)v {
    
    UIView*hover = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 5 )];
    hover.tag = 120;
    
    hover.frame = v.frame;
    
    [v addSubview :hover];
    
    hover.center = v.center;
    
    hover.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    UIActivityIndicatorView* act = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    act.center = hover.center;
    
    [ hover addSubview:act];
    
    [ act startAnimating];
    
    
}

/**
 ## removes hover view from main window when app finishes getting the data. ##
 
 - Parameter v: v is the parent view where hover view should be removed.
 
 */

+(void)  removeHoverView:(UIView*)vd
{
    for (UIView*v in vd.subviews) {
        
        if(v.tag==120)
        {
            [ v removeFromSuperview];
        }
    }
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    //   if (userInfo[@"vsdcvsdfcsdfcs"]) {
    //      NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    //  }
    
    // Print full message.
    NSLog(@"ios 11 ,9 and 8 push notifications %@", userInfo);
    
    /*UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
     message:userInfo[@"aps"][@"alert"]
     delegate:nil
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert show];*/
    
    
    
    // Let FCM know about the message for analytics etc.
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    // handle your message.
    
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    
    
    NSLog(@"fcmToken10000 %@", fcmToken);
    
    
}


-(void)tokenRefreshNotification:(NSNotification*) Notification
{
    
    
    NSLog(@"dscasdfcasdcasc1121221 : %@",Notification);
    
    
    if ([FIRInstanceID instanceID].token != nil) {
        
        
        NSLog(@"InstanceID100 token:%@" ,[FIRInstanceID instanceID].token);
        
        
    }
    else{
        
        NSLog(@"InstanceID100 a777777a");
        
        
    }
    
    [self connectToFcm];
}

-(void) connectToFcm
{
    
    [[FIRMessaging messaging ] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        
        
        if ( error != nil )
        {
            NSLog(@"Unable to connect with FCM : %@" ,error);
        } else
        {
            NSLog(@"Connected to FCM.");
        }
        
        
        
    }];
    
    
    
}

-(void) retrieveCleintData {
    
    NSLog(@"Retrieve user data");
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/client/%@",Domain,[DataClassH shared].user.phone];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@""];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             NSLog(@"retrieve user data respo is : %@",responseDict);
             if(responseDict!=nil)
             {
                 if([[responseDict valueForKey:@"success"] integerValue]==1)  {
                     // TODO : implement this after megre with shehata
                     // override cashed user with saved retrieved data
                     // ...
                     IdentifiedUser *user = [[IdentifiedUser alloc] initWithId:responseDict[@"id"] firstName:responseDict[@"name"] lastName:responseDict[@""] phone:responseDict[@"number"] type:responseDict[@"verified"]?@"defined" : @"undefined"];
                     
                     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
                     NSUserDefaults*defaults =  [NSUserDefaults standardUserDefaults];
                     [defaults setObject:data forKey:kCashedUser];
                     [defaults synchronize];
                     
                     [DataClassH shared].user = user;
                     
                 } else {
                     
                 }
             } else {
                 NSLog(@"retrieve user data nil Dic");
             }
         } else {
             NSLog(@"retrieve user data error : %@",error);
         }
     }];
}

-(void) registerPlatform {
    
    NSLog(@"send platform");
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@api/v1/client/register",Domain];
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *userName = [NSString stringWithFormat:@"%@ %@",[DataClassH shared].user.firstName,[DataClassH shared].user.lastName];
    
    NSString *body =[NSString stringWithFormat:@"name=%@&platform=%@&number=%@&device_id=%@",userName,@"ios",[DataClassH shared].user.phone,[defaults valueForKey:kFCMTokenHelprox]];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             NSLog(@"send platform respo is : %@",responseDict);
             if(responseDict!=nil)
             {
                 if([[responseDict valueForKey:@"success"] integerValue]==1)  {
                     
                 } else {
                     
                 }
             } else {
                 NSLog(@"send platform nil Dic ");
             }
         } else {
             NSLog(@"send platform error : %@",error);
         }
     }];
}


-(void)saveToken
{
    
    NSLog(@"saveToken2001455555");
    
    NSString *urlAsString = @"http://46.101.143.146:7894/apis/v1/saveToken";
    
    NSURL *url = [NSURL URLWithString:urlAsString];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSString *body =[NSString stringWithFormat:@"apiKey=%@&platform=%@&user_type=%@&user_id=%@&fcm_token=%@",[DataClassH shared].apiKey,@"ios",[defaults valueForKey:kUserType],[DataClassH shared].user.id,[defaults valueForKey:kFCMTokenHelprox]];
    
    [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             
             
             
             NSLog(@"saveToken respo is : %@",responseDict);
             
             if(responseDict!=nil)
             {
                 
                 if([[responseDict valueForKey:@"success"] integerValue]==1)
                 {
                     
                     
                     /*dispatch_queue_t mainQueue = dispatch_get_main_queue();
                      
                      dispatch_async(mainQueue, ^(void) {
                      
                      UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:nil
                      message:@"token sent"
                      delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil];
                      [alert show];
                      
                      });*/
                 } else {
                    
                 }
             } else {
                 NSLog(@"saveToken nil Dic ");
             }
         } else {
             NSLog(@"saveToken error : %@",error);
         }
     }];
}





@end
